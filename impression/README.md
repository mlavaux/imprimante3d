# 3D Printer project

## Structure

### models/
ply and stl files of some models used for 3D printing

### report/
Report to be evaluated by the jury

### scripts/
The blender scripts

### setup_doc/
Prerequirement, everything you need to install and configure in order to use this project

### slides/
Presentation of the project

### userDocumentation/
User documentation, describe how to use the Blender plugin

### tech_doc/
Sphinx generated documentation from the source code

### config_slic3r_14-02.ini
Latest Slic3r configuration

### startup.blend
Setup Blender file.


