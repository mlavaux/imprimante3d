% Ensure that every parameters/execution cause no bug disregarding the quality

function ret = U1_nobug(verbose)

  full_clock = tic;
  addpath('../src/');

  ret = true;
  verbose = true;
  % input camera
  
  vect = [ 0 0 0 0 0 0];

  signature ={'RMSE'; 'ZNCC'};%; 'SURF']);
  extend = {'extendMatching1';'extendMatching2'};
  quart = {'remove_last_quartile' ; 'none'};
  algo3D = {'loo';'ransac'};
  algoMesh = { 'convexHull' ; 'crust' ; 'delaunayFaceRemoval'};
  % SURF NOT TESTED
  max_picture = 3;
  ok = 1;

  addpath('../src/Calcul_PoI/src');
  addpath('../src/Calibration/src/');
  addpath('../src/Calibration/src/bin');
  addpath('../src/3D_Cloud_Creation/src');
  addpath('../src/Matching_PoI/src');
  addpath('../src/Mesh_Creation/src');
  addpath('../src/Common/src');
  addpath('../src/Background_processing/src');


   folder = '../media/House_Blender_Vincent/version2';
   format = '/*.png';
   addpath(folder);
  total = length(signature)*length(extend)*length(quart)*length(algo3D)*length(algoMesh);
  ite = 0;
  for sig = 1:length(signature)
  for ext = 1:length(extend)
  for qua = 1:length(quart)
  for al3 = 1:length(algo3D)
  for alM = 1:length(algoMesh)
  p_clock = tic;
     si = signature{sig};
     ex = extend{ext};
     qu = quart{qua};
     a3 = algo3D{al3};
     aM = algoMesh{alM};
     ite = ite + 1;
   %if strcmp(am,'convexHull') && ~strcmp(al,'ransac')
   %  break;
   %end
    %%-------- Blender house --------%

   %--------------------------------------------%
     fprintf('TU1 : source : %s%',folder);
     disp(' ');
    fprintf('Using signature : %s%',si);
    disp(' ');
    fprintf('Thoughness algorithm %s%',a3);
    disp(' ');
    fprintf('Mesh algorighm %s%',aM);
    disp(' ');
    fprintf('Extend Matching %s%',ex);
    disp(' ');
    fprintf('Quartil removal %s%',qu);
    disp(' ');
    fprintf('Using %d%',max_picture(1));
    disp(' images');

    load_data = { 'cameras'};
    save_data = {};
    ok = reconstruction(folder,max_picture(1),format,vect,...
                        'nopath',...
                         si,...
                         ex,...
                         qu,...
                        'reprojection',...
                         a3,...
                         aM,...
                         'load',{'cameras';'masks'},...
                         'save',{});
   %--------------------------------------------% 
    fprintf('Test : %d%',ite);
    fprintf('/%d%',total);
    fprintf(' elapsed time : %f%',toc(p_clock));
    disp(' ');

end
end
end
end

fprintf('Elapsed time : %f%',toc(full_clock));

  if verbose && ret
    disp('Unitary test : OK ');
  end

end
