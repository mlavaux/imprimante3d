#!/usr/bin/python2
# -*- coding: utf-8 -*-

import bpy 
from mathutils import * 
from math import *

def camera_intrinsic():
    cam = bpy.data.cameras['Camera']
    scn = bpy.data.scenes['Scene'] 
    w = scn.render.resolution_x*scn.render.resolution_percentage/100. 
    h = scn.render.resolution_y*scn.render.resolution_percentage/100.
    C = Matrix()
    C[0][0] = -w/2 / tan(cam.angle/2) 
    ratio = w/h 
    C[1][1] = h/2. / tan(cam.angle/2) * ratio 
    C[0][2] = w / 2. 
    C[1][2] = h-h / 2. 
    C[2][2] = 1. 
    C[3][3] = 1. 
    return C
    

def camera_projection_matrix():
    """
    Use case: 
    v = Vector((0,0,0)) 
    P = camera_projection_matrix()
    p = P*v
    p /= p[2]
    print(p)
    """

    # Getting width, height and the camera 
     
     
    camobj = bpy.data.objects['Camera'] 


    # Getting camera parameters 
    # Extrinsic 
    RT = camobj.matrix_world.inverted() 

    # Intrinsic 
    C = camera_intrinsic()    

    return C*RT
    
def matrix_to_matlab(m):
    res = "["
    i = 0
    for row in m:
        for e in row:
            res += "%f " % e
        res += "; "
        i += 1
        if i == 3:
            break
    res = res[:-3] + "]"
    return res
    
    
def export_cameras_matlab():
    scn = bpy.data.scenes['Scene']
    cams = []
    camf = "cameras = {"
    for i in range(scn.frame_start, scn.frame_end+1):
        #bpy.ops.anim.change_frame(frame = i)
        bpy.context.scene.frame_set(i)
        #bpy.context.scene.frame_set(scn.frame_current)
        P = camera_projection_matrix()
        cams.append(P)
        camf +=  matrix_to_matlab(P) + ",\n"
    camf = camf[:-2] + "};"
    return (camf,cams)

def project_vertex(cam, vert):
    ob = bpy.context.selected_objects[0]
    v = Vector((0,0,0,1))
    v.x = vert.co.x
    v.y = vert.co.y
    v.z = vert.co.z
    p = cam*ob.matrix_world*v
    #print(j, ob.matrix_world*v)
    return p/p.z

def can_project_vertex(vert):
    cam = bpy.data.objects.get("Camera")
    ob = bpy.context.selected_objects[0]
    v = Vector((0,0,0,1))
    v.x = vert.co.x
    v.y = vert.co.y
    v.z = vert.co.z
    vg = ob.matrix_world*v
    vg.resize_3d()
    v.resize_3d()
    t = ob.matrix_world.copy()
    t.invert()
    m = cam.matrix_world
    cam_location = Vector((m[0][3], m[1][3], m[2][3]))
    cam_vector =  t*cam_location
    #cam_vector = vg - cam.location
    if cam_vector.dot(vert.normal) > 0:
        return True
    else:
        return False

def create_matchings(cams, nb_pictures):
    # Naïve version
    res = ""
    ob = bpy.context.selected_objects[0]
    for vert in ob.data.vertices:
        for i in range(nb_pictures):
            p = project_vertex(cams[i], vert)
            res += "%d %d " % (round(p.x,0), round(p.y,0))
        res += "\n"
    return res

def fake_poi(cams, frame_start, frame_end):
    scn = bpy.data.scenes['Scene']
    ob = bpy.context.selected_objects[0]
    fakes_pois = {}  
    for i in range(frame_start, frame_end+1):
        bpy.context.scene.frame_set(i)
        #bpy.context.scene.frame_set(scn.frame_current)
        fakes_pois[i] = []
        
        for vert in ob.data.vertices:
            if can_project_vertex(vert):
                fakes_pois[i].append(vert)
                
    return fakes_pois

def export_pois(cams, fakes_pois):
    poi = "pts = {"
    for index in fakes_pois:
        poi += "["
        for vert in fakes_pois[index]:
            p = project_vertex(cams[index-1], vert)
            x = round(p.x,0)
            y = round(p.y,0)  
            poi += "[%d %d]' " % (x, y)
        poi += "] , "
    poi = poi[:-2] + "};"
    return poi

def create_pair_matchings(pois1, pois2):    
    indexes1 = {}
    indexes2 = {}
    # indexes[i] = j : the poi of index i in Blender is the jth poi of the list
    for i in range(len(pois1)):
        indexes1[pois1[i].index] = i
    for i in range(len(pois2)):
        indexes2[pois2[i].index] = i
    #print(indexes1.values())
    #print(indexes2.values())
    shared_indexes = set(indexes1.keys()).intersection(set(indexes2.keys()))
    matchings =[]
    for index in shared_indexes:
        # Matlab indexes start at 1, not 0
        matchings.append( [indexes1[index]+1, indexes2[index]+1] )
    return matchings
    
def all_pair_matchings(fakes_pois):
    scn = bpy.data.scenes['Scene']
    res = "pairs = {"
    for i in range(scn.frame_start, scn.frame_end+1):
        pairs = create_pair_matchings(fakes_pois[i], fakes_pois[i%scn.frame_end+1]).__str__()
        res += "{" + pairs[1:-1] + "},\n"
    return res[:-2] + "};"
    
        
    
    
def export_cloud(filename):
    f = open(filename, 'w')
    ob = bpy.context.selected_objects[0]
    if ob.type == 'MESH':        
        mesh = ob.data
        for vert in mesh.vertices:
            f.write("%0.2f %0.2f %0.2f\n" % (vert.co.x, vert.co.y, vert.co.z))
            
def save(var, name):
    f = open(name, "w")
    f.write(var)
    f.close()

# Use case
if __name__ == "__main__" :
    print("-------------")
    save(matrix_to_matlab(camera_intrinsic()), "intrinsic.m")
    
    (camsf, cams) = export_cameras_matlab()
    #print(camsf)    
    matchsf = create_matchings(cams, 3)
    #print (res)
    save(camsf, "cams.m")
    
    fakes_pois = fake_poi(cams, 1, 40) # FIXME: replace with frame_end
    
   # pairs_matching = create_pair_matchings(fakes_pois[1], fakes_pois[2])
   
    pairs = all_pair_matchings(fakes_pois)
    save(pairs, "blender_pairs.m")
   
    save(matchsf, "matchings.xy")
    
    f = save(export_pois(cams, fakes_pois), "blender_pois.m")
    
    export_cloud("cloud.p3d")
    