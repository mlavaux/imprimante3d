import cv
 
# SURF Feature Descriptor
#
# Programmed by Jay Chakravarty
 
import sys, os
from time import time

try:
    os.mkdir("surf")
except:
    pass

def save(var, name):
    f = open(name, "w")
    f.write(var)
    f.close()
    

pois = "pts = {"
descs = "descs = {"
 
for im in sys.argv[1:]:
    pois += "["
    descs += "["
    cv_image1=cv.LoadImageM(im)
    cv_image1_grey = cv.CreateImage( (cv_image1.width, cv_image1.height), 8, 1 )
    cv.Zero(cv_image1_grey)

    cv.CvtColor(cv_image1, cv_image1_grey, cv.CV_BGR2GRAY);
 
 
    # SURF descriptors
    #param1: extended: 0 means basic descriptors (64 elements each), 1 means extended descriptors (128 elements each)
    #param2: hessianThreshold: only features with hessian larger than that are extracted. good default value is
    #~300-500 (can depend on the average local contrast and sharpness of the image). user can further filter out
    #some features based on their hessian values and other characteristics.
    #param3: nOctaves the number of octaves to be used for extraction. With each next octave the feature size is
    # doubled (3 by default)
    #param4: nOctaveLayers The number of layers within each octave (4 by default)
 
    tt = float(time())    
 
    #SURF for image1
    (keypoints1, descriptors1) = cv.ExtractSURF(cv_image1_grey, None, cv.CreateMemStorage(), (0, 400, 3, 4))
 
    tt = float(time()) - tt
 
    print("SURF time = %g seconds\n" % (tt))
 
    #print "num of keypoints: %d" % len(keypoints)
    #for ((x, y), laplacian, size, dir, hessian) in keypoints:
    #    print "x=%d y=%d laplacian=%d size=%d dir=%f hessian=%f" % (x, y, laplacian, size, dir, hessian)
 
    # draw circles around keypoints in image1
    i = 0
    for ((x, y), laplacian, size, dir, hessian) in keypoints1:
        cv.Circle(cv_image1, (int(x),int(y)), int(size*1.2/9.*2), cv.Scalar(0,0,255), 1, 8, 0)
        
        pois += "[%d %d]' " % (x, y)
        descs += "[%f %f %f %f %s]' " % (laplacian, size, dir, hessian, " ".join(map(str,descriptors1[i])))
        
        i+=1
    print("nb of poi", i)
        
        
 
    #cv.ShowImage("SURF_mvg1", cv_image1)
    cv.SaveImage("surf/"+im, cv_image1)
 
    tt = float(time())  
    
    pois = pois[:-1] + "],\n"  
    descs = descs[:-1] + "],\n"
 
pois = pois[:-2] + "};"
descs = descs[:-2] + "};"

save(pois, "pois.m")
save(descs, "surf_descs.m")
