Acquisition protocol:
- Homogenous background
- Position 2 test patterns horizontaly and 1 vertically
- Put the object in between the 2 horizontal test paterns 
- Take the pictures while revolving around the object, with a maximum angle of
  20° between two shots.
- Be sure that the two horizontal paterns are complete and visible in all the
  shots.

