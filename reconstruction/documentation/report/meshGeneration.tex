% Mesh generation

% Problem
\subsubsection{Problem}

\paragraph{} The input is a 3D cloud and the expected output is a printable mesh. The 
problem of the creation of the mesh is the selection of which points are 
vertices, which points form the edges and which ones form the faces.\\

The algorithm can use the 3D cloud information only but it is also possible 
to use other information such as the objects masks.

\subsubsection{Definitions}

\paragraph{}\textit{Watertight}: A watertight mesh is a closed mesh, that has 
no hole in it. \\

\begin{figure}[!h]
  \begin{center}
    \includegraphics[scale=0.55]{img_mesh/watertight.png}
    \includegraphics[scale=0.55]{img_mesh/nwatertight.png}
    \caption{A watertight and a non watertight mesh}
  \end{center}
\end{figure}

\textit{Manifold}: A manifold mesh is a mesh in which all the edges belong 
to exactly two faces. Being manifold implies being watertight.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[scale=0.55]{img_mesh/manifold.png}
    \includegraphics[scale=0.55]{img_mesh/nmanifold.png}
    \caption{A manifold and a non manifold mesh}
  \end{center}
\end{figure}

% Convex hull -- gift Wrapping algorithm
\subsubsection{Gift wrapping algorithm}

\paragraph{} The Gift wrapping algorithm computes the convex hull of a set of 
points. The simple idea underlying this algorithm is that if you "wrap" a set 
of point you will obtain the convex hull of this cloud.\\

\paragraph{Concept of the algorithm}
\begin{verbatim}
% First point
  On the projection on the XY plane select the point A (whose X coordinate is
  maximal)
% Second point
  B is one of the point leaving all the other point to the left of AB
% Third point
  Then C, with all the points lying to the left of ABC
% And so on...
  Similarly it is possible to find new point forming with an existing 
edge a new correct triangle
% End of the algorithm
  At the end all the creating edges during the algorithm should belong to
two differents faces
\end{verbatim}

\begin{figure}[!h]
  \begin{center}
    \includegraphics[scale=0.55]{img_mesh/gw1.png}
    \includegraphics[scale=0.55]{img_mesh/gw2.png}
    \caption{Exemple of two different steps of the algorithm}
  \end{center}
\end{figure}

\paragraph{Properties:} The mesh  resulting of this algorithm is watertight as 
well as manifold.

\paragraph{Advantages:} In addition of the good properties this algorithm is 
also simple enough to be apprehended by the students.

\paragraph{Drawbacks:} Since the result is the convex hull of the 3D cloud it 
will only be accurate on convex objects and is not convenient for reworking 
the mesh.

\paragraph{Results and limitations:} Since the purpose of the algorithm is 
computing the convex hull of the 3D point cloud the result is neither 
right nor wrong. The algorithm gave the expected results but some 
limitations remain. The first one is that the computation is relatively 
long. This may come from Matlab but also from the fact that the optimisation 
of the code can still be improved. The other limitation is a bug that we found 
after the release. The particular set of point causing this bug can be found 
in the appendix section. In spite of this bug, the code is doing well 
on all the other tested set of points.

% Delaunay -- reprojection
\subsubsection{Delaunay and mask reprojection}

\paragraph{} The Delaunay algorithm is a tetrahedralization. Each tetrahedron 
is such that no point is inside of its circumsphere. This criterion avoids the 
presence of skinny tetrahedron.\\

Since the convex hull is included in the Delaunay tetrahedralization the 
apparent surface is the same in both case. The resulting mesh will be 
watertight but not manifold.

\paragraph{Using reprojection} The idea is to use the inside triangles of the 
Delaunay tetrahedralization and to use the mask in order to dig in the mesh. 
Our criterion to keep a triangle in the mesh is that its barycentre 
reprojection is inside the mask. Therefore this method requires an access to 
the cameras and masks information.

\paragraph{Properties:} The problem is that during the triangles removal 
process there is no insurance that the mesh will remain watertight. The loss 
of this property can lead to some problem during the printing step.

\paragraph{Advantages:} Using a lot of information about the shape of the 
object allows to use a quite simple algorithm. 

\paragraph{Drawbacks:} The Delaunay algorithm used is the one available in 
Matlab, \textit{delaunay}, and we note that even if it is not supposed to
happen, the resulting mesh have some faces that intersect each other. This issue 
gave us trouble  during the post-processing step and made the obtained mesh 
hard to print. This problem may be solved by using another implementation. 
But in case of a Matlab implementation the algorithm might become very slow. 

\paragraph{Results:} Apart from the fact that some faces intersect each other, 
making the mesh pretty hard to print, the results are satisfying. It is
especially noticeable on the rabbit where the result is visually very good.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[scale=0.50]{img_mesh/conv1.png}
    \includegraphics[scale=0.50]{img_mesh/conv2.png}
    \caption{Rabbit before and after digging in the mesh}
  \end{center}
\end{figure}

% Crust
\subsubsection{Crust}

\paragraph{} The Crust algorithm is an algorithm based on the 
three-dimensional Voronoi diagram. Given a good sample there is 
guarantee that the output will be topologically correct and will 
converge to the original surface. 

The actual problem is to have a sample good enough, that is dense on 
topologically complex area.

\paragraph{Properties:} The result is both watertight and manifold.

\paragraph{Advantages:} The main advantage is the impressive results on good 
set of points. Moreover thanks to the convergence to the original surface 
this algorithm is able to deal with all kind of topology. In particular the 
algorithm give correct results on non-convex object.

\paragraph{Drawbacks:} The weakness of the algorithm is its need of a very 
good 3D cloud. The algorithm itself and its implementation are quite complex 
and therefore does not match the practical work criterion.

\paragraph{Results:} The results are strongly related to the quality of the 
3D cloud as illustrated below.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[scale=0.30]{img_mesh/cloud1.png}
    \includegraphics[scale=0.30]{img_mesh/crust1.png}
    \caption{With a very good set of point}
    \includegraphics[scale=0.50]{img_mesh/cloud2.png}
    \includegraphics[scale=0.30]{img_mesh/crust2.png}
    \caption{With data from our pipeline}
  \end{center}
\end{figure}

% Conclusion
\subsubsection{Conclusion}

\paragraph{} The different algorithms cover a wide range of possibilities, 
recapitulated below. The Delaunay algorithm combined with the use of reprojection 
appears to be the most adapted of our meshing algorithm in many cases.\\

\begin{tabular}{|c|c|c|c|c|} \hline
                          & \textbf{Rapid} & \textbf{not only convex} & \textbf{Simple} & \textbf{Robust} \\ \hline
       \textbf{Gift wrap}&                &              &       X       &          X        \\ \hline
       \textbf{Delaunay with reprojection} &                &         X        &       X       &         X          \\ \hline
       \textbf{Crust}&         X         &         X      &            &                   \\ \hline
\end{tabular}
