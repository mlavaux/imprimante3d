% Pairwise matching subsubsection
\subsubsection{Pairwise matching}

After the rectification step, for a point from picture 1, we have an array of candidates from picture 2 (figure~\ref{fig:bunny_restricted_candidates}).
The problem consists to find a way to estimate the similitude between original point and its candidates in order to find the best one (figure~\ref{fig:pairwise_matching}).

\begin{figure}[!h]
  \centering
  \begin{tabular}{cc}
    \includegraphics[width=.5\linewidth]{img_matching/bunny_local_descriptors.png}\\    
  \end{tabular}
  \caption{Green matching seems to be better than red matching}
  \label{fig:pairwise_matching}
\end{figure}

We studied two main approaches : 
\begin{itemize}
	\item Colorimetric comparison
	\item 2-order descriptors comparison
\end{itemize}

\paragraph{Colorimetric comparison}
is based on a basic comparison between the windows defining the points neighborhoods.
The windows size is defining the neighborhood. The default side value is 7 pixels for the square centered on the point of interest.
There are two methods we developped to compare these neighborhoods:
\begin{itemize}
	\item {Root-Mean-Square Error} ($\mathrm{RMSE}$)
	\item {Zero-mean Normal Cross Correlation} ($\mathrm{ZNCC}$)
\end{itemize}
The RMSE method is doing a distance measure between windows $w_1$ and $w_2$:
\begin{equation}
\mathrm{RMSE}(w_1,w_2) = \displaystyle { \sqrt{\frac{\sum_{i=1}^{\mathrm{side}} \sum_{j=1}^{\mathrm{side}} (w_1(i,j) - w_2(i,j))^2}{\mathrm{side}^2}}} 
\end{equation}
The ZNCC method is computing a correlation rate between $w_1$ and $w_2$.
\begin{equation}
\mathrm{ZNCC}(w_1,w_2) = \displaystyle { \frac{\sum_{i}\sum_{j} (w_1(i,j) - \overline{w_1})(w_2(i,j) - \overline{w_2})}{(\sum_{i}\sum_{j} (w_1(i,j) - \overline{w_1})^2 \times \sum_{i}\sum_{j} (w_2(i,j) - \overline{w_2})^2)^{1/2}}}
\label{eq1} 
\end{equation}
The $\mathrm{ZNCC}$ method corresponds to a similarity measure between the windows and is robust to little transformations, whereas $\mathrm{RMSE}$ method is less robust to any transformation and more sensitive from mean-intensity. That fact makes that RMSE is more irregular than ZNCC for colorimetric comparison (figure~\ref{fig:zncc_rmse_error}).


\begin{figure}[!h]
  \centering
  \begin{tabular}{cc}
    \includegraphics[width=.4\linewidth]{img_matching/methods/surf_cmp_rmse.png}&
    \includegraphics[width=.4\linewidth]{img_matching/methods/surf_cmp_zncc.png}\\    
	RMSE method&ZNCC method\\
  \end{tabular}
  \caption{RMSE and ZNCC matching error measures. Blue points have a small error, red points have a high error after matching.}
  \label{fig:zncc_rmse_error}
\end{figure}

\paragraph{SURF descriptors comparison} 
is based on more robust local descriptions of picture, partly inspired by the SIFT descriptor. This method is based on sums of 2D Haar wavelet responses, and is as one of the main references in Computer Vision for object recognition and 3D reconstruction. A SURF local descriptor is a vector containing 64 elements (figure~\ref{fig:cmp_descs}). We implemented two methods in order to compare these descriptors:
\begin{itemize}
	\item {Angle-based method between features descriptors} 
	\item {Zero-mean Normal Cross Correlation} ($\mathrm{ZNCC}$) between descriptors
\end{itemize}
The angle-based method is computing an angle between the arrays constituing the descriptors $d_1$ and $d_2$. This result gives a measure of the distance between features.
\begin{equation}
\mathrm{Angle}(d_1, d_2) = \mathrm{arccos}((d_1|d_2))
\end{equation}
The ZNCC method gives, as explained in previous paragraph, a correlation measure between matrices (arrays in this case). Equation~\eqref{eq1} is actually unchanged, with variable $j$ fixed at 1 and $w_1$ and $w_2$ respectively replaced by $d_1$ and $d_2$. The results (figure~\ref{fig:angle_zncc_error}) highlight the efficience of the ZNCC criteria in comparaison to the angle-based method suggested by David Lowe, the publisher of SIFT algorithm.

\begin{figure}[!h]
  \centering
  \begin{tabular}{c}
    \includegraphics[width=\linewidth]{img_matching/cmp_descs_false.png}\\
    Descriptors from not matching points \& difference between the vectors\\    
    \includegraphics[width=\linewidth]{img_matching/cmp_descs_true.png}\\    
	Descriptors from matching points \& difference between the vectors\\
  \end{tabular}
  \caption{Angle and ZNCC matching error measures with SURF descriptors. Blue points have a small error, red points have a high error after matching.}
  \label{fig:cmp_descs}
\end{figure}

\begin{figure}[!h]
  \centering
  \begin{tabular}{cc}
    \includegraphics[width=.4\linewidth]{img_matching/methods/surf_cmp_descs_angle.png}&
    \includegraphics[width=.4\linewidth]{img_matching/methods/surf_cmp_descs_zncc.png}\\    
	Angle method&ZNCC method\\
  \end{tabular}
  \caption{Angle and ZNCC matching error measures with SURF descriptors. Blue points have a small error, red points have a high error after matching.}
  \label{fig:angle_zncc_error}
\end{figure}

\paragraph{Results}
Figures~\ref{fig:zncc_rmse_error} and~\ref{fig:angle_zncc_error} give a good idea of the global fiability of matchings. A figure with red dominance expresses a bad level of confidence, whereas a blue dominance expresses a high level of confidence. 

A necessary condition to obtain high results is to ensure that the method for points of interest detection is robust to rotation, translation and scale variation. This condition gives us the warranty that detected points globally have their matching point in a close image.

The comparison between figures~\ref{fig:zncc_rmse_error} and~\ref{fig:angle_zncc_error} comforts the supposition that a 2nd order-based descriptor is more robust than a basic colorimetric descriptor. The ZNCC method has exactly the same operating with both kind of descriptors and the level of confidence is obviously higher for SURF descriptors than for colorimetric ones.

It could be interesting to evaluate matchings with more simple kind of 2nd-order descriptors, like the points' Hessian matrices adapted as function of the camera parameters in order to ensure invariance by cameras moving. Lack of time at the end of the project did not enable us to implement this descriptor.
