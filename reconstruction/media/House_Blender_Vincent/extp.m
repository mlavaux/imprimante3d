function res=extp(a,b)
% EXTP corresponds to the cross function
% (extp(a,b) is external product of vectors a,b)
res = cross(a', b');
res = res';

return;
