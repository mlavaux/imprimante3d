#!/usr/bin/python2
# -*- coding: utf-8 -*-

import cv, os, sys

if len(sys.argv) < 2:
    print "Usage: python2 features_points.py <picture>"
    sys.exit(1)

res = "pois = {"

for file in sys.argv[1:]:
    print file
    res += "["
    im=cv.LoadImage(file, cv.CV_LOAD_IMAGE_GRAYSCALE)
    im2=cv.LoadImage(file)
    eig_image = cv.CreateImage(cv.GetSize(im), cv.IPL_DEPTH_32F, 1)
    temp_image = cv.CreateImage(cv.GetSize(im), cv.IPL_DEPTH_32F, 1)
    """
    Parameters:	
        image (CvArr) – The source 8-bit or floating-point 32-bit, single-channel image
        eigImage (CvArr) – Temporary floating-point 32-bit image, the same size as image
        tempImage (CvArr) – Another temporary image, the same size and format as eigImage
        cornerCount (int) – number of corners to detect
        qualityLevel (float) – Multiplier for the max/min eigenvalue; specifies the minimal accepted quality of image corners
        minDistance (float) – Limit, specifying the minimum possible distance between the returned corners; Euclidian distance is used
        mask (CvArr) – Region of interest. The function selects points either in the specified region or in the whole image if the mask is NULL
        blockSize (int) – Size of the averaging block, passed to the underlying CornerMinEigenVal or CornerHarris used by the function
        useHarris (int) – If nonzero, Harris operator ( CornerHarris ) is used instead of default CornerMinEigenVal
        k (float) – Free parameter of Harris detector; used only if ( useHarris != 0 )
    """
    pts = cv.GoodFeaturesToTrack(im, eig_image, temp_image, 500, 0.0001, 10, None, 2, True, 0.001)
    print len(pts), "points found"
    for (x,y) in pts:
        res += "[%f %f]' " % (x,y)
        cv.Circle(im2, (int(x),int(y)), 3, (0, 0, 255))
    cv.ShowImage("", im2)

    print "Saving ", "new_"+file
    cv.SaveImage("new_"+os.path.basename(file), im2)
    #os.system("eog mod.png")
    
    res += "] , "
res = res[:-2] + "};"
f = open("poi.m", "w")
f.write(res)
f.close()
