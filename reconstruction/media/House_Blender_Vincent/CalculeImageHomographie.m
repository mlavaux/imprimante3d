function [imout1,imout2,PoIout1,PoIout2]=CalculeImageHomographie(H1,H2,imin1,imin2,PoIin1,PoIin2)
%
%@Gael Michelin
%La fonction calcule les images imout<i> � partir des images imin<i> et des
%homographies H<i> entre les images d'entree et sortie t.q. imout= H*imin
%imout<i> : image rectifiee de imin<i> 
%hautout,largout= dimensions de l'image de sortie
%imin= image d'entr�e
%hautin,largin= dimensions de l'image d'entr�e


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[hautin,largin,~]=size(imin1);
hautout=hautin;
largout=largin;
imout1=uint8(zeros(size(imin1)));
imout2=uint8(zeros(size(imin1)));
PoIout1=[];
PoIout2=[];
inv1=false;
inv2=false;

%Coordonnees des extremites de l'image 1 apres rectification
m11=H1*[1;1;1];
m12=H1*[1;hautin;1];
m21=H1*[largin;1;1];
m22=H1*[largin;hautin;1];
m11=m11(1:2)/m11(3);
m12=m12(1:2)/m12(3);
m21=m21(1:2)/m21(3);
m22=m22(1:2)/m22(3);
%Abscisses et ordonnees extremales de l'image rectifiee 1
xmin1=min(m11(1),min(m12(1),min(m21(1),m22(1))));
ymin1=min(m11(2),min(m12(2),min(m21(2),m22(2))));
xmax1=max(m11(1),max(m12(1),max(m21(1),m22(1))));
ymax1=max(m11(2),max(m12(2),max(m21(2),m22(2))));
if m11(1)>m21(1)
	inv1=true;	%l'image 1 rectifiee est inversee selon l'axe vertical
end

%Coordonnees des extremites de l'image 2 apres rectification
n11=H2*[1;1;1];
n12=H2*[1;hautin;1];
n21=H2*[largin;1;1];
n22=H2*[largin;hautin;1];
n11=n11(1:2)/n11(3);
n12=n12(1:2)/n12(3);
n21=n21(1:2)/n21(3);
n22=n22(1:2)/n22(3);
%Abscisses et ordonnees extremales de l'image rectifiee 2
xmin2=min(n11(1),min(n12(1),min(n21(1),n22(1))));
ymin2=min(n11(2),min(n12(2),min(n21(2),n22(2))));
xmax2=max(n11(1),max(n12(1),max(n21(1),n22(1))));
ymax2=max(n11(2),max(n12(2),max(n21(2),n22(2))));
if n11(1)>n21(1)
	inv2=true;	%l'image 2 rectifiee est inversee selon l'axe vertical
end

%Calcul des translations pour recentrer les images 
xrect1=floor(1/2*(-largout+xmax1+xmin1-1));
xrect2=floor(1/2*(-largout+xmax2+xmin2-1));
yrect1=floor(1/2*(-hautout+ymax1+ymin1-1));
yrect2=floor(1/2*(-hautout+ymax2+ymin2-1));
%Attention : necessite d'avoir la meme translation verticale
yrect=floor(1/2*(yrect1+yrect2));

H1inv=inv(H1);
H2inv=inv(H2);

%Calcul des nouvelles images
for y=1:hautout
    for x=1:largout
        p21=H1inv*[x+xrect1,y+yrect,1]'; %transformation homographique inverse 1
        p22=H2inv*[x+xrect2,y+yrect,1]'; %transformation homographique inverse 2
        ud1=round(p21(1)/p21(3));
        vd1=round(p21(2)/p21(3));
        ud2=round(p22(1)/p22(3));
        vd2=round(p22(2)/p22(3));
        if inv1
        	x1=largout-x+1;
        else
        	x1=x;
        end
        if inv2
        	x2=largout-x+1;
        else
        	x2=x;
		end
		
        if vd1>0 & ud1>0 & vd1<=hautin & ud1<=largin	%image 1
            %ca tombe dans l'image
            val1=imin1(vd1,ud1,1);
            imout1(y,x1,1)=val1; %on attribue la couleur du pixel concerne

            val2=imin1(vd1,ud1,2);
            imout1(y,x1,2)=val2; %on attribue la couleur du pixel concerne

            val3=imin1(vd1,ud1,3);

            imout1(y,x1,3)=val3; %on attribue la couleur du pixel concerne

        else
            %ca tombe hors de l'image datamatrix, on laisse la valeur du
            %fond
            %eventuellement, on aurait pu faire:
%            imout1(y,x)=255; %on attribue la couleur blanche
        end

        if vd2>0 & ud2>0 & vd2<=hautin & ud2<=largin	%image 2
            %ca tombe dans l'image
            val1=imin2(vd2,ud2,1);
            imout2(y,x2,1)=val1; %on attribue la couleur du pixel concerne

            val2=imin2(vd2,ud2,2);
            imout2(y,x2,2)=val2; %on attribue la couleur du pixel concerne

            val3=imin2(vd2,ud2,3);

            imout2(y,x2,3)=val3; %on attribue la couleur du pixel concerne

        else
            %ca tombe hors de l'image datamatrix, on laisse la valeur du
            %fond
            %eventuellement, on aurait pu faire:
%            imout2(y,x)=255; %on attribue la couleur blanche
        end
    end
end

%Calcul des nouvelles coordonnees des points d'interet
if nargin == 6

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%    			GROS WARNING				 %       
        %	Pour garder les indices (identifiants)	 %
		%	des points d'interet, on ne supprime pas %
		%	les coordonnees sortant de l'image apres %
		%	rectification de l'image !				 %
		%	Il faudra tester si le PoI est bien dans %
		%	l'image avant de faire l'exploitation.	 %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%PoI image 1 :
	for i=1:size(PoIin1,2)
		x=PoIin1(1,i);
		y=PoIin1(2,i);
       	p21=H1*[x,y,1]'; %transformation homographique directe 1
        ud1=round(p21(1)/p21(3))-xrect1;
        vd1=round(p21(2)/p21(3))-yrect;
        if inv1   
			PoIout1=[PoIout1, [largout-ud1+1;vd1]];
		else
			PoIout1=[PoIout1, [ud1;vd1]];	
		end
	end
	
	%PoI image 2 :
	for i=1:size(PoIin2,2)
		x=PoIin2(1,i);
		y=PoIin2(2,i);
       	p22=H2*[x,y,1]'; %transformation homographique directe 1
        ud2=round(p22(1)/p22(3))-xrect2;
        vd2=round(p22(2)/p22(3))-yrect;
        if inv2
			PoIout2=[PoIout2, [largout-ud2+1;vd2]];
		else
			PoIout2=[PoIout2, [ud2;vd2]];
		end	
	end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
