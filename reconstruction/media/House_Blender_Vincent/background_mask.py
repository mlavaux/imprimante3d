import bpy
from os.path import dirname
import glob

def export_mask(im):
    res = "background_mask = ["
    
    for i in range(im.size[1]):
        for j in range(im.size[0]):
            background = round( im.pixels[3+4*(j+i*im.size[0])] )
            res += "%d " % background
        res += ";\n"
    
    f = open("background.m", "w")
    f.write(res[:-2]+"];")
    f.close()
    

filepath = dirname(bpy.data.filepath)
renderpath = bpy.data.scenes["Scene"].render.filepath

#bpy.ops.render.render(animation=True)

ext = bpy.data.scenes["Scene"].render.image_settings.file_format.lower()
files = glob.glob(filepath+renderpath+"/*."+ext)

im = bpy.data.images.load(files[0])
export_mask(im)
#for file in files:
#    im = bpy.data.images.load(file)



     
        
                
        
        