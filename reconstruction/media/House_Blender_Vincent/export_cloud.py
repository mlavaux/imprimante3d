#-*- coding: utf8 -*-

import bpy

def main(filename):
    f = open(filename, 'w')
    ob = bpy.context.selected_objects[0]
    if ob.type == 'MESH':        
        mesh = ob.data
        for vert in mesh.vertices:
            f.write("%0.2f %0.2f %0.2f\n" % (vert.co.x, vert.co.y, vert.co.z))
            
                
main("mesh.p3d")
