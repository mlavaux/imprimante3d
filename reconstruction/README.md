# 3D Reconstruction

## Structure

### documentation
Slides, report and user documentation

### prototype
Source code of the prototype

### src
Source code and unitary tests of the project

#### 3D_Cloud_Creation 
3D cloud creation from the matchings

#### Background_processing
Segment the background

#### Calcul_PoI
Compute feature points with Harris

#### Calibration
Compute intrinsic and extrinsic camera parameters using CCTags

#### Calibration-ng
New generation calibration code which handles automatic CCTag referencement using colors

#### Common
Common functions

#### Matching_PoI
Match the feature points computed on the different pictures

#### Mesh_Creation
Compute a mesh from a 3D point cloud and export it into OBJ
Two algorithms : CRUST and Delaunay + carving

### test
Integration tests, and scripts to generate tests data

#### data_generation

##### surf.py
Compute surf descriptors using the Python binding of OpenCV

##### blender
Script to generate 3D reconstruction test data from computer graphic scenes
