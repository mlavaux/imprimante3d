clear all; close all;

%type_signature à modifier dans test_3_images si tu veux changer le type de signature...
test_3_images;



indices=find(index_matchings(:,1) & index_matchings(:,2) & index_matchings(:,3));
subindices=find(index_matchings(indices,1)~=index_matchings(indices,2) | index_matchings(indices,2)~=index_matchings(indices,3));

index_matching_errors=index_matchings(indices(subindices),:);

cross_indices=find(~(index_matchings(:,1) & index_matchings(:,2) & index_matchings(:,3)));
cross_subindices=find((index_matchings(cross_indices,1)~=index_matchings(cross_indices,2) & index_matchings(cross_indices,1) & index_matchings(cross_indices,2) ) | (index_matchings(cross_indices,2)~=index_matchings(cross_indices,3) & index_matchings(cross_indices,2) & index_matchings(cross_indices,3) ) | (index_matchings(cross_indices,1)~=index_matchings(cross_indices,3) & index_matchings(cross_indices,1) & index_matchings(cross_indices,3) ));

index_matching_cross_errors=index_matchings(cross_indices(cross_subindices),:);

index_matching_errors
index_matching_cross_errors

triangular_falses=size(subindices,1)/size(index_matchings,1)
cross_falses=size(cross_subindices,1)/size(index_matchings,1)

total_errors_rate=triangular_falses+cross_falses
success_rate=1-total_errors_rate

