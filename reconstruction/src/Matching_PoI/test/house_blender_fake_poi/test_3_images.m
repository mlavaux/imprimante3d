clear all;
close all;

addpath('../../../../media/House_Blender_Vincent/fake_render2/');
addpath('../../src');

cams
fake_poi;

%roof


images_origine = {};
images_origine{1} = imread('../../../../media/House_Blender_Vincent/fake_render2/0001.jpg');
images_origine{2} = imread('../../../../media/House_Blender_Vincent/fake_render2/0002.jpg');
images_origine{3} = imread('../../../../media/House_Blender_Vincent/fake_render2/0003.jpg');

type_signature=-2	%you can change the type_signature value to test a different "distance"
					%currently availables : rmse (-1) | zncc (-2)
index_matchings = matching(images_origine, pois, cameras, type_signature);	



for j = 1:length(images_origine)
    figure (j);
    hold on;
    imagesc(images_origine{j});
    axis ij;
    for i = 1:size(index_matchings, 1)
        index = index_matchings(i,j);
        if length(find(index_matchings(i,:)==0)) > 0
            color = 'rx';
        else
            color = 'gx';
        end
        if index > 0
            plot(pois{j}(1,index),pois{j}(2,index), color);
        end
    end
    hold off;
end

display(sprintf('%f%% de matchings retrouvés.', 100*size(index_matchings, 1)/size(pois{1},2)));

[x,y] = find(index_matchings == 0);
display(sprintf('%f%% de matchings incomplets.',  100*length(unique(x))/size(index_matchings, 1)));

addpath('../../../Calcul_PoI/src');
addpath('../../../Calibration/src/');
addpath('../../../Calibration/src/bin');
addpath('../../../3D_Cloud_Creation/src');
addpath('../../../Matching_PoI/src');
addpath('../../../Mesh_Creation/src');

matchings = export_matchings(index_matchings, pois);

%% --- Points Cloud

display('Creating the points cloud ...');

p = calc_3D_script(matchings, cameras);


%% --- Reconstruction

display('Generating a mesh ...');

[t,tnorm]=MyRobustCrust(p);
visuMesh;
