clear all;
close all;

addpath('../../../../media/House_Blender_Vincent/fake_render/');
addpath('../../src');
addpath('../../../3D_Cloud_Creation/src');
addpath('../../../Mesh_Creation/src');

cams
fake_poi
%roof

folder = '../../../../media/House_Blender_Vincent/fake_render';
images_origine = {}
D = dir(strcat(folder, '/*.jpg'));
imcell = cell(1, numel(D));
for i = 1:numel(D)
  images_origine{i} = imread(strcat(strcat(folder, '/'), D(i).name));
end

type_signature=-1;	%you can change the type_signature value to test a different "distance"
					%currently availables : rmse (-1) | zncc (-2)
index_matchings = matching(images_origine, pois, cameras, type_signature);	


for j = 1:length(images_origine)
    figure (j);
    hold on;
    imagesc(images_origine{j});
    axis ij;
    for i = 1:size(index_matchings, 1)
        index = index_matchings(i,j);
        if length(find(index_matchings(i,:)==0)) > 0
            color = 'rx';
        else
            color = 'gx';
        end
        if index > 0
            plot(pois{j}(1,index),pois{j}(2,index), color);
        end
    end
    hold off;
end

display(sprintf('%f%% de matchings retrouvés.', 100*size(index_matchings, 1)/size(pois{1},2)));

[x,y] = find(index_matchings == 0);
display(sprintf('%f%% de matchings incomplets.',  100*length(unique(x))/size(index_matchings, 1)));

matchings = export_matchings(index_matchings, pois);



%% --- Points Cloud

display('Creating the points cloud ...');

p = calc_3D_script(matchings, cameras);


%% --- Reconstruction

display('Generating a mesh ...');

[t,tnorm]=MyRobustCrust(p);
visuMesh;

