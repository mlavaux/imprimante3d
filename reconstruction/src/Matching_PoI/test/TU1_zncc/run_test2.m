

function [ok err] = run_test2(verbose)

  addpath ('../../src');
  
  test_name = 'M5_TU1.2';
  purpose = 'Proportional windows give null error';
  
  ok = true;
  err = 0;
  
  % float precision (quite tolerant)
  eps = 1e-6;

  if(nargin < 1)
    verbose = false;
  end
  

  if verbose
    disp(sprintf('%s : Starting the Test. Purpose : purpose', test_name, purpose));
  end
  % INIT VALUES 

  w = randint(6, 6, [0, 255]);
  k = rand(1)+eps; % k > 0

  % Execute script
  if(verbose)
    disp(sprintf('%s : Running', test_name));
  end

  res = zncc(w, k*w);

  if(verbose)
    disp(sprintf('%S : Done', test_name));
  end


  
  err = abs(res - 0);
  
  if(err > eps)
    ok = false;
  end

 

  % END
 if ok 
    if verbose
        disp(sprintf('%s : Test Passed', test_name));
    end
 else
    % Verbose ou pas, il faut signaler les erreurs
    disp(sprintf('%s : Test Failed', test_name));
 end

  
return
