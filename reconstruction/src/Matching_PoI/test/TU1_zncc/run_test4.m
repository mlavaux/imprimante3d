

function [ok err] = run_test4(verbose)

  addpath ('../../src');
  
  test_name = 'M5_TU1.4';
  purpose = 'Unified windows give high errors';
  
  ok = true;
  err = 0;
  
  % float precision (quite tolerant)
  eps = 1e-6;

  if(nargin < 1)
    verbose = false;
  end
  

  if verbose
    disp(sprintf('%s : Starting the Test. Purpose : purpose', test_name, purpose));
  end
  % INIT VALUES 


  % Execute script
  if(verbose)
    disp(sprintf('%s : Running', test_name));
  end

  res = zncc(ones(2,2), eye(2));

  if(verbose)
    disp(sprintf('%S : Done', test_name));
  end

  
  err = abs(res - 2);
  
  if(err > eps)
    ok = false;
  end

 

  % END
 if ok 
    if verbose
        disp(sprintf('%s : Test Passed', test_name));
    end
 else
    % Verbose ou pas, il faut signaler les erreurs
    disp(sprintf('%s : Test Failed', test_name));
 end

  
return
