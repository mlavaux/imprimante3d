

function [ok err] = run_test5(verbose)

    addpath ('../../src');

    test_name = 'M5_TU1.5';
    purpose = 'Quand on augmente de façon aléatoire la différence entre les fenêtres, on augmente pas systématiquement l erreur';

    ok = true;
    err = 0;

    % float precision (quite tolerant)
    eps = 1e-6;

    if(nargin < 1)
        verbose = false;
    end


    if verbose
        disp(sprintf('%s : Starting the Test. Purpose : %s', test_name, purpose));
    end
    % INIT VALUES 


    w = [ 192   140   208   157   234    19 ; ...
            65    35    62   121    73    13 ; ...
           129    38   237    90   193   135 ; ...
           178    65    89   212   192   199 ; ...
           228   215    50   149    97   239 ; ...
           245    65    64   140   145    33];
       
    w2 = w;
    
    znccs = [zncc(w,w2)];

    for i=1:1000
        i = randint(1, 1,[1,6]);
        j = randint(1, 1,[1,6]);
        w2(i,j) = w2(i,j) + randint(1, 1,[-10,10]);
        znccs = [znccs zncc(w,w2)]; 
    end

    % Execute script
    if(verbose)
        disp(sprintf('%s : Running', test_name));
    end


    if(verbose)
        disp(sprintf('%S : Done', test_name));
    end

    % Check we have a monotone increasing sequence
    if( all(diff(znccs) >= 0) )
        ok = false;
        err = Inf;
    end

    % END
    if ok 
        if verbose
            disp(sprintf('%s : Test Passed', test_name));
        end
    else
        % Verbose ou pas, il faut signaler les erreurs
        disp(sprintf('%s : Test Failed', test_name));
    end

  
return
