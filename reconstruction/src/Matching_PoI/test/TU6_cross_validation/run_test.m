%TU6 : cross_validation.m

function [ok] = run_test(verbose,display_param,run_all)

if nargin < 3
  run_all = false;
end
if(nargin < 2)
  display_param = false;
end
if(nargin < 1)
  verbose = false;
end
if ~run_all
  addpath('../../src');
end

ok	= true;
err	= 0;

load images_origine.mat;	%load original pictures 1 & 2
load images_rect.mat;		%load rectified images 1 & 2
load pois.mat;				%load rectified pois of images 1 & 2
load signatures.mat;		%load signatures corresponding to pois{1} and pois{2}

test_name = 'M5_TU6';
purpose = 'Correct execution of cross_validation.m : eliminates false matchings and preserve good ones';
if verbose
    disp(sprintf('%s : Starting the Test. Purpose : %s', test_name, purpose));
end

%TU6-1 : type_sig=1 (SURF with zncc comparison)
type_sig=1;
im1=[];
im2=[];
if display_param
	im1 = images_rect{1};
	im2 = images_rect{2};
end

ruban_half_heigth = 2;	%default value

		%TU6-1-1 : Classic case
		test_name = 'M5_TU6-1-1';
		y1 = pois{1}(2,1);
		indices2=find(pois{2}(2,:)>=y1-ruban_half_heigth & pois{2}(2,:)<=y1+ruban_half_heigth);	%N=length(indices) 
		y2 = pois{2}(2,1);
		indices1=find(pois{1}(2,:)>=y2-ruban_half_heigth & pois{1}(2,:)<=y2+ruban_half_heigth);	%N=length(indices) 
		poi1 = pois{1}(:,indices1);
		sig1 = signatures{1}(:,indices1);
		poi2 = pois{2}(:,indices2);
		sig2 = signatures{2}(:,indices2);
		epsilon=Inf;
		[table_matched_poi21, recommended_epsilon21]=matching_poi(poi2,poi1,type_sig,sig2,sig1,im2,im1,display_param,epsilon);
		[table_matched_poi12, recommended_epsilon12]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param,epsilon);
		assert(isequal(size(table_matched_poi12), size(indices1)), 'The size of table_matched_poi12 is not the one expected');
		assert(isequal(size(table_matched_poi21), size(indices2)), 'The size of table_matched_poi21 is not the one expected');
		assert(isequal(length(table_matched_poi12{1}{1}),length(indices2)) && isequal(length(table_matched_poi12{1}{2}),length(indices2)), 'Assertion failed : problem in matching_poi.m');
		candidate12=table_matched_poi12{1}{1}(1);
		candidate21=table_matched_poi21{1}{1}(1);
		assert(isequal(candidate12,candidate21),'Assertion failed : bad calculation of distances in matching_poi.m');		
		matching=cross_validation(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param);
		assert(~isempty(matching) && ~isempty(matching{1}));
		res_cross=matching{1};
		TU_ok = isequal(res_cross,[candidate12, candidate21]);
		if TU_ok
	   		if verbose
	   		    disp(sprintf('%s : Test Passed', test_name));
		 	end
		else
			ok = false;
			disp(sprintf('%s : Test Failed', test_name));
		end

		%TU6-1-2 : Rejection case
		test_name = 'M5_TU6-1-2';
		y1 = pois{1}(2,5);
		indices2=find(pois{2}(2,:)>=y1-ruban_half_heigth & pois{2}(2,:)<=y1+ruban_half_heigth);	%N=length(indices) 
		y2 = y1;
		indices1=find(pois{1}(2,:)>=y2-ruban_half_heigth & pois{1}(2,:)<=y2+ruban_half_heigth);	%N=length(indices) 
		poi1 = pois{1}(:,indices1);			% first poi of poi1 is not corresponding to any poi of poi2
		sig1 = signatures{1}(:,indices1);
		poi2 = pois{2}(:,indices2);
		sig2 = signatures{2}(:,indices2);
		epsilon=Inf;
		[table_matched_poi21, recommended_epsilon21]=matching_poi(poi2,poi1,type_sig,sig2,sig1,im2,im1,display_param,epsilon);
		[table_matched_poi12, recommended_epsilon12]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param,epsilon);
		% if matching_poi.m and the distance calculus are ok, the best candidate for poi1(:,1) should not have poi1(:,1) as his best candidate
		assert(isequal(size(table_matched_poi12), size(indices1)), 'The size of table_matched_poi12 is not the one expected');
		assert(isequal(size(table_matched_poi21), size(indices2)), 'The size of table_matched_poi21 is not the one expected');
		assert(isequal(length(table_matched_poi12{1}{1}),length(indices2)) && isequal(length(table_matched_poi12{1}{2}),length(indices2)), 'Assertion failed : problem in matching_poi.m');
		candidate12=table_matched_poi12{1}{1}(1);
		candidate21=table_matched_poi21{1}{1}(candidate12);
		assert(~isequal(candidate21,1),'Assertion failed : bad calculation of distances in matching_poi.m');		
		matching=cross_validation(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param);
		TU_ok = isempty(find(cellfun(@(t) isequal(t(1),1), matching)));
		if TU_ok
	   		if verbose
	   		    disp(sprintf('%s : Test Passed', test_name));
		 	end
		else
			ok = false;
			disp(sprintf('%s : Test Failed', test_name));
		end

		
