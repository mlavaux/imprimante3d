

function [ok err] = run_test0(verbose)

    addpath ('../../src');

    test_name = 'M5_TU0.0';
    purpose = 'Validity with random camera matrices';

    ok = true;
    err = 0;

    % float precision (quite tolerant)
    eps = 1e-6;

    if(nargin < 1)
        verbose = false;
    end


    if verbose
        disp(sprintf('%s : Starting the Test. Purpose : %s', test_name, purpose));
    end
    % INIT VALUES 


    c1 = [...
      -2100.00       -899.71        334.83      -3269.71 ; ...
             0        226.36       2156.47      -4827.86 ; ...
             0         -0.94          0.35         -3.41 ; ...
    ];

    c2 = [...
      -2214.89       -560.07        334.93      -3269.07 ; ...
         35.45        223.80       2156.45      -4827.39 ; ...
         -0.15         -0.93          0.35         -3.41 ; ...
         ];
     
    T1_expected = [...
        -0.998693368291544       -0.0315457366333569          1156.79083958456 ; ...
        0.0315260958341724         -1.00254713179901          508.388808949779 ; ...
     -4.17062197771874e-05     -9.73365271530883e-07        -0.952690164542205 ; ...
    ];

    T2_expected = [...
          1.00203642982576       -0.0236986971087827         -819.896756380307 ; ...
       -0.0241945327357922         -1.00255352839942          561.683381874543 ; ...
      2.95940926331185e-05     -9.43215672734738e-07         -1.02109438906573 ; ...
    ];

    Pn1_expected = [...
          2097.25607341224         -195.989671768225          2.45685867179926    -526.92066968407 ; ...
         -66.2048012517621          -733.18639284978         -1973.47084751004    3003.47016639848 ; ...
        0.0875830615320935         0.932831926702542        -0.349505084164864    3.3897399762164 ; ...
               ];

    Pn2_expected = [...
       -2097.25607341224          195.989671768226         -2.45685867179957         -365.47643895769 ; ...
         -66.2048012517621          -733.18639284978         -1973.47084751004       3003.47016639848 ; ...
        0.0875830615320935         0.932831926702542        -0.349505084164864       3.3897399762164 ; ... 
    ];

    % Execute script
    if(verbose)
        disp(sprintf('%s : Running', test_name));
    end

     [T1,T2,Pn1,Pn2]=rectify(c1, c2);

     err1 = norm(T1_expected - T1);
     err2 = norm(T2_expected - T2);
     err3 = norm(Pn1_expected - Pn1);
     err4 = norm(Pn2_expected - Pn2);
     
    if(verbose)
        disp(sprintf('%S : Done', test_name));
    end



    err = max([err1, err2, err3, err4]);

    if(err > eps)
        ok = false;
    end



    % END
    if ok 
        if verbose
            disp(sprintf('%s : Test Passed', test_name));
        end
    else
        % Verbose ou pas, il faut signaler les erreurs
        disp(sprintf('%s : Test Failed', test_name));
    end

  
return
