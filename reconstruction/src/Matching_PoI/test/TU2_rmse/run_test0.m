

function [ok err] = run_test0(verbose)

    addpath ('../../src');

    test_name = 'M5_TU2.0';
    purpose = 'Validity with random grayscale windows';

    ok = true;
    err = 0;

    % float precision (quite tolerant)
    eps = 1e-6;

    if(nargin < 1)
        verbose = false;
    end


    if verbose
        disp(sprintf('%s : Starting the Test. Purpose : %s', test_name, purpose));
    end
    % INIT VALUES 


    w1 = [ 192   140   208   157   234    19 ; ...
            65    35    62   121    73    13 ; ...
           129    38   237    90   193   135 ; ...
           178    65    89   212   192   199 ; ...
           228   215    50   149    97   239 ; ...
           245    65    64   140   145    33];

    w2 = [  46   140   102   106    86    61 ; ...
            67    37    19    12   230   103 ; ...
            37   218    61   231    94    24 ; ...
            34   159    31   241    28    33 ; ...
           222    89    47   125   199   241 ; ...
           148   131    61   125    99   244];

    % Execute script
    if(verbose)
        disp(sprintf('%s : Running', test_name));
    end

    res = rmse(w1, w2);

    if(verbose)
        disp(sprintf('%S : Done', test_name));
    end



    % Rejection check

    err = abs(res - 9.160629184217914);

    if(err > eps)
        ok = false;
    end



    % END
    if ok 
        if verbose
            disp(sprintf('%s : Test Passed', test_name));
        end
    else
        % Verbose ou pas, il faut signaler les erreurs
        disp(sprintf('%s : Test Failed', test_name));
    end

  
return
