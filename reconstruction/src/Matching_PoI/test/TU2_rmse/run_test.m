function ok = run_test(verb)
if nargin < 1
  verb = false;
end

ok =  run_test0(verb) && run_test1(verb) &&  run_test2(verb) &&   run_test3(verb);

end

