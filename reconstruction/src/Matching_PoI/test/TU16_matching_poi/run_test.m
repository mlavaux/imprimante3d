%TU16 : matching_poi.m

function [ok] = run_test(verbose,display_param,run_all)

if nargin < 3
  run_all = false;
end
if(nargin < 2)
  display_param = false;
end
if(nargin < 1)
  verbose = false;
end
if ~run_all
  addpath('../../src');
end

ok	= true;
err	= 0;

load images_origine.mat;	%load original pictures 1 & 2
load images_rect.mat;		%load rectified images 1 & 2
load pois.mat;				%load rectified pois of images 1 & 2
load signatures.mat;		%load signatures corresponding to pois{1} and pois{2}

signatures_ordre_0 = cell(length(pois));
window_half_size = 3;	% default value defining the neighborhood
for i=1:2
	[y_max, x_max]=size(images_origine{i});
	sig=[];
	for k=1:size(pois{i},2)
		c = pois{i}(:,k);	% c = [x ; y]
		if c(1)-window_half_size>=1 && c(1)+window_half_size<=x_max && c(2)-window_half_size>=1 && c(2)+window_half_size<=y_max
			% The window is entierly inside the picture
			w = images_origine{i}(c(2)-window_half_size:c(2)+window_half_size,c(1)-window_half_size:c(1)+window_half_size,:);
			sig = [sig, w(:)];
		else 
			% The window is too much close to the borders of the picture
			sig = [sig, - ones(3*(1+2*window_half_size),1)];
		end
	end
	signatures_ordre_0{i}=sig;
end	


test_name = 'M5_TU16';
purpose = 'Correct execution of matching_poi.m even with extremal inputs';
if verbose
    disp(sprintf('%s : Starting the Test. Purpose : %s', test_name, purpose));
end

%TU16-1 : tests for SURF descriptors
	im1=[];
	im2=[];
	if display_param
		im1 = images_rect{1};
		im2 = images_rect{2};
	end
	type_sig 	= 1;	%SURF compared with ZNCC
	%TU16-1-1 : 0 poi in one of the pictures
		%TU16-1-1-1 : poi1=[];
		test_name = 'M5_TU16-1-1-1';
		poi1=[];
		poi2=pois{2};
		sig1=[];
		sig2=signatures{2};
		[table_matched_poi, recommended_epsilon]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param);
		TU_ok = isequal(size(table_matched_poi),[0 0]) && isequal(recommended_epsilon,0);
		if TU_ok
    		if verbose
    		    disp(sprintf('%s : Test Passed', test_name));
   		 	end
 		else
 			ok = false;
		    disp(sprintf('%s : Test Failed', test_name));
		end

		%TU16-1-1-2 : poi2=[];
		test_name = 'M5_TU16-1-1-2';
		poi1=pois{1};
		poi2=[];
		sig1=signatures{1};
		sig2=[];
		[table_matched_poi, recommended_epsilon]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param);
		TU_ok = isequal(size(table_matched_poi),[0 0]) && isequal(recommended_epsilon,0);
		if TU_ok
    		if verbose
    		    disp(sprintf('%s : Test Passed', test_name));
   		 	end
 		else
  			ok = false;
		    disp(sprintf('%s : Test Failed', test_name));
		end
	
	%TU16-1-2 : 0 poi in the ruban containing the candidates for poi1(:,1)
	test_name = 'M5_TU16-1-2';
	epsilon = Inf;
	ruban_half_heigth = 1;
	poi1 = pois{1}(:,1);
	sig1 = signatures{1}(:,1);
	y = poi1(2,1);
	indices=find(pois{2}(2,:)<y-ruban_half_heigth | pois{2}(2,:)>y+ruban_half_heigth);	%extraction of poi2 which are out of the ruban 
	poi2 = pois{2}(:,indices);
	sig2 = signatures{2}(:,indices);
	[table_matched_poi, recommended_epsilon]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param,epsilon,ruban_half_heigth);
	assert(isequal(size(table_matched_poi), [1 1]), 'The size of table_matched_poi is not the one expected : probably a wrong parameter poi1');
	TU_ok = isempty(table_matched_poi{1}{1}) && isempty(table_matched_poi{1}{2}) ;
	if TU_ok
   		if verbose
   		    disp(sprintf('%s : Test Passed', test_name));
	 	end
	else
		ok = false;
	    disp(sprintf('%s : Test Failed', test_name));
	end
	
	%TU16-1-3 : N poi in the ruban containing the candidates for poi1(:,1)
		%TU16-1-3-1 : epsilon=0 : no matchings expected
		test_name = 'M5_TU16-1-3-1';
		epsilon = 0;
		ruban_half_heigth = 1;
		poi1 = pois{1}(:,1);
		sig1 = signatures{1}(:,1);
		y = poi1(2,1);
		indices=find(pois{2}(2,:)>=y-ruban_half_heigth & pois{2}(2,:)<=y+ruban_half_heigth);	%N=length(indices) 
		poi2 = pois{2}(:,indices);
		sig2 = signatures{2}(:,indices);
		[table_matched_poi, recommended_epsilon]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param,epsilon,ruban_half_heigth);
		assert(isequal(size(table_matched_poi), [1 1]), 'The size of table_matched_poi is not the one expected : probably a wrong parameter poi1');
		TU_ok = isempty(table_matched_poi{1}{1}) && isempty(table_matched_poi{1}{2}) ;
		if TU_ok
	   		if verbose
	   		    disp(sprintf('%s : Test Passed', test_name));
		 	end
		else
			ok = false;
			disp(sprintf('%s : Test Failed', test_name));
		end

		%TU16-1-3-2 : epsilon=Inf : N matchings expected
		test_name = 'M5_TU16-1-3-2';
		epsilon = Inf;
		ruban_half_heigth = 1;
		poi1 = pois{1}(:,1);
		sig1 = signatures{1}(:,1);
		y = poi1(2,1);
		indices=find(pois{2}(2,:)>=y-ruban_half_heigth & pois{2}(2,:)<=y+ruban_half_heigth);	%N=length(indices) 
		poi2 = pois{2}(:,indices);
		sig2 = signatures{2}(:,indices);
		[table_matched_poi, recommended_epsilon]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param,epsilon,ruban_half_heigth);
		assert(isequal(size(table_matched_poi), [1 1]), 'The size of table_matched_poi is not the one expected : probably a wrong parameter poi1');
		TU_ok = isequal(length(table_matched_poi{1}{1}),length(indices)) && isequal(length(table_matched_poi{1}{2}),length(indices));
		if TU_ok
	   		if verbose
	   		    disp(sprintf('%s : Test Passed', test_name));
		 	end
		else
			ok = false;
			disp(sprintf('%s : Test Failed', test_name));
		
		end
	
	%TU16-1-4 : 2 superposed pois after rectification
		%TU16-1-4-1 : superposed in image 1
		test_name = 'M5_TU16-1-4-1';
		epsilon = Inf;
		ruban_half_heigth = 1;
		poi1 = [pois{1}(:,1), pois{1}(:,1)];
		sig1 = [signatures{1}(:,1), signatures{1}(:,2)];
		y = poi1(2,1);
		indices=find(pois{2}(2,:)>=y-ruban_half_heigth & pois{2}(2,:)<=y+ruban_half_heigth);	
		poi2 = pois{2};
		sig2 = signatures{2};
		[table_matched_poi, recommended_epsilon]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param,epsilon,ruban_half_heigth);
		assert(isequal(size(table_matched_poi), [1 2]), 'The size of table_matched_poi is not the one expected : probably a wrong parameter poi1');
		TU_ok = isequal(sort(table_matched_poi{1}{1}),sort(table_matched_poi{2}{1})) && isequal(size(table_matched_poi{1}{2}),size(indices)) ;
		if TU_ok
	   		if verbose
	   		    disp(sprintf('%s : Test Passed', test_name));
		 	end
		else
			ok = false;
			disp(sprintf('%s : Test Failed', test_name));
		end

		%TU16-1-4-2 : superposed in image 2
		test_name = 'M5_TU16-1-4-2';
		epsilon = Inf;
		ruban_half_heigth = 1;
		poi1 = pois{1}(:,1);
		sig1 = signatures{1}(:,1);
		y = poi1(2,1);
		indices=find(pois{2}(2,:)>=y-ruban_half_heigth & pois{2}(2,:)<=y+ruban_half_heigth);	
		poi2 = [pois{2}(:,indices), pois{2}(:,indices(1))];
		sig2 = [signatures{2}(:,indices), signatures{2}(:,indices(1)+1)];
		[table_matched_poi, recommended_epsilon]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param,epsilon,ruban_half_heigth);
		assert(isequal(size(table_matched_poi), [1 1]), 'The size of table_matched_poi is not the one expected : probably a wrong parameter poi1');
		TU_ok = isequal(length(table_matched_poi{1}{1}),length(indices)+1) && isequal(length(table_matched_poi{1}{2}),length(indices)+1);
		if TU_ok
	   		if verbose
	   		    disp(sprintf('%s : Test Passed', test_name));
		 	end
		else
			ok = false;
			disp(sprintf('%s : Test Failed', test_name));
		end



%TU16-2 : tests for zncc/rmse descriptors
	im1=[];
	im2=[];
	if display_param
		im1 = images_rect{1};
		im2 = images_rect{2};
	end
	type_sig 	= -1;	%RMSE comparison
	%TU16-2-1 : 0 poi in one of the pictures
		%TU16-2-1-1 : poi1=[];
		test_name = 'M5_TU16-2-1-1';
		poi1=[];
		poi2=pois{2};
		sig1=[];
		sig2=signatures_ordre_0{2};
		[table_matched_poi, recommended_epsilon]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param);
		TU_ok = isequal(size(table_matched_poi),[0 0]) && isequal(recommended_epsilon,0);
		if TU_ok
    		if verbose
    		    disp(sprintf('%s : Test Passed', test_name));
   		 	end
 		else
 			ok = false;
		    disp(sprintf('%s : Test Failed', test_name));
		end

		%TU16-2-1-2 : poi2=[];
		test_name = 'M5_TU16-2-1-2';
		poi1=pois{1};
		poi2=[];
		sig1=signatures_ordre_0{1};
		sig2=[];
		[table_matched_poi, recommended_epsilon]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param);
		TU_ok = isequal(size(table_matched_poi),[0 0]) && isequal(recommended_epsilon,0);
		if TU_ok
    		if verbose
    		    disp(sprintf('%s : Test Passed', test_name));
   		 	end
 		else
 			ok = false;
		    disp(sprintf('%s : Test Failed', test_name));
		end
	
	%TU16-2-2 : the pois of first image is too close to the image borders
	test_name = 'M5_TU16-2-2';
	epsilon = Inf;
	ruban_half_heigth = 1;
	[y_max1,x_max1,~] = size(images_rect{1});
	poi1 = [1 1 x_max1 x_max1;
			1 y_max1 1 y_max1];
	sig1 = - ones(1,4);
	[y_max2,x_max2,~] = size(images_rect{2});
	poi2 = [1 1 x_max2 x_max2;
			1 y_max2 1 y_max2];
	sig2 = - ones(1,4);
	[table_matched_poi, recommended_epsilon]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param,epsilon,ruban_half_heigth);
	assert(isequal(size(table_matched_poi), [1 4]), 'The size of table_matched_poi is not the one expected : probably a wrong parameter poi1');
	TU_ok = isempty(table_matched_poi{1}{1}) && isempty(table_matched_poi{2}{1}) && isempty(table_matched_poi{3}{1}) && isempty(table_matched_poi{4}{1});
	if TU_ok
   		if verbose
   		    disp(sprintf('%s : Test Passed', test_name));
	 	end
	else
		ok = false;
	    disp(sprintf('%s : Test Failed', test_name));
	end
	

	
	%TU16-2-3 : 0 poi in the ruban containing the candidates for poi1(:,1)
	test_name = 'M5_TU16-2-3';
	epsilon = Inf;
	ruban_half_heigth = 1;
	poi1 = pois{1}(:,1);
	sig1 = signatures_ordre_0{1}(:,1);
	y = poi1(2,1);
	indices=find(pois{2}(2,:)<y-ruban_half_heigth | pois{2}(2,:)>y+ruban_half_heigth);	%extraction of poi2 which are out of the ruban 
	poi2 = pois{2}(:,indices);
	sig2 = signatures_ordre_0{2}(:,indices);
	[table_matched_poi, recommended_epsilon]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param,epsilon,ruban_half_heigth);
	assert(isequal(size(table_matched_poi), [1 1]), 'The size of table_matched_poi is not the one expected : probably a wrong parameter poi1');
	TU_ok = isempty(table_matched_poi{1}{1}) && isempty(table_matched_poi{1}{2}) ;
	if TU_ok
   		if verbose
   		    disp(sprintf('%s : Test Passed', test_name));
	 	end
	else
		ok = false;
	    disp(sprintf('%s : Test Failed', test_name));
	end
	
	%TU16-2-4 : N poi in the ruban containing the candidates for poi1(:,1)
		%TU16-2-4-1 : epsilon=0 : no matchings expected
		test_name = 'M5_TU16-2-4-1';
		epsilon = 0;
		ruban_half_heigth = 1;
		poi1 = pois{1}(:,1);
		sig1 = signatures_ordre_0{1}(:,1);
		y = poi1(2,1);
		indices=find(pois{2}(2,:)>=y-ruban_half_heigth & pois{2}(2,:)<=y+ruban_half_heigth);	%N=length(indices) 
		poi2 = pois{2}(:,indices);
		sig2 = signatures_ordre_0{2}(:,indices);
		[table_matched_poi, recommended_epsilon]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param,epsilon,ruban_half_heigth);
		assert(isequal(size(table_matched_poi), [1 1]), 'The size of table_matched_poi is not the one expected : probably a wrong parameter poi1');
		TU_ok = isempty(table_matched_poi{1}{1}) && isempty(table_matched_poi{1}{2}) ;
		if TU_ok
	   		if verbose
	   		    disp(sprintf('%s : Test Passed', test_name));
		 	end
		else
			ok = false;
			disp(sprintf('%s : Test Failed', test_name));
		end

		%TU16-2-4-2 : epsilon=Inf : N matchings expected
		test_name = 'M5_TU16-2-4-2';
		epsilon = Inf;
		ruban_half_heigth = 1;
		poi1 = pois{1}(:,1);
		sig1 = signatures_ordre_0{1}(:,1);
		y = poi1(2,1);
		indices=find(pois{2}(2,:)>=y-ruban_half_heigth & pois{2}(2,:)<=y+ruban_half_heigth);	%N=length(indices) 
		poi2 = pois{2}(:,indices);
		sig2 = signatures_ordre_0{2}(:,indices);
		[table_matched_poi, recommended_epsilon]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param,epsilon,ruban_half_heigth);
		assert(isequal(size(table_matched_poi), [1 1]), 'The size of table_matched_poi is not the one expected : probably a wrong parameter poi1');
		TU_ok = isequal(length(table_matched_poi{1}{1}),length(indices)) && isequal(length(table_matched_poi{1}{2}),length(indices));
		if TU_ok
	   		if verbose
	   		    disp(sprintf('%s : Test Passed', test_name));
		 	end
		else
			ok = false;
			disp(sprintf('%s : Test Failed', test_name));
		end
	
		
		
