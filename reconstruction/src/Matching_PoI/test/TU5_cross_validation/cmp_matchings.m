function [correct_matchings, wrong_matchings] = cmp_matchings(matchings_computed, matchings_expected, cameras, masks, pois, verbose, show_plots)

    %% Init
    if nargin <= 3
        show_plots = false;
        if nargin <= 2
            verbose = false;
        end
    end

    wrong_matchings   = [];
    correct_matchings = 0;
    n = size(matchings_expected, 1);
   
    %% Preconditions
    %assert(length(matchings_computed)==length(matchings_expected), 'We should have the same number of matchings');

    if show_plots
       figure;
       hold on
       axis equal
    end
    
    for i=1:size(matchings_computed,1)
        is_matching_correct = false;
        for j=1:n
            % B is included in a if union(A,B) == B
            if isequal(union(matchings_expected(j,:), [0 matchings_computed(i,:)]) , union(matchings_expected(j,:), 0))
                correct_matchings = correct_matchings + 1;
                is_matching_correct = true;
                break;
            end
        end
        if ~is_matching_correct
            wrong_matchings = [wrong_matchings i];
        end
        
        if show_plots
            matching = export_matchings(matchings_computed(i,:), pois);
            [p3d errors stats exec_time] = calc_3D_script(matching, cameras, masks);
            if length(p3d.cloud) > 0
                if is_matching_correct
                    plot3(p3d.cloud(1,1),p3d.cloud(1,2),p3d.cloud(1,3),'g.');
                else
                    plot3(p3d.cloud(1,1),p3d.cloud(1,2),p3d.cloud(1,3),'r.')
                end
            end
        end
            
    end
    
    correct_matchings   = 100*correct_matchings/size(matchings_computed,1);
    if verbose
       display(sprintf('Matchings :  correct matchings %f %%\n\n', correct_matchings) ); 
    end
    
    if show_plots
       axis vis3d
       hold off
    end
end