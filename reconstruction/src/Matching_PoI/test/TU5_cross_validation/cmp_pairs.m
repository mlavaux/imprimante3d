function [correct_pairs, missing_pairs] = cmp_pairs(pairs_computed, pairs_expected, verbose, show_plots, images_origines, pois)

    %% Init
    if nargin <= 3
        show_plots = false;
        if nargin <= 2
            verbose = false;
        end
    end

    
    n = length(pairs_computed);
    correct_pairs   = zeros(1, length(pairs_computed));
    missing_pairs   = zeros(1, length(pairs_computed));

    %% Preconditions
    %assert(length(pairs_computed)==length(pairs_expected), 'We should have the same number of pairs');

    
    for i=1:n
        if show_plots
            figure;
            subplot(1, 2, 1);
            hold on;
            axis ij;
            axis off;
            imagesc(images_origines{i});
            for l=1:length(pairs_computed{i})
                pb1 = plot(pois{i}(1,pairs_computed{i}{l}(1)), pois{i}(2,pairs_computed{i}{l}(1)), 'rx');
            end
            
            subplot(1, 2, 2);
            hold on;
            axis ij;
            axis off;
            imagesc(images_origines{mod(i,n)+1});
            for l=1:length(pairs_computed{i})
                pb2 = plot(pois{mod(i,n)+1}(1,pairs_computed{i}{l}(2)), pois{mod(i,n)+1}(2,pairs_computed{i}{l}(2)), 'rx');
            end
            axis ij;
        end
        for j=1:length(pairs_expected{i})
            is_pair_correct = false;
            for k=1:length(pairs_computed{i})
                ipoi = pairs_expected{i}{j};
                if isequal(pairs_expected{i}{j}, pairs_computed{i}{k})
                    correct_pairs(i) = correct_pairs(i) + 1;
                    is_pair_correct = true;
                    if show_plots
                        subplot(1, 2, 1);
                        pg1 = plot(pois{i}(1,ipoi(1)), pois{i}(2,ipoi(1)), 'gx');
                        subplot(1, 2, 2);
                        pg2 = plot(pois{mod(i,n)+1}(1,ipoi(2)), pois{mod(i,n)+1}(2,ipoi(2)), 'gx');
                    end
                    break;    
                end
            end
            if ~is_pair_correct
                missing_pairs(i) = missing_pairs(i) + 1;
                if show_plots
                    subplot(1, 2, 1);
                    pr1 = plot(pois{i}(1,ipoi(1)), pois{i}(2,ipoi(1)), 'bx');
                    subplot(1, 2, 2);
                    pr2 = plot(pois{mod(i,n)+1}(1,ipoi(2)), pois{mod(i,n)+1}(2,ipoi(2)), 'bx');
                end
            end
        end
        missing_pairs(i)   = 100*missing_pairs(i)/length(pairs_expected{i});
        correct_pairs(i)   = 100*correct_pairs(i)/length(pairs_computed{i});
        if show_plots
            legend([pb1, pg1, pr1], 'Incorrect pairs', 'Correct pairs', 'Missing pairs');
            legend([pb2, pg2, pr2], 'Incorrect pairs', 'Correct pairs', 'Missing pairs');
            title(sprintf('Points of interests belonging to pairs between pictures %d and %d',i,mod(i,n)+1));
        end
        if verbose
           display(sprintf('Pairs %d and %d :\n - correct pairs %f %%\n - missing pairs %f %%\n', i, mod(i,n)+1, correct_pairs(i), missing_pairs(i))); 
        end
    end
end