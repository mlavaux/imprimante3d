

function [ok err] = test1(verbose, display_plots)

    addpath ('../../src');

    test_name = 'M5_TU5.1';
    purpose = 'Validity of the cross-validation matching indexes with the DENSE House version1 compared with the perfect result generated with Blender';

    ok = true;
    err = 0;

    % float precision (quite tolerant)
    eps = 1e-6;

    if nargin < 2
        display_plots = false;
        if nargin < 1
            verbose = false;
        end
    end


    if verbose
        disp(sprintf('%s : Starting the Test. Warning: it is be VERY slow. Purpose : %s', test_name, purpose));
    end
    % INIT VALUES 

    load house1_images.mat
    load house1_cameras.mat
    load house1_dense_pairs.mat
    load house1_dense_pts.mat
    
    removal = true;
    
    [signatures]=compute_signatures(images_origine, pts);
    
    masks = {};
    for i=1:length(images_origine)
        masks{i} = [0];
    end

    % Execute the script
    if(verbose)
        disp(sprintf('%s : Running', test_name));
    end

    for type_signatures = -2:-1
        matchingij = {};
        n = length(pts);



        for i=1:n
            if verbose
                display(sprintf('Matching picture %d and %d / %d', i, mod(i,n)+1, n));
            end
            [T1,T2,~,~]=rectify(cameras{i},cameras{mod(i,n)+1});
            [im1,im2,poi1,poi2,sig1,sig2]=CalculeImageHomographie(T1,T2,images_origine{i},images_origine{mod(i,n)+1},pts{i},pts{mod(i,n)+1}, false, false);
            matchingij{i}=cross_validation(poi1,poi2,type_signatures,signatures{i},signatures{mod(i,n)+1},im1,im2,removal, 0,verbose);
        end

        [correct_pairs, missing_pairs] = cmp_pairs(matchingij, pairs, verbose, display_plots, images_origine, pts);
        
        computed_matchings = extendMatchings(matchingij);
        expected_matchings = extendMatchings(pairs);
        matchings = export_matchings(computed_matchings, pts);
        matchings = remove_pairs(matchings);
        [correct_matchings, wrong_matchings] = cmp_matchings(computed_matchings, expected_matchings, cameras, masks, pts, verbose, display_plots);
        size(wrong_matchings)



        for i=1:n
            if correct_pairs(i) < 60 || missing_pairs(i) > 60
                error = Inf;
                ok = false;
                if verbose
                    display( sprintf('The pair %d and %d have a correction rate of %f and a missing rate of %f :(', i, mod(i,n)+1, correct_pairs(i), missing_pairs(i)) );
                end
            end
        end
    end

    if(verbose)
        disp(sprintf('%S : Done', test_name));
    end



    % END
    if ok 
        if verbose
            disp(sprintf('%s : Test Passed', test_name));
        end
    else
        disp(sprintf('%s : Test Failed', test_name));
    end

  
return
