function ok = run_test(disp)

ok = test0(disp) && test1(disp)

end
