function [signatures]=compute_signatures(images_origine, pois)
    
    signatures = {};
    window_half_size = 3;	% default value defining the neighborhood
	for i=1:length(images_origine)
		[y_max, x_max]=size(images_origine{i});
		sig=[];
		for k=1:size(pois{i},2)
			c = pois{i}(:,k);	% c = [x ; y]
			try	% The window is entierly inside the picture
				w = images_origine{i}(c(2)-window_half_size:c(2)+window_half_size,c(1)-window_half_size:c(1)+window_half_size,:);
			catch % The window is not entierly inside the picture
				w = -ones(1+2*window_half_size, 1+2*window_half_size, 3);
			end
			sig = [sig, w(:)];
		end
		signatures{i}=sig;
    end	
end