clear all;

addpath('../../src');

im1 = imread('../../../../media/city_stereo/city_left.jpg');
im2 = imread('../../../../media/city_stereo/city_right.jpg');

images = {im1, im2};

test1_poi


%im2 = im1;
%pois{2} = pois{1};

% On enlève des POI pour les test
%echant1 = [348];
%echant2 = 1:5:size(pois{2},2);
%pois{1} = pois{1}(:,echant1);
%pois{2} = pois{2}(:,10:20:end);

espilon = 2.46;

match_poi;

success = 0;
for i = 1:length(matching)
    if length(find(matching{i}(3:end) == matching{i}(1))) > 0 && length(find(matching{i}(3:end) == matching{i}(2))) > 0
        success = success + 1;
    else
        display('failure');
        i
    end
end
success = success / length(matching)


% -----------------------

matching1 = matching;
pois_matrix1 = pois_matrix{1};

im3 = im1;
im1 = im2;
im2 = im3;

poi = pois{1};
pois{1} = pois{2};
pois{2} = poi;


match_poi;

matching2 = matching;
pois_matrix2 = pois_matrix{1};


% -------- cross_check

matching = {};
k = 1;
% Pour chaque poi de l'im1
for i=1:length(matching1)
    match1 = matching1{i};
    %pt1_matches = match1(3:end); 
    % Pour tous les poi appariés
    for imatch = 3:2:length(match1)
        % où trouver dans matching2 les points matchés par matching1{1}
        j = pois_matrix2(match1(imatch), match1(imatch+1));
        if j>0 && length(find(matching2{j}(3:end) == match1(1))) > 0 && length(find(matching2{j}(3:end) == match1(2))) > 0
            matching{k} = [match1(1:2) match1(imatch:imatch+1)];
            k = k+1;
        end
    end
end

figure
hold on

im = [im2 im1];
imagesc(im);
axis ij;
plot(pois{2}(1,:),pois{2}(2,:), 'rx')
plot(pois{1}(1,:)+size(im1,2),pois{1}(2,:), 'gx')

for i = 1:length(matching)
    plot([matching{i}(1) matching{i}(3)+size(im1,2)], [matching{i}(2) matching{i}(4)], '-r');
end
hold off
