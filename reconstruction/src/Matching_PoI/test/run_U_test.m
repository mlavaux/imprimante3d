%
addpath('../src');
disp('Run TU16')

addpath('TU16_matching_poi/')
fail = false;
ok = run_test;
if ok
  disp('TU16 ok');
else
  disp('TU16 nok');
  fail = true;
end

rmpath('TU16_matching_poi/');
%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Run TU0')

addpath('TU0_rectify/')
ok = run_test;
if ok
  disp('TU0 ok');
else
  disp('TU0 nok');
  fail = true;
end

rmpath('TU0_rectify/');
%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Run TU1_ZNCC')

addpath('TU1_zncc/')
ok = run_test(false);
if ok
  disp('TU1 ok');
else
  disp('TU1 nok');
  fail = true;
end

rmpath('TU1_zncc/');
%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('run TU2_rmse')

addpath('TU2_rmse/')
ok = run_test(false);
if ok
  disp('TU2 ok');
else
  disp('TU2 nok');
  fail = true;
end

rmpath('TU2_rmse/');
%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('run TU6_CROSS')

addpath('TU6_cross_validation/')
ok = run_test(false);
if ok
  disp('TU6 ok');
else
  disp('TU6 nok');
  fail = true;
end

rmpath('TU6_cross_validation/');

if fail
  disp('FAILURE');
else
  disp('GREAT SUCESS');
end
