function matching=cross_validation(poi1,poi2,type_sig,sig1,sig2,im1,im2,removal,display_param,verbose)
%
%	INPUTS 	: 
%
%	poi1, poi2 : points of interest from images 1 and 2
%	sig1, sig2 : signatures of each poi from each image
%	type_sig   : type of the signature (negative if no signatures)
%	im1, im2   : rectified images 1 and 2 ([] if no need) 
%	display_param	: integer for different levels of displays
%				0 : no plot
%				1 : display rectified pictures with apparied pois
%				2 : display only rectified pictures and pois peer to peer (does not concern this function)
%				3 : display potential false matchings stats
%				4 : display potential false matchings stats and plots
%				5 : display everything 
%
%
%	OUTPUTS : 
%
%	matching : cell array 
%	matching{k} = [index1, index2] where poi1(index1) and poi2(index2) are matched
%	length(matching) = number of apparied pois found after cross validation

if nargin < 10
	verbose = false;
	if nargin < 9
		display_param = 0;
		if nargin < 8
			removal = false;
		end
	end
end

display_false_matchings = (display_param == 3 || display_param == 4 || display_param == 5);
matching = {};


[table_matched_poi12, recommended_epsilon12]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2, display_param);
[table_matched_poi21, recommended_epsilon21]=matching_poi(poi2,poi1,type_sig,sig2,sig1,im2,im1, display_param);
if display_false_matchings
	visu_false_matchings = (display_param == 4 || display_param == 5);
	visuFalseMatchings(im1, im2, poi1, poi2, table_matched_poi12, recommended_epsilon12, visu_false_matchings);
	visuFalseMatchings(im2, im1, poi2, poi1, table_matched_poi21, recommended_epsilon21, visu_false_matchings);
end

%	Removal of matchings with too big error
if removal
	for i = 1:length(table_matched_poi12)
		indices = find(table_matched_poi12{i}{2} < recommended_epsilon12);
		table_matched_poi12{i}{1}=table_matched_poi12{i}{1}(indices);
		table_matched_poi12{i}{2}=table_matched_poi12{i}{2}(indices);	
	end
	for i = 1:length(table_matched_poi21)
		indices = find(table_matched_poi21{i}{2} < recommended_epsilon21);
		table_matched_poi21{i}{1}=table_matched_poi21{i}{1}(indices);
		table_matched_poi21{i}{2}=table_matched_poi21{i}{2}(indices);	
	end
end

% Counts the number of matches -- Statistics

index_success12 = find(~cellfun(@(t) isempty(t{1}), table_matched_poi12));
index_success21 = find(~cellfun(@(t) isempty(t{1}), table_matched_poi21));

index_failures12 = setdiff(1:size(poi1,2), index_success12);
index_failures21 = setdiff(1:size(poi2,2), index_success21);

success12 = length(index_success12);
success21 = length(index_success21);

success12 = success12/size(poi1,2);
success21 = success21/size(poi2,2);

if verbose
	display(sprintf('Recommended_epsilons : %f %f', recommended_epsilon12, recommended_epsilon21));
	display(sprintf('%f%% of matchings found from left to right.', 100*success12));
	display(sprintf('%f%% of matchings found from right to left.', 100*success21));
end

% -------- cross_check

k = 1;

% For each poi of im1
errors_matching = {};
for ind1=1:length(table_matched_poi12)
    match1 = table_matched_poi12{ind1};
    %match1 : cell array... match1{1} : array of indexes of candidate pois for the matching in im2... match1{2} : array of errors assiciated to these matchings
    %les poi (et leurs erreurs associées) sont classées par ordre croissant d'erreur, autrement dit l'appariement le + probable est celui avec le 1er élt de la liste
    %the poi (and their associated errors) are sorted by ascend order
	if length(match1{1})
	    ind2=match1{1}(1);
	    match2 = table_matched_poi21{ind2}; % match2 : cf match1...
		if length(match2{1})		
			if match2{1}(1)==ind1
			    matching{k} = [ind1, ind2];
                errors_matching{k} = mean([match1{2}(1), match2{2}(1)]);
                if abs(match1{2}(1)-match2{2}(1)) > 0.1
                    warning(sprintf('Non symmetric error for %d -- %f %f', k, match1{2}(1), match2{2}(1)));
                end
			    k = k+1;
			end
		end
	end
end

if verbose
	success = length(matching)/min(size(poi1,2), size(poi2,2));
	display(sprintf('%f%% of matchings after the cross validation', 100*success));
end


error_max = max([errors_matching{:}]);
%error_max = max(recommended_epsilon12, recommended_epsilon21);
error_min = min([errors_matching{:}]);

if (display_param == 1 || display_param == 5)
    figure
    hold on
    cVec = colormap(hsv(200));
    imagesc([im1, im2]);
    axis ij;
    for i = 1:length(poi1)
    	plot(poi1(1,i), poi1(2,i), '.', 'markersize',20, 'color','k');
	end
	for i = 1:length(poi2)
    	plot(poi2(1,i)+size(im1,2), poi2(2,i), '.', 'markersize',20, 'color','k');
	end
    for i = 1:length(matching)
        %1+round((errors_matching{i}-error_min)/(error_max-error_min)*100)
        %cVec(1+round((errors_matching{i}-error_min)/(error_max-error_min)*100),:)
        if errors_matching{i}<=error_max
	        plot(poi1(1,matching{i}(1)), poi1(2,matching{i}(1)), '.', 'markersize',20, 'color',cVec(99+round((errors_matching{i}-error_min)/(error_max-error_min)*100),:));
   	        plot(poi2(1,matching{i}(2))+size(im1,2), poi2(2,matching{i}(2)), '.', 'markersize',20, 'color',cVec(99+round((errors_matching{i}-error_min)/(error_max-error_min)*100),:));
	    else
	    	plot(poi2(1,matching{i}(2))+size(im1,2), poi2(2,matching{i}(2)), '.', 'markersize',20, 'color','k');
	    end
    end
    if false
		k = 0;
		cVec = colormap(hsv(ceil(length(matching)/4)+1));
		%plot the segments...
		for i = 1:length(matching)
			%moy = length(matching12{i})
			if mod(i, 4)==0
				k = k+1;
				p = matching{i}(1);
				q = matching{i}(2);
				plot([poi1(1,p), poi2(1,q)+size(im1,2)], [poi1(2,p), poi2(2, q)], 'color', cVec(k,:));
			end
		end
	end
    hold off
end
