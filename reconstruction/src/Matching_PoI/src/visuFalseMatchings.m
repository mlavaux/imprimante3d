function visuFalseMatchings(im1, im2, poi1, poi2, table_matched_poi12, epsilon, visu_false_matchings)
% Function used to visualize poi matchings between im1 and im2 which have an error rate > epsilon	
% The im<i> and poi<i> inputs can be original or rectified images	
%	visu_false_matchings	: 1 if we want to visualize potential false matchings ; 0 else

	if nargin<7
		display_param=0;
	end
	
	if visu_false_matchings
		figure;
		hold on;
		im = [im1 im2];
		imagesc(im);
		axis ij;
	
	
		for i=1:length(poi1)
		    plot(poi1(1,i),poi1(2,i), 'rx')
		end
		for j=1:length(poi2)
		    plot(poi2(1,j)+size(im1,2),poi2(2,j), 'gx')
		end
	end
	
	indices=[];
	for i = 1:length(table_matched_poi12)
		tmp = table_matched_poi12{i};
		if ~isempty(tmp{1}) && tmp{2}(1) > epsilon
			indices=[indices i];
		end
	end
	if visu_false_matchings
		cVec = colormap(hsv(length(indices)));
		for i = 1:length(indices)	
			c1=poi1(:,indices(i));	
			c2=poi2(:,table_matched_poi12{indices(i)}{1}(1));	% on récupère le poi qu'on considère de l'image 2
			j1 = c1(1);
			j2 = c2(1);
			i1 = c1(2);
			i2 = c2(2);
			plot([j1,j2+size(im1,2)],[i1,i2],'color',cVec(i,:));
		
		end
	end
	display(sprintf('For epsilon = %f, number of false matchings = %d',epsilon,length(indices)));
	
return
