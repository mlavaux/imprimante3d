function new_matchings = remove_pairs(matchings)
    new_matchings = [];
    for i=1:size(matchings,1)
        if nnz(matchings(i,:)+1) > 4
            new_matchings = [new_matchings ; matchings(i,:)];
        end
    end
end