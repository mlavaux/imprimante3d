function [matchings]=matching(images_origine,pois_origine,cameras,type_signatures,signatures, extendMatching, removal, display_param, verbose)
%
%
%
%	INPUTS :
%       images_origine  : cell array containing the "n" matrices of the images
%       pois_origine    : cell array containing for each image a matrix of the feature points
%               pois_origine{i} contains the feature points of the picture number i
%               pois_origine{i} = [x1 x2 x3 ... ;  y1 y2 y3  ... ] 
%               where (x1, y1), (x2, y2), ... are the coordinates of feature
%               points.
%       cameras         : cell array containing the camera projection
%               matrices
%       type_signatures : signed integer describing the signature used with
%               the pois. A negative integer means the rectification of the
%               pictures is needed
%       signatures      : cell array containing for each image a matrix of
%               the signatures of the feature points
%               signatures{i} = signature_vector_of_the_ith_poi
%       extendMatching	: integer for choosing between different matching
%               extensions
%               1 : naive extension
%               2 : extend a matching only if the reprojection of the new
%                   pair is correct.
%		display_param	: integer for different levels of displays
%				0 : no plot
%				1 : display rectified pictures with apparied pois
%				2 : display only rectified pictures and pois peer to peer
%				3 : display potential false matchings stats
%				4 : display potential false matchings stats and plots
%				5 : display everything 
%
%
%
%	OUTPUT : 
%       matchings : m x 2*n matching matrix 
%               with n = number of pictures
%                    m = number of matchings
%               index_matchings(i,:) describes the ith matching
%               index_matchings(i,2*j-1:2*j) gives the coordinates 
%               of the poi index of the jth picture which intervenes in 
%               the ith matching. 
%               The value is -1 if the poi doesn't intervene.
%               
%

    %% Initializations
	if nargin < 9
		verbose = false;
		if nargin < 8
			display_param = 0;	% no plots as default
			if nargin < 7
				removal = false;
		        if nargin < 6
		            extendMatching = 1;
		            if nargin < 5
		                signatures = cell(1,length(pois_origine));
		                if nargin < 4
		                    type_signatures = -1;	% RMSE as default
		                end
		            end
		        end
			end
		end
	end
    visu_rectification = (display_param == 1 || display_param == 2 || display_param ==5);
	visu_cross_validation = (display_param == 1 || display_param ==5);

    matchingij={};
    n=length(pois_origine);
    calcul_rectification = (visu_rectification);	% We should compute the rectified pictures whether we use rmse, zncc or a signure type < 0.

    %% Preconditions
    assert(length(images_origine)==length(cameras), 'Error : we should have as many camera as pictures');
    assert(~isempty(images_origine), 'We need at least two pictures.');
    assert(length(pois_origine) == length(images_origine), 'We should have as many set of pois as pictures.');
    if ~calcul_rectification
        assert(length(pois_origine) == length(signatures), 'We should have as many pois as signatures.');
    end

	n_plot=0;
	m_plot=0;
	
	if visu_rectification
		figure(4);
		n_plot=floor(sqrt(n));
		m_plot=ceil(sqrt(n));
		if n_plot*m_plot<n
			n_plot=n_plot+1;
		end
    end
    
    calcul_descs=false;
	if numel(find(~cellfun(@(t) isempty(t), signatures))) == 0		% empty cell of signatures
		calcul_descs = true;
		if verbose
			warning('Computation of descriptors on rectified pictures... but don''t worry')
		end
	end


    %% Step 1 : Compute a matching for each pair of picture
    for i=1:n-1
    	if verbose
	        display(sprintf('Matching picture %d and %d / %d', i, i+1, n));
	    end
        [T1,T2,~,~]=rectify(cameras{i},cameras{i+1});
        [im1,im2,poi1,poi2,sig1,sig2]=CalculeImageHomographie(T1,T2,images_origine{i},images_origine{i+1},pois_origine{i},pois_origine{i+1}, calcul_descs,calcul_rectification);
        if ~calcul_descs
    		sig1 = signatures{i};
			sig2 = signatures{i+1};
    	end
        matchingij{i}=cross_validation(poi1,poi2,type_signatures,sig1,sig2,im1,im2,removal,display_param,verbose);
        if (visu_rectification || visu_cross_validation)
        	figure(4);
        	
        	subplot(n_plot,m_plot,i);
		    hold on	
		    im = [im1 im2];
		    imagesc(im);
		    axis ij;
		    for p=1:size(poi1,2)
		        plot(poi1(1,p),poi1(2,p), 'rx')
		    end
		    for q=1:size(poi2,2)
		        plot(poi2(1,q)+size(im1,2),poi2(2,q), 'gx')
		    end
        	title(sprintf('Rect %d - %d',i,i+1));
		   	if visu_cross_validation
		   		%plot the segments...
		   	end
        end 

    end
    % Compute a matching for the final pair (n,1)
    if verbose
	    display(sprintf('Matching picture %d and %d / %d', n, 1, n));
	end
    [T1,T2,~,~]=rectify(cameras{n},cameras{1});
    [im1,im2,poi1,poi2,sig1,sig2]=CalculeImageHomographie(T1,T2,images_origine{n},images_origine{1},pois_origine{n},pois_origine{1},calcul_descs, calcul_rectification);
    if ~calcul_descs
    	sig1 = signatures{n};
    	sig2 = signatures{1};
    end
    matchingij{n}=cross_validation(poi1,poi2,type_signatures,sig1,sig2,im1,im2,removal,display_param,verbose);
    if (visu_rectification || visu_cross_validation)
       	subplot(n_plot,m_plot,n);
	    hold on	
	    im = [im1 im2];
	    imagesc(im);
	    axis ij;
	    for p=1:size(poi1,2)
	        plot(poi1(1,p),poi1(2,p), 'rx')
	    end
	    for q=1:size(poi2,2)
	        plot(poi2(1,q)+size(im1,2),poi2(2,q), 'gx')
	    end
       	title(sprintf('Rect %d - %d',i,i+1));
       	if visu_cross_validation
       		%plot the segments...
       	end
    end

    %% Step 2 : Extend the matchings
    if verbose
	    display('Extending the matchings');
    end
    if extendMatching == 1
        index_matchings = extendMatchings(matchingij,verbose);
        matchings = export_matchings(index_matchings, pois_origine);
    elseif extendMatching == 2
        [matchings, index_matchings] = extendMatchings2(matchingij,cameras,pois_origine,verbose);
    else
        error(sprintf('Unknonw value %d for extendMatching', extendMatching));
    end
    matchings = remove_pairs(matchings);
end
