% Root Mean Square Error for k = 1

% INPUT
%   im1  : a n x n x z matrix representing the first picture z \in {1,3}
%   im2  : a n x n x z matrix representing the second picture


% OUTPUT
%   res = the RMSE of the 2 pictures



function  res = rmse(im1, im2)
    assert(size(im1,3) == size(im2,3));
    
    diff      = int16(im1)- int16(im2);
    intensity = diff(:,:,1).^2;
    for i=2:size(diff,3)
         intensity =  intensity + diff(:,:,i).^2;
    end
    intensity = sqrt(double(intensity));
    res       = sqrt(sum(sum(intensity))/(size(intensity,1)*size(intensity,2)));
    
end
