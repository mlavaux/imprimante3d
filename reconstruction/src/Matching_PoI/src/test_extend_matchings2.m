function [] = test_extend_matchings2()

    addpath('../../../media/House_Blender_Vincent/version1');

    cams;

    p3d1 =  [ 0 ; 0 ; 5 ];
    p3d2 =  [ 0.1 ; 0.1 ; 5.1 ]; % p3d1 bruité
    p3d3 =  [ 1 ; 2 ; 3 ];
    p3d4 =  [ 2 ; 3 ; 4 ];


    pois1_1 = projection(cameras{1},p3d1); % 1.0e+03 * [0.9600 -1.2584]
    pois1_2 = projection(cameras{2},p3d1); % 1.0e+03 * [0.9600 -1.1913]
    pois1_3 = projection(cameras{3},p3d2);

    pois2_1 = projection(cameras{1},p3d3);
    pois2_2 = projection(cameras{2},p3d3);
    pois2_3 = projection(cameras{3},p3d3);
    
    pois3_1 = projection(cameras{1},p3d4);
    pois3_2 = projection(cameras{2},p3d4);

    pois = cell([1,3]);
    pois{1} = [pois1_1 pois2_1 pois3_1];
    pois{2} = [pois1_2 pois2_2 pois3_2];
    pois{3} = [pois1_3 pois2_3];

    indexes_pois = cell([1,3]);
    indexes_pois{1} = cell([1,3]);
    indexes_pois{1}{1} = [1 1];
    indexes_pois{1}{2} = [2 2];
    indexes_pois{1}{3} = [3 3];
    indexes_pois{2} = cell([1,2]);
    indexes_pois{2}{1} = [1 1];
    indexes_pois{2}{2} = [2 2];
    indexes_pois{3} = cell([1,2]);
    indexes_pois{3}{1} = [1 1];
    indexes_pois{3}{2} = [2 2];

    [matchings, index_matchings] = extendMatchings2(indexes_pois, cameras, pois, true)
end

function p2d = projection(cam, p3d)
    p2d = cam*[p3d ; 1];
    p2d = p2d(1:2)/p2d(3);
end
