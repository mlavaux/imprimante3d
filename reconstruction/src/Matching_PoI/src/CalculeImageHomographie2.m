function [imsOut, poiOuts]=CalculeImageHomographie2(Hs, imsIn, poisIn, visualisation)
%
% @Gael Michelin
% La fonction calcule les images imout<i> � partir des images imin<i> et des
% homographies H<i> entre les images d'entree et sortie t.q. imout= H*imin
% imout<i> : image rectifiee de imin<i>
% imout<i> = [] si nargin==6 ou visualisation == false 
% hautout,largout= dimensions de l'image de sortie
% imin= image d'entr�e
% hautin,largin= dimensions de l'image d'entr�e
% 
%
% Possibilit� de ne pas passer d'argument PoIin1 et PoIin2 
% Dans ce cas, les listes PoIout<i> seront vides

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 7
	visualisation = 1;
end 

[hautin, largin, cspace]=size(imsIn{1});

imsOut  = {};
poiOuts = {};
xrect   = {};
inv     = {};
largout = {};

ymin    = hautin;
ymax    = 0;

%Coordonnees des extremites de l'image 1 apres rectification pour chaque image
for i=1:length(imsIn)
    [xmini, xmaxi, ymini, ymaxi, inv{i}] = coordsImOut(Hs{i}, hautin, largin);
    ymin = min(ymini, ymin);
    ymax = max(ymaxi, ymax);
    largout{i} = floor(xmaxi)-floor(xmini)+1;
    xrect{i}   = xmini;
end
hautout = floor(ymax)-floor(ymin)+1;
yrect   = ymin;


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%    			GROS WARNING				 %       
        %	Pour garder les indices (identifiants)	 %
		%	des points d'interet, on ne supprime pas %
		%	les coordonnees sortant de l'image apres %
		%	rectification de l'image !				 %
		%	Il faudra tester si le PoI est bien dans %
		%	l'image avant de faire l'exploitation.	 %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Calcul des nouvelles coordonnees des points d'interet
    for i=1:length(imsIn)
        poiOuts{i} = coordsPoi(Hs{i}, poisIn{i}, xrect{i}, yrect, largout{i}, inv{i});
    end
	

    % Apply the homography on the pictures
	if visualisation 
        for i=1:length(imsIn)
            imsOut{i} = applyImHomography(Hs{i}, imsIn{i}, hautout, largout{i}, xrect{i}, yrect, inv{i});
        end
	end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [xmin, xmax, ymin, ymax, inv] = coordsImOut(H, hautin, largin)

    %Coordonnees des extremites de l'image 1 apres rectification
    m11=H*[1;1;1];
    m12=H*[1;hautin;1];
    m21=H*[largin;1;1];
    m22=H*[largin;hautin;1];
    m11=m11(1:2)/m11(3);
    m12=m12(1:2)/m12(3);
    m21=m21(1:2)/m21(3);
    m22=m22(1:2)/m22(3);
    %Abscisses et ordonnees extremales de l'image rectifiee 1
    xmin=min(m11(1),min(m12(1),min(m21(1),m22(1))));
    ymin=min(m11(2),min(m12(2),min(m21(2),m22(2))));
    xmax=max(m11(1),max(m12(1),max(m21(1),m22(1))));
    ymax=max(m11(2),max(m12(2),max(m21(2),m22(2))));
    if m11(1)>m21(1)
        inv = true;	%l'image 1 rectifiee est inversee selon l'axe vertical
    else
        inv = false;
    end
return;

function [poiout] = coordsPoi(H, poiIn, xrect, yrect, largout, inv)
    % Calcul des coordonnees des points d'interet dans l'image rectifiée
    poiout = [];
    for i=1:size(poiIn,2)
		x=poiIn(1,i);
		y=poiIn(2,i);
       	p=H*[x,y,1]'; %transformation homographique directe 
        ud=round(p(1)/p(3)-xrect);
        vd=round(p(2)/p(3)-yrect);
        if inv   
			poiout=[poiout, [largout-ud+1;vd]];
		else
			poiout=[poiout, [ud;vd]];	
		end
	end
    
return


function [imOut] = applyImHomography(H, imIn, hautout, largout, xrect, yrect, inversion)
    Hinv=inv(H);

    [hautin, largin, cspace]=size(imIn);
    imOut=uint8(zeros([hautout,largout, cspace]));
    %Calcul des nouvelles images
    for y=1:hautout
        for x=1:largout
            p=Hinv*[x+xrect, y+yrect, 1]'; %transformation homographique inverse
            ud=round(p(1)/p(3));
            vd=round(p(2)/p(3));
            if inversion
                x=largout-x+1;
            end		
            if vd>0 & ud>0 & vd<=hautin & ud<=largin
                %ca tombe dans l'image
                val = imIn(vd,ud,:);
                imOut(y,x,:) = val; %on attribue la couleur du pixel concerne
            else
                % ca tombe hors de l'image datamatrix, on laisse la valeur du
                % fond, eventuellement, on aurait pu faire:
                % imout1(y,x)=255; %on attribue la couleur blanche
            end
        end
    end
        
return
