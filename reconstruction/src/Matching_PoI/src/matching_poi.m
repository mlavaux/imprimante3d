function [table_matched_poi, recommended_epsilon]=matching_poi(poi1,poi2,type_sig,sig1,sig2,im1,im2,display_param,epsilon,ruban_half_height)

% This function matchs points from 2 rectified pictures
% from a set of images.

% INPUT
%   im1  : a n x n x 3 matrix representing the first picture, (i,j)
%     coordinates or (y,x)
%   im2  : a n x n x 3 matrix representing the second picture, (i,j)
%     coordinates or (y,x)
%   poi1, poi2 : contain for each image a matrix of the feature points
%   poi<i> = [x1 x2 x3 ... ;  y1 y2 y3  ... ] 
%   where (x1, y1), (x2, y2), ... are the coordinates of feature points.
%
%	optional parameters :
%	ruban_half_height
%	window_half_size
%
%	optional optional parameter :
%	epsilon
%

% OUTPUT
%   table_matched_poi : array containing for each index i a sorted table of index and error 
%	for each poi of the second picture matching the poi with the index i of
%	the first picture 
%   table_matched_poi{i}={[j1 j2 j3 ... ], [eps1 eps2 eps3 ...]}
%	means poi1(i) can be matched with poi2(j<k>) (k=1, 2, 3 ...) with the error	eps<k> 
%   
%  
%
%
%   recommended_epsilon : choosed around the 3rd quartile of smallest errors : 20% of matching rejections


table_matched_poi = {};
recommended_epsilon = 0;


%% Preconditions

% We assume all the pictures have the same number of rows
if (display_param == 1) || (display_param == 2) || (display_param == 5)
	assert( isequal(size(im1,3), size(im2,3)), 'All pictures should be grayscale or RGB');
	assert( isequal(size(im1,1), size(im2,1)), 'All the pictures should have the same number of row');
    assert( isequal([size(im1,1), size(im1,2), size(im2,2)]>0 , [1 1 1]), 'The pictures must not be missing when the display plots parameter is ON');
    if isequal(size(poi1), [0 0])
    	warning('poi1 = []');
    else
		assert(min(min(poi1)) > 0);
		assert(max(poi1(1,:)) <= size(im1,2));
		assert(max(poi1(2,:)) <= size(im1,1));
	end
	if isequal(size(poi2), [0 0])
		warning('poi2 = []');
	else
    	assert(min(min(poi2)) > 0);
	    assert(max(poi2(1,:)) <= size(im2,2));
	    assert(max(poi2(2,:)) <= size(im2,1));
	end
end

assert( isequal(size(poi1,2),size(sig1,2)), 'We should have as many pois1 as signatures1');	
assert( isequal(size(poi2,2),size(sig2,2)), 'We should have as many pois2 as signatures2');

% If one of the poi list is empty, we stop
if isequal(size(poi1), [0 0]) || isequal(size(poi2), [0 0])
	return;
end

%% Variable inputs

if nargin<10
	ruban_half_height = 2; % to be adapted with the alpha coefficient
	if nargin<9		
		if type_sig == -1
			epsilon=Inf;
		else
			epsilon=Inf;
		end
	end
end

pois_matrix = {};
if (display_param == 1) || (display_param == 2) || (display_param == 5)
	size_im1=size(im1);
	size_im2=size(im2);
else
	size_im=[max( max(poi1(2,:)),max(poi2(2,:)))+ruban_half_height, max( max(poi1(1,:)),max(poi2(1,:)))+ruban_half_height];
end

%	pois_matrix Computation : 
%	returns a matrix so that pois_matrix{1} and pois_matrix{2} respect the following conventions:
%	pois_matrix{1}(i,j) = 	[indices(poi(i,j))] if (i,j) is one of the poi of the picture 1,
%							[] there is no poi in (i,j)
%	pois_matrix{2}(i,j) =	[indices(poi(i,j))] if (i,j) is one of the poi of the picture 2,
%							[] there is no poi in (i,j)

pois_matrix_0={};


for i_image = 1:2	
%	i_image
    if (display_param == 1) || (display_param == 2) || (display_param == 5)
        if i_image==1
            size_im=size_im1;
        else
            size_im=size_im2;
        end
    end
    if size_im(1) > 1e4 || size_im(2) > 1e4
        error(sprintf('The rectified picture is too big (%dx%d), probably the camera calibration is wrong.', size_im(1), size_im(2)));
    end
    im_pois_0 = zeros(size_im(2:-1:1));
    im_pois = cell(double(size_im(2:-1:1))); % size_im must be in double

	if i_image==1			
	    pois_i = poi1;
		extremes_i=[1:ruban_half_height, size(im_pois,1)-ruban_half_height+1:size(im_pois,1)]; % rows not to consider 
	else	%i_image==2
		pois_i = poi2;
		extremes_i=[]; % rows not to consider 
	end

    % pois_i : i_image pois coordinates
    indices = sub2ind(size_im(2:-1:1),pois_i(1,:),pois_i(2,:));	%indices :indexes of the pois matrix of size [size_im(2),size_im(1)] 
    %optimisable (voir comment supprimer la boucle)
    for ind_=1:length(indices)
	    im_pois{indices(ind_)} = [im_pois{indices(ind_)}, ind_];
	end
  	im_pois_0(indices)=1;
  	
	im_pois(extremes_i,:)={[]};
	im_pois_0(extremes_i,:)=0;
	
    pois_matrix{i_image} = im_pois;
    pois_matrix_0{i_image} = im_pois_0;
    
end



if size(im1,1)~=0	%si on a im1 et im2 en input, on peut afficher les poi
    if display_param==5
   		indices1=find(pois_matrix_0{1});
		indices2=find(pois_matrix_0{2});
        figure
        hold on	
        im = [im1 im2];
        imagesc(im);
        axis ij;
        for i=1:length(indices1)
            plot(poi1(1,pois_matrix{1}{indices1(i)}),poi1(2,pois_matrix{1}{indices1(i)}), 'rx')
        end
        for j=1:length(indices2)
            plot(poi2(1,pois_matrix{2}{indices2(j)})+size(im1,2),poi2(2,pois_matrix{2}{indices2(j)}), 'gx')
        end
    end
end

minima = [];

% Row by row

% For each row of im1
% +-1 for the size of the comparison area
if size(im1,1) && display_param==5
	cVec = colormap(hsv(size(poi1,2)));
	k = 1;
end
%tic

i_start = 1+ruban_half_height;
i_end = size(pois_matrix_0{1},2)-ruban_half_height;
% fig_waitbar = waitbar(0, 'Matching')

for i = i_start:i_end

    % Progress bar
    % waitbar((i-i_start)/(i_end-i_start), fig_waitbar, 'Matching')

    % lookup strip in the 2nd picture
    i_imin = i-ruban_half_height;
    i_imax = i+ruban_half_height;
    
    % For each poi of im1
    poi_im1 = pois_matrix{1};
    poi_im2 = pois_matrix{2};
    poi_im1_0 = pois_matrix_0{1};
    poi_im2_0 = pois_matrix_0{2};
    % We keep the interesting part of the matrices
    poi_im1 = poi_im1(:, i);
    poi_im2 = poi_im2(:, i_imin:i_imax);
    poi_im1_0 = poi_im1_0(:, i);
    poi_im2_0 = poi_im2_0(:, i_imin:i_imax);
%    indices1 = poi_im1(find(~cellfun(@(t) isempty(t), poi_im1))); 
    indices1 = poi_im1(find(poi_im1_0)); % Le vecteur indices1 contient l'ensemble des indices dans poi1 des points d'intérêt de l'image 1 présents sur la ligne i
%    indices2 = poi_im2(find(~cellfun(@(t) isempty(t), poi_im2))); 
    indices2 = poi_im2(find(poi_im2_0)); % Le vecteur indices2 contient l'ensemble des indices dans poi2 des points candidats de l'image 2 présents dans la bande
    % Pour chaque poi de la ligne i
	for n=1:length(indices1)
	   ind_poi1=indices1{n};	% poi1(:,ind_poi1(:))=points d'interet considérés de l'image 1
	   for n1=1:length(ind_poi1)
		   c1 = floor(poi1(:,ind_poi1(n1)));	% Point d'intérêt considéré de l'image 1 
		   normes_blocs = [];		% contiendra l'ensemble des distances correspondantes entre les éléments de l'array candidats et c1
		   candidats = [];			% contiendra l'ensemble des indices dans poi2 des candidats pour le matching avec c1
		   % On évalue tous les candidats dans i=i_min:i_max et j=1:end
		   for m=1:length(indices2)
		   	   ind_poi2=indices2{m};
			   % attention diff d'images couleurs !
			   %[j i candidats_x(c) candidats_y(c)]
			   for m2=1:length(ind_poi2)
			   	  c2 = floor(poi2(:,ind_poi2(m2)));	% Point d'intérêt candidat de l'image 2
				  norme=0;
				  assert(isequal(size(sig1(:,ind_poi1(n1))),size(sig2(:,ind_poi2(m2)))),'All the signatures should have the same size');
			      if type_sig==-1
                      desc1 = sig1(:,ind_poi1(n1));
                      desc2 = sig2(:,ind_poi2(m2));
                      if (desc1(1) >= 0) && (desc2(1) >= 0)
                      	w1 = reshape(desc1,length(desc1)/3,1,3);
                      	w2 = reshape(desc2,length(desc2)/3,1,3);
					    norme = rmse(w1, w2);	% Calcul de l'erreur associée au matching des deux fenêtres 
				  	  else 
				  	  	norme = Inf;
				  	  end
				  elseif type_sig==-2	
				      desc1 = sig1(:,ind_poi1(n1));
                      desc2 = sig2(:,ind_poi2(m2));
                      if (desc1(1) >= 0) && (desc2(1) >= 0)
                      	w1 = reshape(desc1,length(desc1)/3,1,3);
                      	w2 = reshape(desc2,length(desc2)/3,1,3);
					    norme = zncc(w1, w2);	% Calcul de l'erreur associée au matching des deux fenêtres 
				  	  else 
				  	  	norme = Inf;
				  	  end	      
			      	  %norme = zncc(w1, w2);
			      elseif type_sig==1
                      desc1 = sig1(:,ind_poi1(n1));
                      desc2 = sig2(:,ind_poi2(m2));
                      %cmp_desc(desc1(5:end), desc2(5:end));
                      norme = zncc(desc1(5:end), desc2(5:end));
                  elseif type_sig==2
                      desc1 = sig1(:,ind_poi1(n1));
                      desc2 = sig2(:,ind_poi2(m2));
                      norme = acos(dot(desc1(5:end), desc2(5:end)));
				  else
				  	  error('type_sig value is invalid.');
				  end
			      normes_blocs = [normes_blocs norme];
			      candidats = [candidats ind_poi2(m2)];
		   	   end	%m1
		   end	%m

		   indices_inf = find(normes_blocs < epsilon);	% Position des poi dans l'array candidats qui respectent le critère de la distance < epsilon

           if ~isempty(indices_inf)
                minima = [minima min(normes_blocs)];
           end
		   match = {[], []};	% match{1} : indices des poi candidats ; match{2} : erreurs associées à ces poi
		   for d=1:length(indices_inf)
		   	   match{1} = [match{1} candidats(indices_inf(d))];
		   	   match{2} = [match{2} normes_blocs(indices_inf(d))];
		   end
		   [~,ix]=sort(match{2});
		   table_matched_poi{ind_poi1(n1)} = {match{1}(ix), match{2}(ix)}; % on classe les poi candidats du plus "probable" au moins "probable"
		   if size(im1,1) && display_param==5	   
			   if mod(k,19) == 0 && length(indices_inf)
			   	   c2=poi2(:,candidats(indices_inf(1)));	% on récupère le poi qu'on considère de l'image 2
			   	   j1 = c1(1);
			   	   j2 = c2(1);
			   	   i1 = c1(2);	% = i
			   	   i2 = c2(2);
                   hold on
                   plot([j1,j2+size(im1,2)],[i1,i2],'color',cVec(k,:));
    		   end	
			   k = k+1;
		   end
	   end	%n1
    end	%n
end
%toc
match={[] []};
for n=1:size(poi1,2)
	if n>length(table_matched_poi) || ~size(table_matched_poi{n},1)
		table_matched_poi{n}=match;
	end
end

if size(im1,1) && display_param==5
	hold off
end

recommended_epsilon = median(minima(find(minima>median(minima))));






