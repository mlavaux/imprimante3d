% Zero mean Normalized Cross-Correlation function (ZNCC)

% INPUT 
%   im1  : a n x n x z matrix representing the first picture  z \in {1,3}
%   im2  : a n x n x z matrix representing the second picture
%   

% OUTPUT
%   res = 1-ZNCC of the two pictures

% ZNCC is a correlation measure between two pictures.
% To be exploited by the rest of the algorithm, we return 1-zncc 
% because the information we use is the "error" resulting from the
% matching of the points of interest...

% The ZNCC has a major fault : when all the pixels of a window are
% identical, it returns 0/0, and we return 1 - (-1) = 2

function  res = zncc(w1, w2)
    assert(size(w1,3) == size(w2,3));
    
    if size(w1,3) == 1
        res = znccGray(w1, w2);
    else
        znccR = znccGray(w1(:,:,1), w2(:,:,1));
        znccG = znccGray(w1(:,:,2), w2(:,:,2));
        znccB = znccGray(w1(:,:,3), w2(:,:,3));
        res = mean([znccR, znccG, znccB]);
    end
end

function  res = znccGray(w1, w2)
    assert(size(w1,3) == 1, 'We assume we have gray scale pictures');
    assert(size(w2,3) == 1, 'We assume we have gray scale pictures');
    
    
	w1=double(w1); w2=double(w2);
    
    mu1=mean(mean(w1(:,:)));
    mu2=mean(mean(w2(:,:)));
    
    I1 = zeros(size(w1));
    I2 = zeros(size(w2));
    I1(:,:) = w1(:,:)-mu1;
    I2(:,:) = w2(:,:)-mu2;
    
    num = sum(sum(I1(:,:).*I2(:,:)));

    den = sqrt( sum(sum(I1(:,:).^2))*sum(sum(I2(:,:).^2)) );
    if den == 0 % ==> I1 ou I2 nul ==> image unie ==> num = 0
        quotient = -1; % On casse la continuité
    else
        quotient = num/den;
    end
    
    res = 1 - quotient;
end





