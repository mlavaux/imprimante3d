function cmp_desc(desc1, desc2)

    
    s = length(desc1);
    
    x = 5:s;
    
    y1 = desc1(5:end);

    y2 = desc2(5:end);

    diff = y1 - y2;
    
    figure;
    
    subplot(1,2, 1)
    size(x)
    size([y1 , y2])
    bar(x,[y1 , y2]);
    subplot(1,2,2)
    bar(x,diff);
 return