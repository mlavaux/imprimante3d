function index_matchings = extendMatchings(matchingij, verbose)

    % INPUT : 
    %   matchingij : a cell array containing n sets of pair matchings
    %                where n is the number of pictures and the length of matchingij
    %                matchingij{i} = [j k] means that in the ith picture
    %                the jth poi is matched with the kth poi of the (i+1)th picture
    %
    % OUTPUT
    %   index_matchings : m x n matching matrix 
    %                 with n = number of pictures
    %                 m = number of matchings
    %                 index_matchings(i,:)  = [7 0 4] means the 7th poi of the
    %                 picture 1 is matched with the 4th picture of the picture 3
    %                 and no poi was found in the second picture for this
    %                 matching.
    
    if nargin == 1
        verbose = false;
    end
    
    if verbose
        display('Extending the pair matchings...');
    end

    n =  length(matchingij);
    k = 1;	%k : matchings counter
    % We don't know in advance the maximum number of matchings
    % Probably, between pictures 2 and 3, there is more matchings than
    % between pictures 1 and 2. That why we don't create a full matrix of 0
    index_matchings=zeros(1,n);
    % We add the first pairs
    for ind = 1:length(matchingij{1})
        ind_i = matchingij{1}{ind}(1);
        ind_j = matchingij{1}{ind}(2);
        index_matchings(k,:)=zeros(1,n);
        index_matchings(k,1)=ind_i;
        index_matchings(k,2)=ind_j;
        k=k+1;
    end
    if verbose
        display(sprintf('%d/%d', 1,n));
    end
    for i=2:n-1
        for ind=1:length(matchingij{i})
            ind_i = matchingij{i}{ind}(1);
            ind_j = matchingij{i}{ind}(2);
            vec=find(index_matchings(:,i)==ind_i);
            if ~isempty(vec)
                % Extending the matching
                index_matchings(vec,i+1)=ind_j;
            else
                % A new matching
                index_matchings(k,:)=zeros(1,n);
                index_matchings(k,i)=ind_i;
                index_matchings(k,i+1)=ind_j;
                k=k+1;
            end
        end
        if verbose
            display(sprintf('%d/%d', i,n));
        end
    end
    for ind=1:length(matchingij{n})
        ind_i = matchingij{n}{ind}(1);
        ind_j = matchingij{n}{ind}(2);
        vec=find(index_matchings(:,n)==ind_i);
        if ~isempty(vec)
            index_matchings(vec,1)=ind_j;
        else
            index_matchings(k,:)=zeros(1,n);
            index_matchings(k,n)=ind_i;
            index_matchings(k,1)=ind_j;
            k=k+1;
        end
    end
return;
