% Export the mathing in the appropriate format
%
%	INPUTS : 
%       index_matchings : m x n matching matrix 
%               with n = number of pictures
%                    m = number of matchings
%               index_matchings(i,:)  = [7 0 4] means the 7th poi of the
%               picture 1 is matched with the 4th picture of the picture 3
%               and no poi was found in the second picture for this
%               matching.
%       pois            : pois coordinates where pois{i}(:,k) is 
%                          the kth poi in the ith image
%
%	OUTPUT : 
%       matchings : matchs with indexes replaced by coordinates and zeros by [-1 -1]
%
%

function matchings = export_matchings(index_matchings, pois)

    nb_matchings = size(index_matchings,1);
    nb_images    = length(pois);

    % Preconditions
    assert(nb_images == size(index_matchings,2));
    assert(min(min(index_matchings)) >= 0);

    % Initialized with -1
    matchings = zeros(nb_matchings, 2*nb_images) -1;
    
    for i = 1:nb_matchings
        matches    = index_matchings(i,:);
       for j=1:size(matches,2);
           image_pois = pois{j};
           if matches(j) > 0
               matchings(i, 2*(j-1)+1:2*j) = image_pois(:,matches(j))';
           end
       end
    end
    
    % Postconditions : 
    assert(size(matchings,1) == size(index_matchings,1));
    assert(size(matchings,2) == 2*size(index_matchings,2));

    % Exporting in the .xy format for tests
    save matchings.xy matchings '-ascii';
    %
end
