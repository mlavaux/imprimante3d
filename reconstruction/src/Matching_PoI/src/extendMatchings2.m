function [matchings, index_matchings] = extendMatchings2(matchingij, cameras, pois, verbose)

    % INPUT : 
    %   matchingij : a cell array containing n sets of pair matchings
    %                where n is the number of pictures and the length of matchingij
    %                matchingij{i} = [j k] means that in the ith picture
    %                the jth poi is matched with the kth poi of the (i+1)th picture
    %
    % OUTPUT
    %   matchings : matchs with indexes replaced by coordinates and zeros by [-1 -1]
    %   (the output of export_matchings)
    
    if nargin == 2
        verbose = false;
    end
    
    epsilon = 2;
    
    if verbose
        display('Extending the pair matchings...');
    end

    n =  length(matchingij);
    k = length(matchingij{1});	%k : matchings counter
    
    % 3d points : 
    
    % We add the first pairs
    imatchings = cell2mat(matchingij{1}');
    index_matchings = [imatchings zeros(k, n-2)];
    matchings       = [pois{1}(:,imatchings(:,1))' pois{2}(:,imatchings(:,2))' zeros(k, 2*n-4)-1];
    p3ds            = arrayfun(@(x,y,z,t) triangulate(cameras{1},cameras{2}, [x ; y], [z ; t]), matchings(:,1), matchings(:,2), matchings(:,3), matchings(:,4), 'UniformOutput', false);
    k = k+1;
    if verbose
        display(sprintf('%d/%d', 1,n));
    end
    
    for i=2:n
        j = i+1;
        if j > n
            j = 1;
        end
        for ind=1:length(matchingij{i}) 
            ind_i   = matchingij{i}{ind}(1);
            ind_j   = matchingij{i}{ind}(2);
            [ip jp] = find(index_matchings(:,i)==ind_i);
            coord_j = pois{j}(:,ind_j);
            new_matching = true;
            if ~isempty(ip) 
                proj    = cameras{j} * [p3ds{ip} ; 1];
                proj    = proj(1:2)/proj(3);
                if norm(proj-coord_j) < epsilon
                    % Extending the matching
                    index_matchings(ip,j)  = ind_j;
                    matchings(ip, 2*j-1:2*j) = coord_j;
                	new_matching = false;
                end
            end
            if new_matching
                % A new matching
                index_matchings(k,:) = zeros(1,n);
                index_matchings(k,i) = ind_i;
                index_matchings(k,j) = ind_j;
                coord_i   = pois{i}(:,ind_i);
                p3ds{k} = triangulate(cameras{i}, cameras{j}, coord_i, coord_j);
                matchings(k,:) = zeros(1,2*n)-1;
                matchings(k, 2*i-1:2*i) = coord_i';
                matchings(k, 2*j-1:2*j) = coord_j';
                k=k+1;
            end
        end
        if verbose
            display(sprintf('%d/%d', i,n));
        end
    end

end

function p3d=triangulate(c1, c2, p2d1, p2d2)
    b = [ p2d1(1)*c1(3,4) - c1(1,4) ;
          p2d1(2)*c1(3,4) - c1(2,4) ;
          p2d2(1)*c2(3,4) - c2(1,4) ;
          p2d2(2)*c2(3,4) - c2(2,4) ];
          
    A = [ c1(1,1:3) - p2d1(1) * c1(3,1:3) ; 
          c1(2,1:3) - p2d1(2) * c1(3,1:3) ;
          c2(1,1:3) - p2d2(1) * c2(3,1:3) ; 
          c2(2,1:3) - p2d2(2) * c2(3,1:3) ];
    p3d = A\b;
end
