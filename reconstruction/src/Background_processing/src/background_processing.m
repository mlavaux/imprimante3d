% Background removal and mask creation
% INPUT
%   - im          : image to be processed
%   - ellipses    : ellipses of the images, result of the calibration script
%                   ([] for digital images w/o calibration pattern)
%   - disparam    : verbose level
% OUTPUT
%   - image_no_bg : image without the background
%   - mask        : mask containing only the object
function [ image_no_bg, mask ] = background_processing(im, ellipses, disparam)

  % We use an HSV representation of the picture
  im_HSV = rgb2hsv(im);
  image_no_bg = im;
  mask = ones(size(im, 1), size(im, 2));

  im_HSV(:, :, 3) = uint8(im_HSV(:, :, 3) * 256);
  im_HSV(:, :, 2) = uint8(im_HSV(:, :, 2) * 256);
  
  % We create the histogram for Saturation and Value
  HV = zeros(256, 1);
  HS = zeros(256, 1);
  for i = 1:size(im_HSV, 1)
    for j = 1:size(im_HSV, 2)
      HV(im_HSV(i, j, 3) + 1) = HV(im_HSV(i, j, 3) + 1) + 1;
      HS(im_HSV(i, j, 2) + 1) = HS(im_HSV(i, j, 2) + 1) + 1;
    end
  end
  
  % Majority Value and Saturation
  [maxV, indV] = max(HV);
  [maxS, indS] = max(HS);
  
  % We assume that a point is in the background if its Value *and* Saturation
  % is close to the majority.
  for i = 1:size(im_HSV, 1)
    for j = 1:size(im_HSV, 2)
      if abs(im_HSV(i, j, 3) - indV) < 25
        if abs(im_HSV(i, j, 2) - indS) < 25
          image_no_bg(i, j, 3) = 0;
          mask(i, j) = 0;
        end
      end
    end
  end

  % We finish to compute the mask by filling the elipses of the calibration
  % pattern.
  if ~isempty(ellipses)
    for n = 1:4
      params = ellipse2param(ellipses(:, :, 1, n));
      xmin = round(params(1) - params(3)) - 20;
      xmax = round(params(1) + params(3)) + 20;
      ymin = round(params(2) - params(4)) - 20;
      ymax = round(params(2) + params(4)) + 20;
      if xmin > 0 && ymin > 0
        for i = ymin:ymax
          for j = xmin:xmax
            if ([j i 1] * ellipses(:, :, 1, n) * [j i 1]') <= 0
              mask(i, j) = 0;
            end
          end
        end
      end
    end
  end
  
  % Erosion then dilation to remove elipse residuals.
  sel = strel('disk', 5);
  mask = imerode(mask, sel);
  mask = imdilate(mask, sel);

end
