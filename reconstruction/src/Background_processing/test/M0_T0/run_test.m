function [err ok] = run_test()
  ok = false;
  err = Inf;

  im = imread('carre_vert.jpg');

  [nobg, mask] = background_processing(im, [], -1);

  figure, imshow(im);
  figure, imshow(mask);
