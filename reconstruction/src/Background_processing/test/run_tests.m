% Run every unitary test

addpath('../src/');

%------------------------------------------------
addpath('./M0_T0');
disp('----- TEST M0_T0 : LAUNCHING -----');
% eps,verbose,dispay,time,true
[ok err] = run_test();

if ok 
  disp('----- TEST M0_T0 : PASSED -----');
else
  disp('----- TEST M0_TO : FAILED -----');
end

pause;

rmpath('./M0_T0');
%-------------------------------------------------
addpath('./M0_T1');
disp('----- TEST M0_T1 : LAUNCHING -----');
% eps,verbose,dispay,time,true
[ok err] = run_test();

if ok 
  disp('----- TEST M0_T1 : PASSED -----');
else
  disp('----- TEST M0_T1 : FAILED -----');
end

rmpath('./M0_T1');
%-------------------------------------------------
