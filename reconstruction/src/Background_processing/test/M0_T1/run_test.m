function [err ok] = run_test()
  ok = false;
  err = Inf;
  
  load ellipses;

  im = imread('carre_vert_mire/cvm1.jpg');

  elps = ellipses(:, :, :, :, 1);

  [image_no_bg, mask] = background_processing(im, elps, -1);

  figure, imshow(im); 
  figure, imshow(mask);

