function exportMeshObj(name, cloud, triangs)

    % INPUT
    %    name    : Name of the file eg 'my_mesh.obj&'
    %    cloud   : matrix of 3D coordinates nb_point*3 2-dimension array
    %    triangs : Matrix of 3D indices of the points forming mesh triangle
    %		 nb_triangle*3 2-dimension array

  
    fid=fopen(name,'w');
    for i=1:size(cloud,1)
        fprintf(fid, 'v %f %f %f\n',cloud(i,1), cloud(i,2), cloud(i,3)); 
    end
    fprintf(fid,'g mesh\n');

    for i=1:size(triangs,1)
        fprintf(fid,'f %d %d %d\n',triangs(i,1), triangs(i,2), triangs(i,3));
    end
    fprintf(fid,'g\n\n');
    fclose(fid);