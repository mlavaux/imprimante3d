% function [t] = calc_Mesh_script(p,cameras,masks,...) 
% This script calculates the triangle of the mesh

% INPUT
%    p : matrix of 3D coordinates nb_point*3 2-dimension array
%    cameras  : camera cell containing m camera projection matrices
%    masks    : masks cell containing m matrix of the mask of the object

% OPTIONS :
%   'verbose'       : display infos
%   'algoMesh'      : algorithm used for the mesh

% OUTPUT
%     t        : Matrix of 3D indices of the points forming mesh triangle
%		 nb_triangle*3 2-dimension array
% 

% SUBFUNCTION
%   compute_system : compute the over-constraint system
%   check_reprojection : project the 3D-point on every 2D-mask and calculate the distance
%                         between the projection and the mask

% Load point of interest

function [t] = calc_Mesh_script(p, cameras, masks, varargin)
  quit = false;
  
  % Mandatory parameters
  if nargin < 3
    disp('Fatal Error : mask cell is expected as third argument');
    quit = true;
  end
  if nargin < 2
    disp('Fatal Error : camera cell is expected as second argument');
    quit = true;
  end
  if nargin < 1
    disp('Fatal error : cloud point is expected as first argument');
    quit = true;
  end
  if length(cameras)~=length(masks)
    disp('Fatal error : the number of masks and cameras should be the same');
    quit = true;
  end
  if size(p,1)==0
      disp('Fatal error : cloud is empty no mesh reconstruction possible');
      quit = true;
  end
  if size(p,2)~=3
      disp('Fatal error : cloud must be an N*3 array');
      quit = true;
  end

  % Optionals parameters
  nVarargin = length(varargin);

  % Default value
  algorithm = 'crust';
  verbose = false;

  for i = 1:nVarargin
    if strcmp(varargin{i},'crust')
      algorithm = 'crust';
    elseif strcmp(varargin{i},'convexHull')
      algorithm = 'convexHull';
    elseif strcmp(varargin{i},'delaunayFaceRemoval')
      algorithm = 'delaunayFaceRemoval';
    elseif strcmp(varargin{i},'verbose')
      verbose = true;
    end
  end

  if quit
    t = [];
    return
  end

  % Proceed with the function
  if verbose
    fprintf('Running function with following algorithm %s% \n',algorithm);
  end
 
  if strcmp(algorithm,'convexHull')
    [t] = ConvexHull(p);
  elseif strcmp(algorithm,'delaunayFaceRemoval')
    [t] = reprojDelaunay(p,masks, cameras);
  else
    [t, tnorm] = MyRobustCrust(p);
  end

end
