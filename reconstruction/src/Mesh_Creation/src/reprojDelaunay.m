%% Delaunay Reprojection 
%
% Calcul of the delaunay trianglation of the cloud 
% computes the batycenter of this triangle then send them
% the function called calculs if the barycenter are in or out the masks
% and returns the indices of the points out the masks 
% this points correspond to the triangle that should be removed
%
% Syntax:
%
% [triangle]=reprojDelaunay(p,masks, cameras)
%
% Input:
%              p is a Nx3 array containing the 3D set of points
%              Cell containing cameras matrix
%              Cell containing masks
%
% Output:
%              t is a nx3 array containing the indices of the points
%              of p forming triangle of the convex hull.
%

function [t]=reprojDelaunay(p,masks, cameras)

tri = delaunay(p);

t=[];

for i=1:size(tri,1)
    t = [t
        tri(i,1) tri(i,2) tri(i,3)
        tri(i,2) tri(i,3) tri(i,4)
        tri(i,3) tri(i,4) tri(i,1)
        tri(i,4) tri(i,1) tri(i,2)];
end

x=[];
y=[];
z=[];

for k=1:size(t,1)

x = [x
     (p(t(k,1),1)+p(t(k,2),1)+p(t(k,3),1))/3];
 
y = [y
     (p(t(k,1),2)+p(t(k,2),2)+p(t(k,3),2))/3];
 
z = [z
     (p(t(k,1),3)+p(t(k,2),3)+p(t(k,3),3))/3]; 

end


gravityCenters = [x y z];

[badFaces] = faceRemoval(gravityCenters, masks, cameras);

t(badFaces,:)=[];

end