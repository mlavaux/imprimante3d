function [] = visuMesh(cloud,mesh,varargin)
%% plot the points cloud

  meshOnly = false;
  for i=1:length(varargin)
    if strcmp(varargin{i},'mesh-only')
      meshOnly = true;
    end
  end

  if ~meshOnly
    figure(1);
    set(gcf,'position',[0,0,1280,800]);
    subplot(1,2,1)
    hold on
    axis equal
    title('Points Cloud','fontsize',14)
    plot3(cloud(:,1),cloud(:,2),cloud(:,3),'g.')
    view(3);
    axis vis3d
  end

  %% plot of the output triangulation

  subplot(1,2,2)
  hold on
  title('Output Triangulation','fontsize',14)
  axis equal
  % plot of the treated surface
  trisurf(mesh,cloud(:,1),cloud(:,2),cloud(:,3),'facecolor','c','edgecolor','b')
  view(3);
  axis vis3d

end
