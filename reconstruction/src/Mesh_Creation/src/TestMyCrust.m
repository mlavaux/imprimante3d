% WARNING WORKSPACE WILL BE CLEARED 
clc
clearvars
close all

% Instructions :

% just uncomment one of the following line and run
% each line correspond to a test matrix
% both *.mat and *.p3d are acceptable inputs
% if the loaded file does not only create a variable named p 
% you will have to rename it (it explains the "p=house;clear house;" part)

%% Points cloud :

% Unitary tests

%  load ../test/cube.p3d;p=cube;clear cube;
 load ../test/tetra.p3d;p=tetra;clear tetra;
% load ../test/gael_issue.p3d;p=gael_issue;clear gael_issue;

% Complexs tests

% load ../test/Block.mat
% load ../test/Skull.mat
% load ../test/Standford_Bunny.mat
% load ../test/Horse.mat
% load ../test/hippo.mat
% load ../test/Elephant.mat
% load ../test/Chair.mat
% load ../test/gargo50k.mat
% load ../test/OilPump.mat
% load ../test/Knot.mat
% load ../test/Beethoven.mat;p=V;clear V F;
% load ../test/Vertebra1.mat
% load ../../../media/House_3D_Gael/house.p3d;p=house;clear house;
% load ../../3D_Cloud_Creation/test/result/cloud.mat;p=result;clear result;
% load ../../3D_Cloud_Creation/test/ %%TODO a completer pour suzanne

%% Run  program

% Input:
%               p is a N*3 array containing the 3D set of points
% Outputs:
%               t is a number_of_faces*3 array
%               each row defines a face
%               the valus are the index of the point in p(:,i)
%
%               tnorm outwards normals of triangles, n*3 array .  

[t,tnorm]=MyRobustCrust(p);

visuMesh
