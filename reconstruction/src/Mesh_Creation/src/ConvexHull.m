%% ConvexHull
%
% Calcul of the convex hull of a set of 3D points
%
% Syntax:
%
% [triangle]=ConvexHull(p);
%
% Input:
%              p is a Nx3 array containing the 3D set of points
% Output:
%              triangle is a nx3 array containing the indices of the points
%              of p forming triangle of the convex hull.
%

function [triangle]=ConvexHull(p)

    % duration information
    starttime=clock;

    % input error check
    if nargin>1
        error('The only input must be the Nx3 array of points');
    end

    [n m]=size(p);

    if m ~=3 || n < 4
       error('Input 3D points must be stored in a Nx3 array, N>3');
    end 
    
    % addition of the indice in the last row
    for i=1:n
        p(i,4) = i;
    end
    
    clear  m n
    
    % initialisation (returns a couple of indices)
    [cloud first second]=init2d(p);
    
    [cloud edge triangle]=init3d(cloud,first,second);

    % main part of the algorithm
	while size(edge,1)>1

        % as long as all edge are not all treated
        % calcul a triangle fitting the criterion
    	[edge triangle]=nextTriangle(p,edge,triangle);

	end 
    
    time=etime(clock,starttime);
    fprintf('Total Time: %4.4f s\n',time)
    
end

%% Initialisation 2D
% finds a couple of point on the frontier 

function [remind pMax second]=init2d(cloud)

    % finds the first point (ones of them with the greater x coordinate)
    max = cloud(1,1);
    iMax = 1;
    n = size(cloud,1);
    
    for i = 1:n
        if cloud(i,1)>max
            max = cloud(i,1);
            iMax = i;
        end
    end
    
    pMax = cloud(iMax,:);
    
    % cloud without the first point
    first = [cloud(iMax,1:2) 1];
    if iMax == 1
        remind = cloud(2:n,:);
    else
        remind = [cloud(1:iMax-1,:);cloud(iMax+1:n,:)];
    end
    
    found = false;
    iTest = 0;

    % searchs the second point (all the projection of the remaining points 
    % should be on the left side of the two firsts points)
    while ~found
        
        iTest = iTest + 1;
        toTest = [remind(iTest,1:2) 1];
        
        if iTest == 1
            toCheck = remind(2:n-1,:);
        else
            toCheck = [remind(1:iTest-1,:);remind(iTest+1:n-1,:)];
        end
        
        ok = true;
        isEmpty = false;
        iCheck = 1;
        
        % verification of the criterion
        while ok && ~isEmpty
            check = [toCheck(iCheck,1:2) 1];
            ok = (det([first;toTest;check])>=0);
            isEmpty = ((iCheck-n+2) == 0);
            iCheck = iCheck + 1;
        end
        
        % all the points have been check successfully 
        found = (ok && isEmpty);
        
    end    
        
    second = remind(iTest,:);
    
    % up to date cloud
    remind = toCheck;
        
end


%% Initialisation 3D
% finds the first triangle and initialize the segment

function [remind edge triangle]=init3d(cloud,first,second)

    p1 = [first(1:3) 1];
    p2 = [second(1:3) 1];
     
    found = false;
    n = size(cloud,1);
    iTest = 0; 
          
    % searchs the third point (all the points remaining should be on the
    % left side of this triangle)
    while ~found
        
        iTest = iTest + 1;
        toTest = [cloud(iTest,1:3) 1];
        
        if iTest == 1
            toCheck = cloud(2:n,:);
        else
            toCheck = [cloud(1:iTest-1,:);cloud(iTest+1:n,:)];
        end
               
        ok = true;
        isEmpty = false;
        iCheck = 1;
        
        % verification of the criterion
        while ok && ~isEmpty
            check = [toCheck(iCheck,1:3) 1];
            ok = (det([p1;p2;toTest;check])>=0);
            isEmpty = ((iCheck-n+1) == 0);
            iCheck = iCheck + 1;
        end
        
        % all the points have been check successfully 
        found = (ok && isEmpty);
        
    end
    
    new = cloud(iTest,:);
    
    % up to date cloud
    remind = toCheck;
    
    edge = [first second
            second new
            new first];
    
    triangle = [first(4) second(4) new(4)];
           
end

%% Triangle

function [edge triangle]=nextTriangle(p,edge,triangle)

    p1 = [edge(1,1:3) 1];
    p2 = [edge(1,5:7) 1];
    
    save1 = edge(1,1:4);
    save2 = edge(1,5:8);
    
    i1 = save1(4);
    i2 = save2(4);
    
    nEdge = size(edge,1);
    
    edge = edge(2:nEdge,:);
     
    found = false;
    iTest = 0;
                
    % searchs a third point for the first edge (all the points remaining 
    % should be on the left side of this triangle)
    while ~found

        iTest = iTest + 1;
        toTest = [p(iTest,1:3) 1];
        ind = p(iTest,4);        
        diff = i1~=ind && i2~=ind;   
        toCheck = p;
        ok = true;
        isEmpty = (size(toCheck,1) == 0);
        iCheck = 1;
        
        % verification of the criterion
        while ok && ~isEmpty 
            
            check = [toCheck(iCheck,1:3) 1];
            ok = (det([p1;p2;check;toTest])>=-5*eps);
            same = check==toTest;          
            if same(1)==1 && same(2)==1 && same(3)==1
                ok = true;
            end
            isEmpty = ((iCheck-size(toCheck,1)) == 0);
            iCheck = iCheck + 1;
            
        end
        
        new = p(iTest,:);
        i3 = new(4);
        isNewV = [];
        
        for it=1:size(triangle)
            
            curr = triangle(it,:);       
                      
            bool1 = i1==curr(1) && i2==curr(2) && i3==curr(3);
            bool2 = i1==curr(1) && i3==curr(2) && i2==curr(3);
            bool3 = i2==curr(1) && i1==curr(2) && i3==curr(3);
            bool4 = i2==curr(1) && i3==curr(2) && i1==curr(3);
            bool5 = i3==curr(1) && i1==curr(2) && i2==curr(3);
            bool6 = i3==curr(1) && i2==curr(2) && i1==curr(3);
                        
            isNewV = [isNewV (~bool1&&~bool2&&~bool3&&~bool4&&~bool5&&~bool6)];
            
        end
                
        isNew = (size(find(isNewV==0),2)==0);
        
        % all the points have been check successfully and the triangle does
        % not already exists
        found = (diff && ok && isEmpty && isNew);        

    end
        
    
    % update the edges according to the new triangle
    kTest = find(edge(:,4)==ind);
    kTest = [kTest;find(edge(:,8)==ind)];
                             
    k1 = find(edge(:,4)==i1);
    k1 = [k1;find(edge(:,8)==i1)];
    match1 = false;
    
    for i=1:size(kTest,1)
        
        ki = kTest(i);
        
        for k = 1:size(k1,1)
            
            if ki == k1(k)
 
                match1 = true;
                
                if ki == 1
                    edge = edge(2:size(edge,1),:);
                elseif ki == size(edge,1)
                    edge = edge(1:ki-1,:);
                else
                    edge = [edge(1:ki-1,:);edge(ki+1:size(edge,1),:)];
                end
                
            end
            
        end
        
    end
    
    
    kTest = find(edge(:,4)==ind);
    kTest = [kTest;find(edge(:,8)==ind)];
    
    k2 = find(edge(:,4)==i2);
    k2 = [k2;find(edge(:,8)==i2)];
    match2 = false;
    
    for i=1:size(kTest,1)
        
        ki = kTest(i);
        
        for k = 1:size(k2,1)
            
            if ki == k2(k)
                
                match2 = true;
                
                if ki == 1
                    edge = edge(2:size(edge,1),:);
                elseif ki == size(edge,1)
                    edge = edge(1:ki-1,:);
                else
                    edge = [edge(1:ki-1,:);edge(ki+1:size(edge,1),:)];
                end
                
            end
            
        end
        
    end
    
    % checks that one edge only supports two triagles
    full1 = 0;
    full2 = 0;
    
    for it=1:size(triangle)
        
        curr = triangle(it,:);
        
        bool11 = i1==curr(1) && ind==curr(2);
        bool12 = i1==curr(1) && ind==curr(3);
        bool13 = i1==curr(2) && ind==curr(1);
        bool14 = i1==curr(2) && ind==curr(3);
        bool15 = i1==curr(3) && ind==curr(1);
        bool16 = i1==curr(3) && ind==curr(2);
        
        bool21 = i2==curr(1) && ind==curr(2);
        bool22 = i2==curr(1) && ind==curr(3);
        bool23 = i2==curr(2) && ind==curr(1);
        bool24 = i2==curr(2) && ind==curr(3);
        bool25 = i2==curr(3) && ind==curr(1);
        bool26 = i2==curr(3) && ind==curr(2);
        
        
        if bool11 || bool12 ||bool13 ||bool14 ||bool15 ||bool16
            full1 = full1+1;
        end
        
        if bool21 || bool22 ||bool23 ||bool24 ||bool25 ||bool26
            full2 = full2+1;
        end
        
    end
    
    edgeFull1 = (full1==2);
    edgeFull2 = (full2==2);
    
    if ~match1 && ~edgeFull1
        
        edge = [edge
            save1 new];
        
    end
    
    if ~match2 && ~edgeFull2
        
        edge = [edge
            new save2];
    end
    
    % triangle update
    triangle = [triangle
                i1 i2 ind];
       
end