% WARNING WORKSPACE WILL BE CLEARED 
clc
clearvars
close all

% Instructions :

% just uncomment one of the following line and run
% each line correspond to a test matrix
% both *.mat and *.p3d are acceptable inputs
% if the loaded file does not only create a variable named p 
% you will have to rename it (it explains the "p=house;clear house;" part)

%% Points cloud :

% Unitary tests

% load ../test/cube.p3d;p=cube;clear cube;
% load ../test/tetra.p3d;p=tetra;clear tetra;
% load ../test/gael_issue.p3d;p=gael_issue;clear gael_issue;
 load cloud.mat;

% load ../test/Block.mat

% p =[ -0.5633    0.0591    1.8204  
%     0.5627   -0.0398    1.8218    
%    -0.0494   -0.5527    1.8197   
%    -0.2544    0.3228    2.1102  ];

%  p = [0 0 0
%  1 0 0
%  1 0 1
%      4 0 3
%     2 4 1
%     0 1 0
%     0 0 0
%     2 8 7
%     3 2 4
%     5 5 1];

% p = [0 0 0
% 1 0 0
% 1 0 1
% 0 0 1
% 0 1 0
% 1 1 0 
% 1 1 1
% 0 1 1];


%  p = [1 1 1
%      0 0 0
%     2 0 0
%     0 1 0];


%% Run  program

% Input:
%               p is a N*3 array containing the 3D set of points
% Outputs:
%               t is a number_of_faces*3 array
%               each row defines a face
%               the valus are the index of the point in p(:,i)
%
%               tnorm outwards normals of triangles, n*3 array .  

[triangle]=ConvexHull(p);

figure(1);
set(gcf,'position',[0,0,1280,800]);
subplot(1,2,1)
hold on
axis equal
title('Points Cloud','fontsize',14)
plot3(p(:,1),p(:,2),p(:,3),'g.')
% plot3(p(6,1),p(6,2),p(6,3),'r.')
% plot3(p(11,1),p(11,2),p(11,3),'r.')
trisurf(triangle,p(:,1),p(:,2),p(:,3),'facecolor','c','edgecolor','b')
view(3);
axis vis3d

% visuMesh