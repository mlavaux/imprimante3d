function [ok] = run_test(verbose,run_all)

ok = true;

if(nargin < 1)
    verbose = true;
end

if(nargin < 2)
    run_all = false;
end

if(nargin < 1)
    verbose = true;
end

if ~run_all
    addpath('../../src');
end

if verbose
    disp('M7_TU2 : Starting the Test. Purpose : reconstructing the convex hull of a tetrahedron');
end

test = [0 0 0
    2 0 0
    1 2 0
    1 1 1];

[triangle]=ConvexHull(test);

t = convhull(test);

isNewV = [];

for it=1:size(triangle)
    
    curr = triangle(it,:);
    t1 = t(it,1);
    t2 = t(it,2);
    t3 = t(it,3);
    
    bool1 = t1==curr(1) && t2==curr(2) && t3==curr(3);
    bool2 = t1==curr(1) && t3==curr(2) && t2==curr(3);
    bool3 = t2==curr(1) && t1==curr(2) && t3==curr(3);
    bool4 = t2==curr(1) && t3==curr(2) && t1==curr(3);
    bool5 = t3==curr(1) && t1==curr(2) && t2==curr(3);
    bool6 = t3==curr(1) && t2==curr(2) && t1==curr(3);
    
    isNewV = [isNewV (~bool1&&~bool2&&~bool3&&~bool4&&~bool5&&~bool6)];
    
end

if (size(find(isNewV==0),2)==0) || size(triangle,1)~=size(t,1) || size(triangle,2)~=size(t,2)
    ok = false;
end

end


