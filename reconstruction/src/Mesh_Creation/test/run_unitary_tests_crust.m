% Run every unitary test

addpath('../src/');

%------------------------------------------------
addpath('./M7_TU0');
disp('----- TEST M7_TU0 : LAUNCHING -----');

[ok] = run_test(true,true);

if ok 
  disp('----- TEST M7_TU0 : PASSED -----');
else
  disp('----- TEST M7_TUO : FAILED -----');
end

rmpath('./M7_TU0');
%------------------------------------------------
addpath('./M7_TU1');
disp('----- TEST M7_TU1 : LAUNCHING -----');

[ok] = run_test(true,true);

if ok 
  disp('----- TEST M7_TU1 : PASSED -----');
else
  disp('----- TEST M7_TU1 : FAILED -----');
end

rmpath('./M7_TU1');
%-------------------------------------------------