% Run every unitary test

addpath('../src/');

%------------------------------------------------
addpath('./M7_TU2');
disp('----- TEST M7_TU2 : LAUNCHING -----');

[ok] = run_test(true,true);

if ok 
  disp('----- TEST M7_TU2 : PASSED -----');
else
  disp('----- TEST M7_TU2 : FAILED -----');
end

rmpath('./M7_TU2');
%------------------------------------------------


