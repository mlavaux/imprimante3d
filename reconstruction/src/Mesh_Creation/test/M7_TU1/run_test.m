function [ok] = run_test(verbose,run_all)

  ok = true;

  if(nargin < 1)
    verbose = true;
  end
  
  if(nargin < 2)
    run_all = false;
  end
  
  if(nargin < 1)
    verbose = true;
  end
  
  if ~run_all
    addpath('../../src');
  end
  
  if verbose
    disp('M7_TU1 : Starting the Test. Purpose : reconstructing a tetrahedron from 4 corrects points and 1 incorrect point');
  end
  
  p = [0 0 0
 	   2 0 0
       1 2 0
       1 1 1
       100 0 0];
      
  [t,tnorm]=MyRobustCrust(p);
  
  result =[1 2 3
           1 2 4
           1 3 4
           2 3 4];
       
  ok = isequal(result,t);
    
end
  
  