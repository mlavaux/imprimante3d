function [ok] = run_test(verbose,run_all)

  ok = true;

  if(nargin < 1)
    verbose = true;
  end
  
  if(nargin < 2)
    run_all = false;
  end
  
  if(nargin < 1)
    verbose = true;
  end
  
  if ~run_all
    addpath('../../src');
  end
  
  if verbose
    disp('M7_TU0 : Starting the Test. Purpose : reconstructing a tetrahedron from 4 corrects points');
  end
  
  test = [0 0 0
          2 0 0
          1 2 0
          1 1 1];
      
  [t,tnorm]=MyRobustCrust(test);
  
  d = DelaunayTri(test);
  
  if (size(t,1) ~= size(d,1))||(size(t,2) ~= size(d,2))||(d(1,1) ~= t(1,1))||(d(1,2) ~= t(1,2))||(d(1,3) ~= t(1,3))||(d(1,4) ~= t(1,4))
      ok = false;
  end
  
end
  
  