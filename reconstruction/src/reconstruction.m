%clear; close all hidden;

function ret = reconstruction(folder,max_pictures,format_img,display_vect,varargin)
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%         PARAMETERS
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


  % Pictures folder
  %%%%%%%%%%%%%%%%%%%%%%% DEFAULT VALUES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %folder = '../media/teabox';
  %format = '/*.JPG';
  % 
  num_plan = 2;
  addpath(folder);
  % Load cameras projection matrix
  load_cameras = false;
  save_cameras = false;
  
  % Generate mask from alpha layer
  load_masks = false;
  save_masks = false;
  
  % Load points of interests
  load_pois = false;
  save_pois = false;
  
  % Load SURF descriptors
  load_descs = false;
  save_descs = false;
  calcul_descs = false;	% Computation of descriptors at the same time as computation of pois
  
  % Load matchings
  load_matchings = false;
  save_matchings = false;
  
  % Load Cloud
  load_cloud = false;
  save_cloud = false;
  
  % Save mesh
  save_mesh = false;
  
  % Signature type (for detection/matching)
  %  1 --> SURF
  % -1 --> RMSE
  % -2 --> ZNCC
  type_signatures = -1;
  

  % Number of pictures
  % Put -1 to take all the pictures in the given folder
  
  verbose = false;
  algo = 'none';
  nopath = false; 
  extendMatching = 1; % out of 2
  remove_last_quartile = false;
  face_removal = false;
  reprojection = false;
  %%%%%%%%%%%%%%%%%%%%% PARSING PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  for i=1:length(varargin)
  
    if strcmp(varargin{i},'verbose')
      verbose = true;
    end
    if strcmp(varargin{i},'nopath')
      nopath = true;
    end
    % Load values
    if strcmp(varargin{i},'load')
      array = varargin{i+1};
      
      for a = 1:length(array)
        if strcmp(array(a),'cameras')
          load_cameras = true;
        end
        if strcmp(array(a),'masks')
          load_masks = true;
        end
        if strcmp(array(a),'pois')
          load_pois = true;
        end
        if strcmp(array(a),'descs')
          load_descs = true;
        end
        if strcmp(array(a),'matchings')
          load_matchings = true;
        end
      end
    end
   % Save values
    if strcmp(varargin{i},'save')
      array = varargin{i+1};
      for a = 1:length(array)
        if strcmp(array(a),'cameras')
          save_cameras = true;
        end
        if strcmp(array(a),'masks')
          save_masks = true;
        end
        if strcmp(array(a),'pois')
          save_pois = true;
        end
        if strcmp(array(a),'descs')
          save_descs = true;
        end
        if strcmp(array(a),'matchings')
          save_matchings = true;
        end
        if strcmp(array(a),'cloud')
          save_cloud = true;
        end
        if strcmp(array(a),'mesh')
          save_mesh = true;
        end
      end
    end 
    % Matching parameters
     if strcmp(varargin{i},'SURF')
       type_signatures = 1;
     end
     if strcmp(varargin{i},'RMSE')
       type_signatures = -1;
     end
     if strcmp(varargin{i},'ZNCC');
       type_signatures = -2;
     end
     if strcmp(varargin{i},'remove_last_quartile');
       remove_last_quartile = true;
     end
  
    % 3D reconstruction parameters
    if strcmp(varargin{i},'reprojection')
      reprojection = varargin{i};
    end
  
    if strcmp(varargin{i},'loo')
      algo = 'loo';
    end
  
    if strcmp(varargin{i},'ransac')
      algo = 'ransac';
    end
    
    % Mesh parameters
    if strcmp(varargin{i},'crust')
      algoMesh = 'crust';
    end
  
    if strcmp(varargin{i},'convexHull')
      algoMesh = 'convexHull';
    end
    
    if strcmp(varargin{i},'delaunayFaceRemoval')
      algoMesh = 'delaunayFaceRemoval';
    end
    
    if strcmp(varargin{i},'extendMatching2')
      extendMatching = 2;
    end
    
    if strcmp(varargin{i},'extendMatching1')
      extendMatching = 1;
    end
  

  
  end
  if type_signatures < 0
    poi_str = 'Harris';
  else
    poi_str = 'SURF_opencv';
  end
  if type_signatures == 1
    sig_str = 'SURF';
  elseif type_signatures == -1
    sig_str = 'RMSE';
  elseif type_signatures == -2
    sig_str = 'ZNCC';
  end

  cloud_str = algo;
  mesh_str = algoMesh;

  if strcmp(algoMesh,'crust') && face_removal
    disp('Face removal parameter is only authorized with ''convexHull''.');
    face_removal = false;
  end
 

  % Sub-module path
  if ~nopath
    addpath('Background_processing/src');
    addpath('Calcul_PoI/src');
    addpath('Calibration/src/');
    addpath('Calibration/src/bin');
    addpath('3D_Cloud_Creation/src');
    addpath('Matching_PoI/src');
    addpath('Mesh_Creation/src');
    addpath('Common/src');
  end

  % Vector of display variables for each module ; 
  % Each module takes an integer parameter with different levels of display
  % size(display_vect)=number of modules
  % display_vect(1) : calibration
  % display_vect(2) : masks computation
  % display_vect(3) : pois computation
  % display_vect(4) : matching 
  % display_vect(5) : cloud computation
  % display_vect(6) : mesh computation
  % Convention : display = 0 if we do not want any plot except the final one
  %display_vect = zeros(1,6);
  %display_vect(4)=1;
  
  %TEMPORARY%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Disable the plots, except the final one
  no_plots = -1;
  
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  %% --- Calibration
  if verbose
      disp('Calibration : Launching the module');
  end

  

  % Create the input I
  D = dir(strcat(folder, format_img));
  nb_pictures = numel(D);
  if max_pictures > -1
      nb_pictures = min(max_pictures, nb_pictures);
  end
  imcell         = cell(1, nb_pictures);
  images_origine =  cell(1, nb_pictures);
  alphas          = cell(1, nb_pictures);
  masks          = cell(1, nb_pictures);
  for i = 1:nb_pictures
    I(:,:,:,i) = imread(strcat(strcat(folder, '/'), D(i).name));
    [images_origine{i}, ~, alphas{i}] = imread(strcat(strcat(folder, '/'), D(i).name));
  end
  
  if max_pictures > 0
      images_origine        = reduce_array_length(images_origine, nb_pictures);
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%% Calibration %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if ~load_cameras
        [P,K,M] = calibrage(I, num_plan, no_plots);
    if P == -1
      error('Calibration fail. Exiting.');
      quit
    else
      % Create the output cameras
      cameras = {};
      for i=1:size(P,3)
          cameras{i} = P(:,:,i);
      end
    end
  else
    if verbose
      disp('Calibration : loading values');
    end
    try
      load cameras.mat;
	  catch
	    error('cameras.mat not found');
	  end
  end
 
  %%%% Reduce the amount of picture used %%%%
  if max_pictures > 0
      cameras        = reduce_array_length(cameras, nb_pictures);
  end
  if save_cameras
    path = strcat(folder,'/cameras_save.mat');
    disp(strcat('Saving the cameras in ',path));
    save(path, 'cameras');
  end

  %%%%%%%%%%%%%%%%%%%%%%%% Background suppression %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  % TODO mask computation for real pictures
  if verbose
    disp('Background suppression : launching the module');
  end
  if load_masks
      % Mask from the alpha channel of the computer graphic pictures
      if verbose
        disp('Masks : loading the masks');
      end
      masks = cellfun(@create_background_mask, alphas, 'UniformOutput', false);
  else
    if verbose
      disp('Masks : full masks creation');
    end
      for i = 1:nb_pictures
          im_size = size(images_origine{i});
          if exist('M')
            [nobg, mask_tmp] = background_processing(images_origine{i}, M(:, :, :, :, i), no_plots);
          else
            [nobg, mask_tmp] = background_processing(images_origine{i}, [], no_plots);
          end
          masks{i} = mask_tmp;
      end
  end
  if save_masks
    path = strcat(folder,'/masks_output.mat');
    disp(strcat('Saving the masks in ',path));
    save(path, 'masks');
  end

  %%%%%%%%%%%%%%%%%%%%%%% Calculation of PoI %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  calcul_descs = ~load_pois;
  if verbose
    display('Points of Interest : launching the module');
  end
  descs = {};
  for i=1:nb_pictures
      descs{i} = [];
  end
  
  if load_pois
    if verbose
      disp('PoI : loading the PoI');
    end
	try
      load pts.mat;
	catch
	  error('pts.mat not found');
	end
  else
    if verbose
      disp('PoI : Calculating the PoI : Harris method');
    end
     [pts, descs] = keypoints(images_origine, calcul_descs);
     %TODO : computation of descs...
  end
  if load_descs
  	if verbose
  		disp('Desc : loading the descriptors');
  	end
  	  try
	  	  load descs.mat;
	  catch
	  	  error('descs.mat not found');
	  end
  end
  if save_pois
    path = strcat(folder,['/pts' '_' 'output' '_' poi_str '.mat']);
  	disp(strcat('Saving the points of interest in ',path));
    save(path, 'pts');
  end
  close all;
  if verbose
    display('POIs computed');
  end
  if save_descs
    path = strcat(folder,['/descs' '_output_' 'poi_str' '.mat']);
    save(path, 'descs');
  end
  if max_pictures > 0
      pts        = reduce_array_length(pts, nb_pictures);
      descs      = reduce_array_length(descs, nb_pictures);
  end
  if display_vect(3)
      visuPOI;
  end
  
   %%%%%%%%%%%%%%%%%%%%%%% Matching of PoI %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if verbose
    display('Matching : Launching the module');
  end
  if load_matchings
    if verbose
      disp('Matching : loading matchings.xy');
    end
    try 
	  	load matchings.xy	% contenu dans folder variable matchings
	catch
		error('matchings.xy not found');
    end
  else
    if verbose
      disp('Matching : calculating matchings.xy');
    end
    matchings = matching(images_origine,pts,cameras, type_signatures, descs, extendMatching, remove_last_quartile, display_vect(4),verbose);
  end
  
  if save_matchings 
     path = strcat(folder,['/matchings' '_output_' poi_str '_' sig_str '.xy'] );
  	 disp(strcat('Saving the matchings in ',path));
     save(path, 'matchings', '-ASCII'); 
  end
   %%%%%%%%%%%%%%%%%%%%%%% Cloud creation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if verbose
    display('Cloud creation : launching the module');
  end
  if reprojection
    [p3d errors stats exec_time] = calc_3D_script(matchings, cameras, masks,'reprojection',algo);
  else
    [p3d errors stats exec_time] = calc_3D_script(matchings, cameras, masks,algo);
  end
  
  p = p3d.cloud;
  if save_cloud
 

    path = strcat(folder,['/cloud' '_output_' poi_str '_' sig_str '_' algo '.mat']);
  	disp(strcat('Saving the cloud in ',path));
    save(path,'p');
  end
   
  % param = [display_cloud,display_histogram,display_graph,display_verbose]
  

  if display_vect(5)
    param = [true true true false];
    display_stats(p3d,errors,stats,exec_time,param);
  end

  if verbose
    param = [false false false true];
    display_stats(p3d,errors,stats,exec_time,param);
  end
  
  %p = clearcloud(p, masks, cameras);
  
    %%%%%%%%%%%%%%%%%%%%%%% Mesh creation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if verbose
    display('Mesh generation : launching the module');
  end
 
  [t] = calc_Mesh_script(p, cameras, masks, algoMesh);
 
  if(size(t) > 0)
    if save_mesh
      path = strcat(folder,['/mesh_output_' poi_str '_' sig_str '_' algo '_' algoMesh '.mat']);
      disp(strcat('Saving the mesh in ',path));
      save(path,'t');
    end
  
    if verbose
      visuMesh(p,t);
    end
  
    ret = 1;
  
    if verbose
      display('Mesh exportation');
    end
    exportMeshObj('mesh.obj', p, t);
  else
    ret = 0
  end
end
