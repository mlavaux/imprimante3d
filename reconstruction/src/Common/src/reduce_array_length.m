% This function reduce the number of elements of an horizontal/vertical cells array

% INPUT
%   array         : the cells array to resize
%   new_length    : length of the resized cells array
% OUTPUT
%   resized_array : the new array of size [1, new_length]

function resized_array=reduce_array_length(array, new_length)
    resized_array = cell(1, new_length);
    for i=1:new_length
        resized_array{i} = array{i};
    end
end