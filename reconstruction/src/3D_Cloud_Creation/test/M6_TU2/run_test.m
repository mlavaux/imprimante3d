% Test the leave_one_out
function [ok err] = run_test(eps,verbose,run_all)

  ok = true;
  err = 0;

  if nargin < 3
    run_all = false;
  end
  if(nargin < 2)
    verbose = true;
  end
  if(nargin < 1)
    eps = 0.1;
  end
  if ~run_all
    addpath('../../../3D_Cloud_Creation/src');
  end

  % If only two image are available, no one ise rejected

   cameras = {1.0e+03*[-2.1000   -0.7035    0.6532   -4.5235
                        0    1.0333    1.9063   -4.8813
                        0   -0.0007    0.0007   -0.0047];

             1.0e+03*[-2.2144   -0.0194    0.6540   -4.5204
                        0.3200    0.9848    1.9051   -4.8770
                       -0.0002   -0.0007    0.0007   -0.0047];
  };


  x = 1.0e+03 * [ 0.8385 ; 0.7605 ];
  y = 1.0e+03 * [ 0.0465 ; 0.0945 ];

  if verbose
    disp('M6_TU2.1 : Checking the number of rejected point');
  end
  [p3d err nb stats] = leave_one_out(x,y,cameras,eps);

  nb_rejection = 2 - nb;
  if nb_rejection ~= 0
    nb_rejection
    disp('M6_TU2.1 : Error : No points should have been removed');
    ok = false;
  else
  
  [p3d1 err1] = compute_system(x,y,cameras);

  if norm(p3d1 - p3d)/norm(p3d1) > err1 + err
    disp('M6_TU2.1 : Error : Point should be the same as if directly computed');
  end

  if ok && verbose
    disp('M6_TU2.1 : OK');
  end
 
  if verbose
    disp('M6_TU2.2 : Checking that the precision is higher or equal');
  end
  cameras = {1.0e+03*[-2.1000   -0.7035    0.6532   -4.5235
                        0    1.0333    1.9063   -4.8813
                        0   -0.0007    0.0007   -0.0047];

             1.0e+03*[-2.2144   -0.0194    0.6540   -4.5204
                        0.3200    0.9848    1.9051   -4.8770
                       -0.0002   -0.0007    0.0007   -0.0047];

             1.0e+03*[-2.1121    0.6657    0.6538   -4.5214
                       0.6082    0.8371    1.9055   -4.8783
                      -0.0004   -0.0006    0.0007   -0.0047]
  };

  x_set  = 1.0e+03 *[0.8385    0.7605    0.6915];

  y_set = 1.0e+03 *[ 0.0465    0.0945    0.1665];

  [p3d err] = compute_system(x_set,y_set,cameras);
  [p3d1 err1] = leave_one_out(x_set,y_set,cameras,eps);

  cloud_expected = [ -0.257 ;  0.316 ; 2.280];

  if (norm(p3d - cloud_expected)/cloud_expected + err) < (norm(p3d1 - cloud_expected)/cloud_expected + err1)
    disp('M6_TU2.2 : Error precision is WORST');
    ok = false;
  elseif verbose
    disp('M6_TU2.2 : Precision is ok');
  end

  if ~ok
    disp('M6_TU2 : Error : Test failed');
  elseif verbose 
    disp('M6_TU2 : Test passed');
  end
  

end
