% Run every unitary test

addpath('../src/');

%------------------------------------------------
addpath('./M6_TU0');
disp('----- TEST M6_TU0 : LAUNCHING -----');
% eps,verbose,dispay,time,true
[ok err] = run_test(0.2,false,true);

if ok 
  disp('----- TEST M6_TU0 : PASSED -----');
else
  disp('----- TEST M6_TUO : FAILED -----');
end

rmpath('./M6_TU0');
%-------------------------------------------------
%------------------------------------------------
addpath('./M6_TU2');
disp('----- TEST M6_TU2 : LAUNCHING -----');
% eps,verbose,dispay,time,true
[ok err] = run_test(0.2,false,true);

if ok 
  disp('----- TEST M6_TU2 : PASSED -----');
else
  disp('----- TEST M6_TU2 : FAILED -----');
end

rmpath('./M6_TU2');
%-------------------------------------------------

