

function [ok err] = run_test(eps,verbose,run_all)

  ok = true;
  err = 0;

  if nargin < 3
    run_all = false;
  end
  if nargin < 2
    verbose = true;
  end
  if nargin < 1
    eps = 0.2;
  end
  if ~run_all
    addpath('../../../3D_Cloud_Creation/src');
  end
  
  step_by_step = false;

  if verbose
    disp('M6_TU0 : Starting the Test. Purpose : checking the correct execution of every branch of code,disregarding the quality of result');
  % INIT VALUES 
  end
  cameras = {1.0e+03*[-2.1000   -0.7035    0.6532   -4.5235
                        0    1.0333    1.9063   -4.8813
                        0   -0.0007    0.0007   -0.0047];

             1.0e+03*[-2.2144   -0.0194    0.6540   -4.5204
                        0.3200    0.9848    1.9051   -4.8770
                       -0.0002   -0.0007    0.0007   -0.0047];

             1.0e+03*[-2.1121    0.6657    0.6538   -4.5214
                       0.6082    0.8371    1.9055   -4.8783
                      -0.0004   -0.0006    0.0007   -0.0047]
  };

  masks = { ones(1080,1920);
            ones(1080,1920);
            ones(1080,1920)
  };

  matchings = 1.0e+03 *[0.8385    0.0465    0.7605    0.0945    0.6915    0.1665
                        1.3455    0.5745    1.3755    0.4930    1.3680    0.3720];

  % Execute script
  if(verbose)
    disp('M6_TU0 : Calculating the 3D Cloud');
  end
  
  [cloud_all errors stats time] = calc_3D_script(matchings,cameras,masks);

  if(verbose)
    disp('M6_TU0 : Done');
  end

  rejected_res = cloud_all.outside;
  rejected_expected = [];

  cloud_res = cloud_all.cloud;

  cloud_expected = [ -0.257   0.316  2.280 
                      0.563 -0.03926 1.820];


  % Rejection check
  
  if(size(rejected_expected) ~= size(rejected_res))
    ok = false;
    err = Inf;
  else
    if(rejected_expected ~= rejected_res)
      ok = false;
      err = Inf;
    end
  end

  if(verbose)
    sprintf('M6_TU0, rejection : %s\n% ',ok);
  end

  % Cloud check
  
  if(size(cloud_expected)==size(cloud_res))
    r_err = norm(cloud_expected - cloud_res)/norm(cloud_expected);
    err = r_err;
    if(r_err < eps)
      fprintf('M6_TU0, cloud ok with %f%',100*err);
      disp('% error rate');
    else
      fprintf('M6_TU0, cloud ko with %f% error rate\n',100*err);
      ok = false;
      disp('% error rate');
    end
  else
    err = Inf;
    ok = false;
    if(verbose)
      disp('M6_TU0 : incorrect cloud size');
    end
  end

  % Check execution disregarding resulting values
  if verbose
    disp('M6_TU0.1 : Reprojection out of image detection');
  end

  matchings = 1.0e+03 *[0.8385    0.0465    0.7605    0.0945    0.6915    0.1665
                        1.3455    0.5745    1.3755    0.4930    1.3680    0.3720
                        10        10        5        5        101        101];% This point will be outside the image

  [cloud_all errors stats time] = calc_3D_script(matchings,cameras,masks,'reprojection');
  cloud_res = cloud_all.cloud;
  rejected_res = cloud_all.outside;
  if(~size(rejected_res,1)>0)
    err = Inf;
    ok = false;
    if(verbose)
      disp('M6_TU0.1 Error : One wrong point should have be detected');
    end
  else
    if verbose
      disp('M6_TU0.1 OK : One point was rejected');
    end
  end

  % Check if some point are rejected if they do not belong to the mask

   matchings = 1.0e+03 *[0.8385    0.0465    0.7605    0.0945    0.6915    0.1665
                        1.3455    0.5745    1.3755    0.4930    1.3680    0.3720
              ];  

    masks = { zeros(1080,1920);
              zeros(1080,1920);
              zeros(1080,1920)
              };
   [cloud_all errors stats time] = calc_3D_script(matchings,cameras,masks,'reprojection');
    cloud_res = cloud_all.cloud;
    rejected_res = cloud_all. outside;
  if(~size(rejected_res,1)>0)
    err = Inf;
    ok = false;
    if(verbose)
      disp('M6_TU0.2 : Error : Points outside the mask incorrectly detected');
    end
    
  else
    if verbose
      disp('M6_TU0.2 : OK : All points were rejected');
    end
  end

  % END
 if verbose
   if ok 
     disp('M6_TU0 : Test Passed');
   else
     disp('M6_TU0 : Test Failed');
   end
 end

end
