% Validation with Blender House poI from pipeline

function [ok err] = run_test(eps,verbose,run_all)

  ok = true;
  err = 0;

  if nargin < 4
    run_all = false;
  end
  if(nargin < 3)
    step_by_step = false;
  end
  if(nargin < 2)
    verbose = true;
  end
  if(nargin < 1)
    eps = 0.2;
  end
  if ~run_all
    addpath('../../../3D_Cloud_Creation/src');
  end
  
  if verbose
    disp('Blender house');
  end
  load cameras.mat;
  load masks.mat;
  load matchings.xy;

  [cloud_all errors stats exec_time]  = calc_3D_script(matchings,cameras,masks,step_by_step);

  % DISPLAY 3D CLOUD
  param = [true true true true];
  if verbose
    display_stats(cloud_all,errors,stats,exec_time,param);
  end

end
