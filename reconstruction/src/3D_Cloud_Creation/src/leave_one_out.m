% FUNCTION leave_one_out [p3d err] = leave_one_out(index,x_set,y_set,cameras,eps)
% 
%



function [p3d err images stats] =  leave_one_out(x_set,y_set,cameras,eps)

  start = tic;
  n = length(cameras);
  
  p3d = [];
  err = Inf;

  errors = [];
  out_index = [];

  stats = [];
  n_ini = length(x_set);
  % Safeguard
  ite = 0;
  max_ite = length(cameras);
  
  % Run at least one time
  delta = eps + 1;
  remove = [];
  index = 1:length(x_set);

  while (delta > eps) && length(index)>=2 && ite < max_ite
    ite = ite + 1;
    index = setdiff(index,remove);

    % Compute every n system of n-1 equations
    for out = index

      interval = setdiff(index,out);
      [p3d err] = compute_system(x_set(interval),y_set(interval),cameras(interval));
      errors = [ errors ; err];

    end

    % find the system with the less error
    remove = find(errors==min(errors)); 
    delta = var(errors);
    
  end

  % Compute the resulting system
  [p3d err] = compute_system(x_set(index),y_set(index),cameras(index));

  % Stats
  stop = toc(start);
  images = length(index);
  stats = struct('time',stop,'inversion',ite+1);
 
end

