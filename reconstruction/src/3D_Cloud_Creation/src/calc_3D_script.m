% function [cloud rejected] = calc_3D_script(matchings,cameras,masks,...) 
%This script calculates every 3D-coordinates of every points represented
% by a set of matched point from severals images.

% INPUT
%    matchings: matched poI : n x m matrix 
%       where m = number of pictures
%             n = number of matchings
%    cameras  : camera cell containing m camera projection matrices
%    masks    : masks cell containing m matrix of the mask of the object

% OPTIONS :
%   'verbose'       : display infos
%   'algo'           : algo data check algorithm
%   'reprojection'  : reprojection on mask data checking
%   'step-by-step'  : step-by-step display of reprojection

%   time    : true/false : display the time of computation for every point
%

% OUTPUT
%  cloud_all : struct
%     cloud         : Matrix of 3D coordinates nb_point*3 2-dimension array
%     outside
%
%   1   x1 y1 z1
%   2   x2 y2 z2
% 
%   error     : struct  : cloud, vector
%                       : outside_image

%   stats     : struct  : cloud, vector
%                       : outside, vector
%                       : algo, vector


% SUBFUNCTION
%   compute_system : compute the over-constraint system
%   check_reprojection : project the 3D-point on every 2D-mask and calculate the distance
%                         between the projection and the mask

% Load point of interest

function [cloud_all errors stats execution] = calc_3D_script(matchings, cameras, masks, varargin)
  quit = false;
  
  % Mandatory parameters
  if nargin < 3
    disp('Fatal Error : mask cell is expected as a third argument');
    quit = true;
  end
  if nargin < 2
    disp('Fatal Error : camera cell is expected as a second argument');
    quit = true;
  end
  if nargin < 1
    disp('Fatal error : matchings are expected as a first argument');
    quit = true;
  end
  if length(cameras)~=length(masks)
    disp('Fatal error : the number of masks and cameras should be the same');
    quit = true;
  end

  % Optionals parameters
  nVarargin = length(varargin);

  % Default value
  reprojection = 'none';
  algorithm = 'none';
  verbose = false;
  step_by_step = false;
  reproj = false;

  for i = 1:nVarargin
    if strcmp(varargin{i},'step_by_step')
      step_by_step = true;
    elseif strcmp(varargin{i},'reprojection')
      reprojection = 'reprojection on mask';
      reproj = true;
    elseif strcmp(varargin{i},'loo')
      algorithm = 'algo';
    elseif strcmp(varargin{i},'ransac')
      algorithm = 'ransac';
    elseif strcmp(varargin{i},'verbose')
      verbose = true;
    end

  end

  if step_by_step && ~reproj
    disp('Step by step mode is only available with reprojection');
    disp('Press any key to proceed with reprojection');
    pause
    reprojection = ok;
  end


  if quit
    cloud_all = [];
    errors = [];
    stats = [];
    execution = [];
    return
  end

  % Proceed with the function
  if verbose
    disp('Running function with following parameters : ');
    fprintf('Error detection algorithm %s% \n',algorithm);
    disp(' ');
    fprintf('Reprojection check %s% \n',reprojection);
    disp(' ');
    if step_by_step
      disp('Step by step on reprojection');
    end
  end
  % format point of Interest coordinates
  x = matchings(1:end,1:2:end);
  y = matchings(1:end,2:2:end);

  m = length(cameras);

  % Store good point
  cloud = [];
  cloud_err = [];

  % Store bad point
  outside = [];
  outside_err = [];
  
  % Count how much is used to compute each point
  count_cloud = [];
  count_outside = [];
  usage = 0;
  main_time = tic;

  % inversion
  inversion = 0;

  for k=1:size(x,1)     % for each matching k
    x_set = x(k,:)';
    y_set = y(k,:)';

    if length(x_set) ~= length(cameras)
      n = min(length(x_set),length(cameras));
      x_set = x_set(1:n);
      cameras = cameras(1:n);
    end

    index = find(x_set~=-1);
    
    x_vect = x_set(index);
    y_vect = y_set(index);
    cams = cameras(index);
    
    % COMPUTE SYSTEM
    [point_k err] = compute_system(x_vect,y_vect,cams);
    inversion = inversion + 1;

    % Init params
    eps = 0.01;
    nb = length(index);
    keep = true;
    threshold = 0.05;
    keep_point = true;

    % DATA CHECKING
    if ~strcmp(algorithm,'none')
      if length(x_vect) > 2
        if strcmp(algorithm,'algo')
          [point_k err nb algo_stats] = leave_one_out(x_vect,y_vect,cams,threshold);
        else 
          [point_k err nb algo_stats] = ransac(x_vect,y_vect,cams,eps,threshold,100);
        end
        inversion = algo_stats.inversion + inversion; 
      end
    end % End algo ransac/loo

    % REPROJECTION
    if reproj
      if step_by_step
        fprintf('Point %d%',k);
        fprintf(', Error %f%.',err);
        disp(' ');
      end
      [keep_point] = check_reprojection(point_k,cameras,masks,step_by_step);
    end

    % Check if the point_k is to be added
    if keep_point && point_k(3) >= 0
      cloud = [ cloud ; point_k'];
      count_cloud = [count_cloud ; nb];
      cloud_err = [cloud_err ; err];      
    else
      outside = [outside ; point_k'];
      outside_err = [ outside_err ; err];
      count_outside = [ count_outside ; nb];
    end

      usage = usage + nb/length(index);

  end % End reprojection
   
  % TOC
  main_time_elapsed = toc(main_time);
 
  % Return assignation
  
  usage_rate = usage/(length(cloud) + length(outside));

  cloud_all = struct('cloud', cloud,'outside',outside);
  errors    = struct('cloud', cloud_err,'outside',outside_err);
  stats     = struct('cloud', count_cloud,'outside',count_outside,'usage',usage_rate);
  execution = struct('time',main_time_elapsed,'inversion',inversion);

end
