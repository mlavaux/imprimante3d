function pok = clearcloud(p, masks, cameras, epsilon)

	if nargin < 4
		epsilon = 1;
	end
	
	%size(p)=N*3
	nbimg=length(masks);
	pok=[];
	
	for i=1:size(p,1)
		isok=true;
		for j=1:nbimg
			p2d=cameras{j}*[p(i,:)';1];
			p2d=p2d(1:2)/p2d(3); %p2d=[x;y]
			x=round(p2d(1));
			y=round(p2d(2));
			xmin=x-epsilon;
			xmax=x+epsilon;
			ymin=y-epsilon;
			ymax=y+epsilon;
			
			[size_y,size_x ]=size(masks{j});
			if ~((xmin>size_x) || (xmax<1) || (ymin>size_y) || (ymax<1))
				xmin=max(1,xmin);
				ymin=max(1,ymin);
				xmax=min(xmax,size_x);
				ymax=min(ymax,size_y);
				
				if ~length(find(masks{j}(ymin:ymax,xmin:xmax)))
					isok=false;
					break;
				end
			else
				isok=false;
				break;
			end
		end
		if isok
			pok=[pok;p(i,:)];
		end
	end
		
