% 1 = ok
% 2 = outside the image
% 3 = outside the mask

function pok = check_point(point, mask, epsilon,display)

  
	if nargin < 3
		epsilon = 1;
	end

  if nargin < 4
    display = false;
  end

	[size_y,size_x]=size(mask);  
	pok = true;

  % Nearest Pixel
	x=round(point(1));
	y=round(point(2));


  % Boundary of research
	xmin=max(1,x-epsilon);
	xmax=min(x+epsilon,size_x);
	ymin=max(y-epsilon,1);
	ymax=min(y+epsilon,size_y);
			
  % If the pixel is in the image
	if (x<size_x)&&(x>1)&&(y<size_y)&&(y>1)
    % If the pixel is the mask (with epsilon tolerance)
    if ~length(find(mask(ymin:ymax,xmin:xmax)))
		  pok=false;
      if display
        disp('Pixel is outside the mask');
      end
		end
  else
    pok = false; 
    if display
      disp('Pixel is outside the image');
    end
  end

  if display && pok
    disp('Pixel is inside the mask');
  end
	
end
		
