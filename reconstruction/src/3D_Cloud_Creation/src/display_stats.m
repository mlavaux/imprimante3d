% This function display statistics for the module 3D_Cloud_Creation

  function ret = display_stats(cloud_all,errors,stats,execution,param)

  if nargin < 5
    disp('Error');
    ret = -1;
    return
  end

  display_cloud = param(1);
  display_histo = param(2);
  display_graph = param(3);
  display_text =  param(4);


  index_figure = 1;

  % Points
  cloud = cloud_all.cloud;
  ko_cloud = cloud_all.outside;

  %errors
  cloud_error = errors.cloud;
  ko_error = errors.outside;
  
  % count
  count = stats.cloud;
  ko_count = stats.outside;
  usage_rate = stats.usage;
  % Display 3D points 
  if display_cloud
   index_figure = index_figure + 1;
   figure(index_figure);
   set(gcf,'position',[0,0,1280,800]);
   subplot(1,2,1)
   hold on
   axis equal
   title('Points Cloud','fontsize',14)

  % Cloud
  if(size(cloud,1)>0)
    plot3(cloud(:,1),cloud(:,2),cloud(:,3),'g.')
  end
  % Rejected
  if size(ko_cloud,1)>0
    plot3(ko_cloud(:,1),ko_cloud(:,2),ko_cloud(:,3),'r.')
  end
  % Rejected algo
   view(3);
   axis vis3d;
  end

  if display_histo
    if length(count) > 0
      index_figure = index_figure + 1;
      figure(index_figure)
      hist(count,2:max(count));
      title('Histogram : Image used per correct point');
    end
    if length(ko_count) > 0
      index_figure = index_figure + 1;
      figure(index_figure)
      hist(ko_count,2:max(ko_count));
      title('Histogram : Image used per incorrect point');
    end
   end

  if display_graph
    % Compute stats
   
    p3dko = [];
    p3dok = [];

    % disp
    % ERRORS 
    % Accepted point
    if size(cloud_error) > 0
      index_figure = index_figure + 1;
      figure(index_figure);
      hold on;
      title('Correct points');
      xlabel('Number of image');
      ylabel('Error');
      p1all = plot(count,cloud_error,'+');
      set(p1all,'Color','red');
      hold off;
    end

    % Rejected point
    if size(ko_error) > 0
      index_figure = index_figure + 1;
      figure(index_figure);
      hold on;
      title('Inorrect points');
      xlabel('Number of image');
      ylabel('Error');
      p2all = plot(ko_count,ko_error,'+');
      set(p2all,'Color','red');
      hold off;
    end
     
  end

  if display_text
  % COMPUTE STATS
    
   rejected_rate = size(ko_cloud,1)/(size(cloud,1)+size(ko_cloud,1));
   fprintf('Rate of Incorrect point : %f%%',100*rejected_rate);
   disp(' ');
   fprintf('Calcul cloud creation, elapsed time : %f%',execution.time);
   disp(' '); 
   fprintf('Calcul cloud System inversion : %d%',execution.inversion);
   disp(' ');
   fprintf('Average image usage per matching : %f%%',100*usage_rate);
   disp(' ');
  end

end
