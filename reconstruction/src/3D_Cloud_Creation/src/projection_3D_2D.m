% This function calculate the projection of a 3D point on a 2D image.

% INPUT :
%   One-dimension 3-coordonate vector
%   Cameras matrix

% OUTPUT :
%   One-dimension 2-coordonate vector

function y = projection_3D_2D(point,camera)

  point_tilde = [ point ; 1];
  y_tilde = camera*point_tilde;
  y = y_tilde(1:2)/y_tilde(3);

end

