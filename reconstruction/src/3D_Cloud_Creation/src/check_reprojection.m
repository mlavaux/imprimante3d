% This function checks whether the 3D-point is correctly reprojected 
% on every mask of the object

% INPUT
%   3-coordinante 1-dimension vector : the point
%   Index : one-dimension array for indexation of the cells
%   Cell containing cameras matrix
%   Cell containing masks
%   k : indice of the point
% OUTPUT 
%   true : if correct
%   false: if not correct

function ret = check_reprojection(point,cameras,masks,display)

  if nargin < 4
    display = false;
  end
  belong_to_the_mask = true; 
  for i=1:length(cameras)

    mask = masks{i};
    camera = cameras{i};

    % Reprojection on one image
    point_2D = projection_3D_2D(point,camera);
    tolerance = 1 ;% pixel
    res =  check_point(point_2D,mask,tolerance,display);

    if ~res 
      belong_to_the_mask = false;
    end

    % Display reprojection
    if display
      figure(i) 
      pixel = fix(point_2D);
      pixel = [pixel(2) ; pixel(1)] ; %Conversion to image format
      image = repmat(255*mask,[1 1 3]);
     
      if belong_to_the_mask
        % Draw the point in green
        image(pixel(2)-4:pixel(2)+4,pixel(1)-4:pixel(1)+4,1) = 0; 
        image(pixel(2)-4:pixel(2)+4,pixel(1)-4:pixel(1)+4,2) = 255;
        image(pixel(2)-4:pixel(2)+4,pixel(1)-4:pixel(1)+4,3) = 0; 
        % Draw a target in green
        image(pixel(2),:,:) = 0;
        image(pixel(2),:,2) = 255;
        image(:,pixel(1),:) = 0;
        image(:,pixel(1),2) = 255;
 
      else
        % Draw the point in red
        image(pixel(2)-4:pixel(2)+4,pixel(1)-4:pixel(1)+4,1) = 255; 
        image(pixel(2)-4:pixel(2)+4,pixel(1)-4:pixel(1)+4,2) = 0;
        image(pixel(2)-4:pixel(2)+4,pixel(1)-4:pixel(1)+4,3) = 0;
        % Draw the target in red
        image(pixel(2),:,:) = 0;
        image(pixel(2),:,1) = 255;
        image(:,pixel(1),:) = 0;
        image(:,pixel(1),1) = 255;

      end
      imshow(image)
    end

    % LEAVE ONCE ONE REPROJECTION FAILS
    % In display mode, comment to display every
    % other reprojection of the point
    if ~belong_to_the_mask
      break;
    end

  end

  % Display reprojection on every masks
  % Step-by-step
  if display
    pause
  end
   ret = res;
end
