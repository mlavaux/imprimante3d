% INPUT :
% Index : Indexation for accessing x,y and cameras
% x, y : coordonates for each images

% OUTPUT
% X : Pseudo inverse solution of the over constrained system
% ERR : error associated with the system
% X_couple : vector containing the solution of every partial system


function [X ERR] = compute_system(x,y,cameras)

  % INIT
  n = length(cameras)                                                                 ;
  A = []                                                                              ;
  B = []                                                                              ;
 
  % PSEUDO-INV
  for i = 1:n
    % Declaration
    DLT = cameras{i}                                                                ;
    m = [x(i);y(i)]                                                               ;

    % Construction of the System A_L*X_L = B_L
    % ----------------------------------------
    A_t = [ DLT(1,1)-m(1)*DLT(3,1) , DLT(1,2)-m(1)*DLT(3,2) , DLT(1,3)-m(1)*DLT(3,3) 
            DLT(2,1)-m(2)*DLT(3,1) , DLT(2,2)-m(2)*DLT(3,2) , DLT(2,3)-m(2)*DLT(3,3) ];
    A = [ A ; A_t]                                                                    ;
    B_t = [ -DLT(1,4) + m(1)*DLT(3,4)
            -DLT(2,4) + m(2)*DLT(3,4)]                                                ;
    B = [B ; B_t]                                                                     ;
  end

  % Resolution of the over-constraint system
  X = A\B                                                                           ;                       
  ERR = norm(A*X - B)/norm(B)                                                              ;

end 
