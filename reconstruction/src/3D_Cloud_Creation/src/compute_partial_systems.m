% INPUT :
% Index : Indexation for accessing x,y and cameras
% x, y : coordonates for each images
% shift : integer >= 1 for matching

% OUTPUT

% X : vector containing the solution of every partial system


function X = compute_system(index,x,y,cameras,shift)

  % INIT
  n = length(index);
  X = [];

  
  for i = 1:n
    % Declaration
    current = index(i);

    % The last image is matched with the first
    next =  mod(current + shift,n) + 1;

    DLT_g = cameras{current};
    DLT_d = cameras{next}   ;
    m_g = [x(current);  y(current)];
    m_d = [x(next)   ;  y(next)   ];

    % Construction of the System A*X_ = B
    % ----------------------------------------
    A = [ DLT_g(1,1)-m_g(1)*DLT_g(3,1) , DLT_g(1,2)-m_g(1)*DLT_g(3,2) , DLT_g(1,3)-m_g(1)*DLT_g(3,3) 
          DLT_g(2,1)-m_g(2)*DLT_g(3,1) , DLT_g(2,2)-m_g(2)*DLT_g(3,2) , DLT_g(2,3)-m_g(2)*DLT_g(3,3) 
          DLT_d(1,1)-m_d(1)*DLT_d(3,1) , DLT_d(1,2)-m_d(1)*DLT_d(3,2) , DLT_d(1,3)-m_d(1)*DLT_d(3,3) 
          DLT_d(2,1)-m_d(2)*DLT_d(3,1) , DLT_d(2,2)-m_d(2)*DLT_d(3,2) , DLT_d(2,3)-m_d(2)*DLT_d(3,3) ];

 
    B = [ -DLT_g(1,4) + m_g(1)*DLT_g(3,4)
          -DLT_g(2,4) + m_g(2)*DLT_g(3,4)
          -DLT_d(1,4) + m_d(1)*DLT_d(3,4)
          -DLT_d(2,4) + m_d(2)*DLT_d(3,4) ];
  
    % Resolution of the system
    X_ = A\B;

    % Storing the solution in the main vector
    X = [ X ; X_']; 
 
  end

end 
