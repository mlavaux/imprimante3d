% profile report;profile off;
%% plot the points cloud
function [] = print(cloud)
p = cloud;
figure(1);
set(gcf,'position',[0,0,1280,800]);
subplot(1,2,1)
hold on
axis equal
title('Points Cloud','fontsize',14)
plot3(p(:,1),p(:,2),p(:,3),'g.')
view(3);
axis vis3d
end
