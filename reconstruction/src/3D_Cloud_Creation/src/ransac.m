% FUNCTION leave_one_out [p3d err] = ransac(index,x_set,y_set,cameras,eps)
% 
%

function [p3d err nb stats] = ransac(x_set,y_set,cameras,threshold,eps,ite_max,warningoff)

  % Minimum number of point to adjuste system ( 2 <= d <= length(camera) )
  n = min(2,length(cameras));

  % Minimum needed
  d = 2;

  if nargin < 8
    warningoff = true;
  end
  if nargin < 7
    % Maximum of iteration
    ite_max = 100;
  end
  if nargin < 6
    eps = 0.01;
  end
  % Threshold
  if nargin < 5
    threshold = 0.01;
  end
  % Init 
  inversion = 0;
  started = tic;

  %
  best_err = Inf;
  best_p3d = [];
  ite = 1;

  % Mode 
  % With consecutive, we will build the model by taking a random point and a second point from a consecutive picture
  % otherwise, we will use 2 random pictures
  mode = 'consecutive'; % 'random'

  if strcmp(mode,'consecutive')
    condition = length(x_set);
  else
    condition = min(ite_max,size(perms(1:min(6,length(x_set))),1));
  end

  % Random images
  while ite < condition
    ite = ite + 1;
    % Choose n random 2-points
    perm_ind = randperm(length(x_set));
    first_value = perm_ind(1);

    if perm_ind(1) + 1 > length(cameras)
      second_value = max(1,first_value -  1);
    else
      second_value = first_value + 1;
    end

    if strcmp(mode,'consecutive')
      working_set = [first_value ; second_value] ;
    else
      working_set = [perm_ind(1) ; perm_ind(2)];
    end
    % Compute the model
    [p3d err] = compute_system(x_set(working_set),y_set(working_set),cameras(working_set));
    inversion = inversion + 1;
 
%    setdiff(perm_ind,working_set)
    for k = setdiff(perm_ind,working_set);
      test_set = [working_set ; k];
      [p3d_ad err_ad] = compute_system(x_set(test_set),y_set(test_set),cameras(test_set));
      inversion = inversion + 1;
      if norm(err_ad - err) < threshold
        working_set = [working_set ; k];
      end
    end
%    working_set
%    pause
    if length(working_set) >= d
      [p3d_candidate err_candidate] = compute_system(x_set(working_set),y_set(working_set),cameras(working_set));
      inversion = inversion + 1;
      if err_candidate < best_err
        best_err = err_candidate;
        best_p3d = p3d_candidate;
      end
    end

  end

  if ite == ite_max && ~warningoff
    disp('Warning : Max number of iteration reached in Ransac');
  end

  elapsed_time = toc(started);
  p3d = best_p3d;
  err = best_err;
  nb = length(working_set);
  stats = struct('time',elapsed_time,'inversion',inversion);
end
