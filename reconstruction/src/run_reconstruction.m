clear; 
close all hidden;

% Enter the folder of images and their format
folder = '../media/bunny/version1';

% example : '/*.png'
format_img = '/*.png';

% Number of pictures used
% -1 for all pictures
% 4 is the minimum for calibration
% 2 is the minimum for matching and cloud creation
max_pictures = -1;

% Display additional infos
% Module i - > display_vect(i) = true
display_vect = [ false false false false true true];

% Loading precomputed value stored in the specified folder
%   'load' + ['cameras' ; 'masks' ; 'pois' ; 'descs' ; 'matchings'] 
%   'cameras'     cameras.mat
%   'masks'       masks.mat
%   'pois'         pts.mat
%	  'descs'		  descs.mat
%   'matchings'   matching.xy

load_data = {'cameras';'masks';'pois'};

% Save values in the folder
% 'save' + [ 'cameras' ; 'masks' ; 'pois'; 'matchings' ; 'cloud' ; 'mesh'];

save_data = {'matchings';'pois';'cloud'};

% Calibration

% Calculation of the points of interest

% Matching of the points of interest
% 'SURF'/'ZNCC'/'RMSE
signature = 'ZNCC';

% ExtendMatching method : 'extendMatching1'/'extendMatching2'
extendMatching = 'extendMatching1';
% Removal of the 25% worst matchings found : 'remove_last_quartile'/'none'
remove_last_quartile = 'remove_last_quartile';

% Creation of the cloud
% 'loo'/'ransac' algorithm used remove aberrants points 
% 'reprojection' check 3D-point by projection on a mask

reprojection = 'reprojection';
algo = 'ransac';

% Creation of the mesh
% 'crust'/'convexHull'/'delaunayFaceRemoval'
algoMesh = 'delaunayFaceRemoval';

ok = reconstruction(folder,max_pictures,format_img,display_vect,...
               'verbose', ...
               signature, ...
               extendMatching, ...
               remove_last_quartile, ...
               reprojection,...
               algo,				...
               algoMesh, ...
              'verbose',		...
               'load',load_data,...
               'save',save_data);

if ~ok
  error('reconstruction.m did not executed correctly');
end
