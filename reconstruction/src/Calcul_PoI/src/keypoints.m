  % Extract keypoints from a sequence of images located in 'folder'
  %
  %
  % INPUT
  %   - images_origine : cell containing the pictures.
  %
  % OUTPUT
  %   -pois    : Points of interest with improved harrys method
  %              Array {poi_im_1, ...}
  %              poi_im_n = [x1 x2 ... xn; y1 y2 ... yn]

function [pois, signatures] = keypoints(images_origine, calcul_descs)

  if nargin < 2
  	type_signature = 0;
  end
  
  % Initializing signature struct
  signatures = {};
  
  % Initializing PoI struct
  pois = cell(1, length(images_origine));

  % Computing PoI for image sequence
  for i = 1:numel(pois)
    % Load image
    imcell = rgb2hsv(images_origine{i});
    pois{i} = kp_harris(imcell(:, :, 3));
    
  end
  
  if calcul_descs
  	window_half_size = 3;	% default value defining the neighborhood
	for i=1:length(images_origine)
		[y_max, x_max]=size(images_origine{i});
		sig=[];
		for k=1:size(pois{i},2)
			c = pois{i}(:,k);	% c = [x ; y]
			try	% The window is entierly inside the picture
				w = double(images_origine{i}(c(2)-window_half_size:c(2)+window_half_size,c(1)-window_half_size:c(1)+window_half_size,:));
			catch % The window is not entierly inside the picture
				w = -ones(1+2*window_half_size, 1+2*window_half_size, 3);
			end
			sig = [sig, w(:)];
		end
		signatures{i}=sig;
	end	
  end 
end
