% Test script for keypoint function

folder = '../../../media/tigrou_final';
format = '/*.JPG';

D = dir(strcat(folder, format));
pics = cell(numel(D));
for i = 1:numel(D)
  pics{i} = imread(strcat(strcat(folder, '/'), D(i).name));
end
pts = keypoints(pics);
