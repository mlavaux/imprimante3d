% Load images
D = dir(strcat(folder, format));
imcell = cell(1, numel(D));
for i = 1:numel(D)
  imcell{i} = imread(strcat(strcat(folder, '/'), D(i).name));
end

for i = 1:size(pts, 2)
  figure();
  imshow(imcell{i});
  for j = 1:size(pts{i}, 2)
    hold on;
    plot(pts{i}(1, j), pts{i}(2, j), 'r*'); 
  end
  hold off;
end