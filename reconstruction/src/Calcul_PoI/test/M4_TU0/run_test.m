function [err ok] = run_test(name)
  im = imread(name);
  pts = kp_harris(im);

  if pts
    ok = true;
    err = -1;
  else
    ok = false;
    err = Inf;
  end
