function [err ok] = run_test(name1, name2)
  ok = false;
  err = Inf;

  im1 = imread(name1);
  im2 = imread(name2);
  pts1 = kp_harris(im1);
  pts2 = kp_harris(im2);

  figure;
  imshow(im1)
  hold on;
  for i = 1:size(pts1, 2)
    plot(pts1(1, i), pts1(2, i), 'r+');
  end
  hold off;

  figure;
  imshow(im2)
  hold on;
  for i = 1:size(pts2, 2)
    plot(pts2(1, i), pts2(2, i), 'g+');
  end
  hold off;

