% Run every unitary test

addpath('../src/');

%------------------------------------------------
addpath('./M4_TU0');
disp('----- TEST M4_TU0 : LAUNCHING -----');
% eps,verbose,dispay,time,true
[ok err] = run_test('lc.jpg');

if ok 
  disp('----- TEST M4_TU0 : PASSED -----');
else
  disp('----- TEST M4_TUO : FAILED -----');
end

rmpath('./M4_TU0');
%-------------------------------------------------
addpath('./M4_TU1');
disp('----- TEST M4_TU1 : LAUNCHING -----');
% eps,verbose,dispay,time,true
[ok err] = run_test('0003.png', '0004.png');

if ok 
  disp('----- TEST M4_TU1 : PASSED -----');
else
  disp('----- TEST M4_TU1 : FAILED -----');
end

rmpath('./M4_TU1');
%-------------------------------------------------
