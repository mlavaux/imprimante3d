function [P,K,M] = calibrage(im, num_plan, disparam)
%im contient l ensemble des images
%renvoie P une matrice 3x4xn contenant les matrice de projection de chaque
%image, K la matrice de calibrage 3x3 commune a toutes les images et M la
%matrice contenant les matrices associees a toutes les ellipses,
%projections des cercles composant les mires dans les differentes images

%nb_vues nombres d images
nb_vues = size(im,4);

%declaration de M qui contient les ellipses des mires
M = zeros(3,3,6,4,nb_vues);

w = warning ('off','all');
%detection des mires
for vue = 1:nb_vues
    if disparam > 0 
        figure(vue);
        imshow(im(:,:,:,vue));
        hold on;
    end
    img = im(:,:,:,vue);
    cont = edge(rgb2gray(img),'canny',0.2);%cont(240:260,540:600)=0;%pour que le jeu de donnees des cubes fonctionne
    h    = bwareaopen(cont,50);
    k    = bwlabel(h);
    n    = 0;
    if disparam > 0
      hold on;axis ij;
    end
    for ii = 1:max(max(k))
        [i,j] = find(k==ii);
        %cherche l'ellipse correspondant au mieux aux points (i,j)
        E = fit_ellipse(j, i, [], disparam);
        %ne garde que les ellipses pouvant �tre les images des mires
        if size(E.a)~=0 & E.a/E.b>0.25 & E.a/E.b<4 & erreur(i,j,E)
            n = n+1;
            %affichage des ellipses
            if disparam > 0 
                p_a         = ellipsepoints( [E.X0_in E.Y0_in E.a E.b -E.phi],100 );
                h1          = plot(p_a(1,:),p_a(2,:),'b-');
            end
            params(n,:) = [E.X0_in E.Y0_in E.a E.b -E.phi];
        end;
    end;%pause;
    params = params(1:n,:);
    A = params(:,1);
    A = repmat(A,1,size(params,1));
    B = params(:,2);
    B = repmat(B,1,size(params,1));
    ind_rest = 1:size(params,1);
    compt = 1;
    while size(A,2)~=0 & n~=0
        %cherche les ellipses dont le centre est suffisamment proche
        l = find(abs(A(1,:)-A(:,1)')<10 & abs(B(1,:)-B(:,1)')<10);
        if size(l,2) == 6
            %trie les ellipses de la plus grande � la plus petite
            for a = 1:size(l,2)-1
                for b = a+1:size(l,2)
                    if params(ind_rest(l(a)),3) < params(ind_rest(l(b)),3)
                        temp = l(a); l(a) = l(b); l(b) = temp;
                    end;
                end;
            end;
            %affichage de la mire
            for i = 1:size(l,2)
                M(:,:,i,compt,vue) = param2ellipse(params(ind_rest(l(i)),:));
            end;
            compt = compt + 1;
        end;
        A(:,l) = []; A(l,:) = [];
        B(:,l) = []; B(l,:) = [];
        ind_rest(l) = [];
    end;
    if disparam > 0 
        for j = 1:compt-1
            for k = 1:6
                p_a  = ellipsepoints( ellipse2param(M(:,:,k,j,vue)),100 );
                h1   = plot(p_a(1,:),p_a(2,:),'y-');
            end;
        end;
    end
end;
if disparam > 0
  compt
end
w = warning ('on','all');
%si plus de 2 mires, s assure que les mires sont bien rangees dans le meme ordre sur toutes les
%vues
%if compt > 2
%    for i = 1:compt-2
%        for vue = 1:nb_vues
%            figure(vue);
%            a = ginput(1);
%            plot(a(1),a(2),'rx');
%            dist_max = 1000000;
%            ind_max = i;
%            for j = i:compt-1
%                params = ellipse2param(M(:,:,1,j,vue));
%                dist = (params(1)-a(1))^2+(params(2)-a(2))^2;
%                if dist < dist_max
%                    dist_max = dist;
%                    ind_max = j;
%                end;
%            end;
%            temp           = M(:,:,:,i,vue);
%            M(:,:,:,i,vue) = M(:,:,:,ind_max,vue);
%            M(:,:,:,ind_max,vue) = temp;
%            %params1 = ellipse2param(M(:,:,1,1,vue));
%            %params2 = ellipse2param(M(:,:,1,2,vue));
%            %if (params1(1)-a(1))^2+(params1(2)-a(2))^2 > (params2(1)-a(1))^2+(params2(2)-a(2))^2
%            %    temp           = M(:,:,:,1,vue);
%            %    M(:,:,:,1,vue) = M(:,:,:,2,vue);
%            %    M(:,:,:,2,vue) = temp;
%            %end;
%        end;
%    end;
%end;

eps = 0.08;
N = zeros(4,nb_vues);
for vue = 1:nb_vues
    %figure(vue);
    imhsv = rgb2hsv(im(:,:,:,vue));
    blue_pattern_found = false;
    red_pattern_found = false;
    for i = 1:4
        ind = find(N(:,vue)==0);
        ind = ind(1);
        param = ellipse2param(M(:,:,1,ind,vue));
        L1 = [];
        %plot(param(1),param(2),'rx');
        %L2 = [];
        for k = max(1,int32(param(1)-max(param(3),param(4)))):min(size(im,2),int32(param(1)+max(param(3),param(4))))
            for l = max(1,int32(param(2)-max(param(3),param(4)))):min(size(im,1),int32(param(2)+max(param(3),param(4))))
                pp = double([k;l;1]);
                if pp'*M(:,:,1,ind,vue)*pp < 0
                    if pp'*M(:,:,2,ind,vue)*pp > 0
                        L1 = [L1;imhsv(l,k,1)];
                    %else if pp'*M(:,:,3,i,vue)*pp < 0
                    %        L2 = [L2;imhsv(k,l,1)];
                    %    end;
                    end;
                end;
            end;
        end;
        %figure(vue);
        %plot(param(1),param(2),'rx');
        if size(L1,1) > 0
            L1(find(L1>0.9)) = L1(find(L1>0.9))-1;
            %median(L1)
            %pause;
            %rouge
            if abs(median(L1)-0) < eps || abs(median(L1)-1) < eps
                red_pattern_found = true;
                temp = M(:,:,:,2,vue);
                M(:,:,:,2,vue) = M(:,:,:,ind,vue);
                M(:,:,:,ind,vue) = temp;
                N(2,vue) = 1;
            %vert
            else if abs(median(L1)-0.3833) < eps
                    temp = M(:,:,:,3,vue);
                    M(:,:,:,3,vue) = M(:,:,:,ind,vue);
                    M(:,:,:,ind,vue) = temp;
                    N(3,vue) = 1;
                %bleu
                else if abs(median(L1)-0.6666667) < eps
                        blue_pattern_found = true;
                        temp = M(:,:,:,1,vue);
                        M(:,:,:,1,vue) = M(:,:,:,ind,vue);
                        M(:,:,:,ind,vue) = temp;
                        N(1,vue) = 1;
                    %jaune
                    else if abs(median(L1)-0.1666667) < eps
                            temp = M(:,:,:,4,vue);
                            M(:,:,:,4,vue) = M(:,:,:,ind,vue);
                            M(:,:,:,ind,vue) = temp;
                            N(4,vue) = 1;
                        end;
                    end;
                end;
            end;
        else if size(find(N(:,vue)==0),1)>1
                ind2 = find(N(:,vue)==0);
                ind2 = ind2(2);
                temp = M(:,:,:,ind2,vue);
                M(:,:,:,ind2,vue) = M(:,:,:,ind,vue);
                M(:,:,:,ind,vue) = temp;
            end;
        end;
    end;
end;

if ~blue_pattern_found || ~red_pattern_found
  P = -1;
  K = -1;
  M = -1;
  return
end

if disparam > 0
  N
end

%for vue = 1:4
%    figure(vue);
%    for j = 1:4
%        for k = 1:6
%            p_a  = ellipsepoints( ellipse2param(M(:,:,k,j,vue)),100 );
%            h1   = plot(p_a(1,:),p_a(2,:),'c-');
%        end;pause;
%    end;
%end;

[K,PoseMatrix,ProjectionMatrix] = calibr(M(:,:,:,:,:),size(im,1), size(im,2), num_plan, N, disparam);
P = ProjectionMatrix;
%P(:,1:2,:)=P(:,[2 1],:);
%P(:,1,:)=-P(:,1,:);

if disparam > 0
    for vue = 1:nb_vues
        figure(vue);
        temp1 = P(:,4,vue)/P(3,4,vue);
        temp2 = (P(:,4,vue)+P(:,3,vue))/(P(3,4,vue)+P(3,3,vue));
        plot([temp1(1) temp2(1)], [temp1(2) temp2(2)],'r');
        temp2 = (P(:,4,vue)+P(:,2,vue))/(P(3,4,vue)+P(3,2,vue));
        plot([temp1(1) temp2(1)], [temp1(2) temp2(2)],'g');
        temp2 = (P(:,4,vue)+P(:,1,vue))/(P(3,4,vue)+P(3,1,vue));
        plot([temp1(1) temp2(1)], [temp1(2) temp2(2)],'b');
    end;
end

%calibrage
function [K,PoseMatrix,ProjectionMatrix] = calibr(A,nl,nc, num_plan, N, disparam)

%nb_vues nombres d images
nb_vues = size(A,5);
if disparam > 0
  nb_vues 
end

compt = 1;
for vue = 1:nb_vues
    H(:,:,vue) = process_ellipses(A(:,:,:,1:2,vue),nl,nc,disparam);
    if (N(3,vue) == 1)
        H(:,:,vue+compt) = process_ellipses(A(:,:,:,3,vue),nl,nc,disparam);
        if disparam > 0
          A(:,:,:,3,vue)
          H(:,:,vue+compt)
        end
        compt = compt + 1;
    end;
    if (N(4,vue) == 1)
        H(:,:,vue+compt) = process_ellipses(A(:,:,:,4,vue),nl,nc,disparam);
        if disparam > 0
          A(:,:,:,4,vue)
          H(:,:,vue+compt)
        end
        compt = compt + 1;
    end;
end;

for vue = 1:nb_vues
    for q=1:6
        params_a((vue-1)*6+q,:) = ellipse2param( transpose(H(:,:,vue))*A(:,:,q,1,vue)*H(:,:,vue) );
        params_b((vue-1)*6+q,:) = ellipse2param( transpose(H(:,:,vue))*A(:,:,q,2,vue)*H(:,:,vue) );
    end
end
if disparam > 0
  mean(params_a(:,3)./params_a(:,4))
  mean(params_b(:,3)./params_b(:,4))
end

if disparam > 0
  figure;
  aa = double(int8(sqrt(nb_vues)))+1;
  for vue = 1:nb_vues
      subplot(aa,aa,vue);
      hold on;
      for i = 1:6
          for j = 1:num_plan
              pp = ellipsepoints(ellipse2param((H(:,:,vue))'*A(:,:,i,j,vue)*(H(:,:,vue))),100);
              plot(pp(1,:),pp(2,:),'r');
          end;
      end;
      axis equal;
  end;
  display('Press <enter> to continue...');
  pause;
end;

if disparam > 0
    figure;
end

IAC = plane_based_calibr( H, disparam );
K   = recover_calibr_matrix( IAC );
if disparam > 0
  K
end
%K = 1000*[1.4945         0    1.0274
%         0   -1.5471    0.5222
%         0         0    0.0010];

for p = 1:nb_vues
% Pose computation
    % T : rotation (wrt the camera frame)
    % T*w : translation (wrt the camera frame)
    H(:,:,p) = H(:,:,p)/norm(H(:,:,p),'fro');
    M        = inv(K)*H(:,:,p);
    M_bar    = M(:,1:2);
    [U,S,V]  = svds(M_bar,2);
    T_bar    = U*transpose(V);
    t3       = cross(T_bar(:,1),T_bar(:,2));
    T        = [ T_bar, t3 ];
    lambda   = trace(transpose(T_bar)*M_bar) / trace(transpose(M_bar)*M_bar);
    w        = transpose(T)*M*[0;0;lambda];
    % Projection  matrices
    Q(:,:,p) = T*[eye(3),w];
    P(:,:,p) = K*T*[eye(3),w];
    G(:,:,p) = P(:,[1,2,4],p);
end


for p = 1:nb_vues

    params_a = ellipse2param( transpose(G(:,:,p))*A(:,:,2,1,p)*G(:,:,p) );
    params_b = ellipse2param( transpose(G(:,:,p))*A(:,:,2,2,p)*G(:,:,p) );

    x1 = params_a(1); 
    y1 = params_a(2); 
    z = mean(params_a(3:4));
    x2 = params_b(1); 
    y2 = params_b(2); 
    n  = [x2;y2]-[x1;y1];
    d = norm(n);
    %n  = n/norm(n);
    %a  = -(x1-x2)/(-2*y2*y1+y2^2+y1^2-2*x2*x1+x2^2+x1^2);
    %b  = (y1-y2)/(-2*y2*y1+y2^2+y1^2-2*x2*x1+x2^2+x1^2);
    %c1 = (x1^2-x2*x1+y1^2-y2*y1)/(-2*y2*y1+y2^2+y1^2-2*x2*x1+x2^2+x1^2);
    %c2 = (-x2*y1+y2*x1)/(-2*y2*y1+y2^2+y1^2-2*x2*x1+x2^2+x1^2);   
    a=n(1)*z/d;
    b=n(2)*z/d;
    Corr = [a,-b,x1;b,a,y1;0,0,1]; % *[x1,x2;y1,y2;1,1]
    G(:,:,p) = G(:,:,p) * Corr;
end

for p = 1:nb_vues
% Pose computation
    % T   : rotation (wrt the camera frame)
    % T*w : translation (wrt the camera frame)
    M        = inv(K)*G(:,:,p);
    M_bar    = M(:,1:2);
    [U,S,V]  = svds(M_bar,2);
    T_bar    = U*transpose(V);
    t3       = cross(T_bar(:,1),T_bar(:,2));
    T        = [ T_bar, t3 ];
    lambda   = trace(transpose(T_bar)*M_bar) / trace(transpose(M_bar)*M_bar);
    w        = transpose(T)*M*[0;0;lambda];
    % Projection  matrices
    Q(:,:,p) = T*[eye(3),w];
    P(:,:,p) = K*T*[eye(3),w];
    G(:,:,p) = P(:,[1,2,4],p);
end

if disparam > 0
    axis equal;
    aa = double(int8(sqrt(nb_vues)))+1;
    for p = 1:nb_vues
      subplot(aa,aa,p);
      hold on; axis equal; grid on;
        for q = 1:6
             params_a = ellipse2param( transpose(G(:,:,p))*A(:,:,q,1,p)*G(:,:,p) );
             params_b = ellipse2param( transpose(G(:,:,p))*A(:,:,q,2,p)*G(:,:,p) );
             p_a   = ellipsepoints( params_a,100 );
             p_b   = ellipsepoints( params_b,100 );
             h1 = plot(p_a(1,:),p_a(2,:),'b-');
             h2 = plot(p_b(1,:),p_b(2,:),'r-');
        end
    end
end

for p = 1:nb_vues
% ----------------
     PoseMatrix(:,:,p) = Q(:,:,p);
     ProjectionMatrix(:,:,p) = P(:,:,p);
     G_new(:,:,p) = ProjectionMatrix(:,[1,2,4],p);
% ----------------
end 

% ----------------------------------------------------------------------------
function H = process_ellipses(A0,nl,nc,disparam);
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Status
IPC = [];

f = 4*max(nc,nl);
u0 = nc/2;
v0 = nl/2;

T = [f,0,u0;0,f,v0;0,0,1];
TInv = inv(T);

for noell=1:2
   for nopair=1:size(A0,4)
       A(:,:,noell,nopair) = normalize_matr_det_1( transpose(T)*A0(:,:,(noell-1)*3+2,nopair)*T );
       %A(:,:,noell,nopair+2) = normalize_matr_det_1( transpose(T)*A0(:,:,(noell-1)*3+1,nopair)*T );
   end
end

M   = [ ];
for i = 1:size(A0,4)
    [V,D]     = eig( A(:,:,1,i), A(:,:,2,i) );
    [m,k]     = max(diag(D));
    linf(:,i) = A(:,:,1,i)*V(:,k);
    [L,S,R]    = svd( A(:,:,1,i) - 1/(transpose(linf(:,i))*inv(A(:,:,1,i))*linf(:,i))*linf(:,i)*transpose(linf(:,i)) );
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%    [L,S,R]    = svd( A(:,:,1,i) - m*A(:,:,2,i) );
    DI(:,:,i)  = L(:,1:2)*diag(sqrt([S(1,1);S(2,2)]));
    D11 = DI(1,1,i); D21 = DI(2,1,i); D31 = DI(3,1,i);
    D12 = DI(1,2,i); D22 = DI(2,2,i); D32 = DI(3,2,i);
    b1  = [     D12*D11,     D12*D21+D22*D11,      D12*D31+D32*D11,     D22*D21,    D22*D31+D32*D21,      D32*D31 ];
    b2  = [ D11^2-D12^2, 2*D11*D21-2*D12*D22, -2*D12*D32+2*D11*D31,-D22^2+D21^2, 2*D21*D31-2*D22*D32, D31^2-D32^2 ];
    c1  = [ transpose(linf(1:3,i)), 0,0,0 ];
    c2  = [ 0,linf(1,i),0, transpose(linf(2:3,i)),0 ];
    c3  = [ 0,0,linf(1,i), 0,transpose(linf(2:3,i)) ];
    if disparam > 0
      M   = [ M ; b1 ; b2 ; c1 ; c2 ; c3 ]
    else
      M   = [ M ; b1 ; b2 ; c1 ; c2 ; c3 ];
    end
end 
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[ B, LEFT, RIGHT ] = balance_design_matr(0, M , size(M,1), size(M,2), 1, 1); 
[U,S,V] = svd( B );
x_TLS   = RIGHT*V(:,end);
default_options = optimset;
if disparam > 0
  options = optimset( ...
          'Algorithm','active-set',...
  	      'Jacobian','off',...
  	      'LargeScale','off',...
  	      'DerivativeCheck','off',...
  	      'MaxIter',10e2,...
  	      'MaxFunEvals',10e3,...
  	      'Display','iter' );
  %	      'TolFun',10e-8,...
  %	      'TolX',10e-8,...
  x     = x_TLS/x_TLS(4);
  xinit = x([1:3,5:6])
else
  options = optimset( ...
          'Algorithm','active-set',...
  	      'Jacobian','off',...
  	      'LargeScale','off',...
  	      'DerivativeCheck','off',...
  	      'MaxIter',10e2,...
  	      'MaxFunEvals',10e3,...
  	      'Display','off' );
  %	      'TolFun',10e-8,...
  %	      'TolX',10e-8,...
  x     = x_TLS/x_TLS(4);
  xinit = x([1:3,5:6]);
end
[x, fval, exitflag, output] = fmincon( @objfun,...
                                       xinit,[],[],[],[],[],[],@constrfun,options,...
                                       2, 2, M );
if exitflag<=0 && disparam > 0, disp('Convergence problem.'); end
if disparam > 0
  x_TLS = [x(1:3);1;x(4:5)]
else
  x_TLS = [x(1:3);1;x(4:5)];
end
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ICDCP    = [ transpose(x_TLS(1:3)); x_TLS(2),transpose(x_TLS(4:5)); x_TLS(3),x_TLS(5),x_TLS(6) ];
[U,S,W]  = svd( T*ICDCP*transpose(T) );

IPC     = U(:,1:2)*sqrt(S(1:2,1:2));
% PC    = normalize( normalize(AInv(:,:,2)*W(:,end)) )
s       = diag(S);
H       = normalize_matr_det_1( U*diag([sqrt(s(1:2));1]) );

% ----------------------------------------------------------------------------
function IAC = plane_based_calibr( A, disparam )
  H = A(:,1:2,:);
  if size(H,3)<2,
     IAC = [];
  else
     Z = zeros(size(H,3),1);
     H11=Z; H12=Z; H11(:) = H(1,1,:); H12(:) = H(1,2,:);
     H21=Z; H22=Z; H21(:) = H(2,1,:); H22(:) = H(2,2,:);
     H31=Z; H32=Z; H31(:) = H(3,1,:); H32(:) = H(3,2,:);
     b1  = [ H12.*H11, H12.*H31+H32.*H11, H22.*H21, H22.*H31+H32.*H21, H32.*H31 ];
     b2  = [ H11.^2-H12.^2, -2*H12.*H32+2*H11.*H31,-H22.^2+H21.^2, 2*H21.*H31-2*H22.*H32, H31.^2-H32.^2 ];
     [U,S,V] = svd( [b1;b2], 0 );
     s       = diag(S);
     nullspace_dim = sum(s < eps * s(2) * 1e3);
     if nullspace_dim > 1
        IAC     = [];
     else
        if disparam > 0
          x_TLS   = V(:,end)
        else
          x_TLS   = V(:,end);
        end
        x_TLS   = [x_TLS(1);0;x_TLS(2:end)];
        IAC     = [ transpose(x_TLS(1:3)); x_TLS(2),transpose(x_TLS(4:5)); x_TLS(3),x_TLS(5),x_TLS(6) ];
     end
  end
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
function K = recover_calibr_matrix( IAC )
% ----------------------------------------------------------------------------
%[                     tau^2,                         0,                 -u0*tau^2]
%[                         0,                         1,                       -v0]
%[                 -u0*tau^2,                       -v0, u0^2*tau^2+v0^2+f^2*tau^2]
   K = [];
   if ~isempty(IAC),
     IAC 	= IAC/IAC(2,2);
     tau2 	= IAC(1,1);
     if tau2<0, return; end
     tau 	= sqrt( tau2 );
     u0         = -IAC(1,3)/tau2;
     v0         = -IAC(2,3);
     f2         = (IAC(3,3)-u0^2*tau2-v0^2)/tau2;
     if f2<0, return; end
     f 		= sqrt( f2 );
     K		= [f,0,u0;0,-f*tau,v0;0,0,1];
   end
 % ----------------------------------------------------------------------------
 function f = objfun( x, nb_Views, NbPairOfCircles, M)
 % ----------------------------------------------------------------------------
     x_TLS = [x(1:3);1;x(4:5)];
     F = M*x_TLS;
     f = sum(F.*F);

 % ----------------------------------------------------------------------------
 function [C,Ceq] = constrfun( x, nb_Views, NbPairOfCircles, M)
 % ----------------------------------------------------------------------------
     x_TLS = [x(1:3);1;x(4:5)];
     C     = [ ];
     CDCP  = [ transpose(x_TLS(1:3)); x_TLS(2),transpose(x_TLS(4:5)); x_TLS(3),x_TLS(5),x_TLS(6) ];
     Ceq   = [ det(CDCP)^2 ];

function err = erreur(i,j,E)
    
    Ell = param2ellipse([E.X0_in E.Y0_in E.a E.b -E.phi]);
    temp = Ell*[j';i';ones(1,size(i,1))];
    dist = (temp(1,:).*j'+temp(2,:).*i'+temp(3,:))./sqrt(temp(1,:).^2+temp(2,:).^2);%diag(temp'*[j';i';ones(1,size(i,1))])./(temp(1,:)'.^2+temp(2,:)'.^2);
    err = mean(abs(dist)) < E.a/10;
    %pp = ellipsepoints( [E.X0_in E.Y0_in E.a E.b -E.phi],200 );
    %for k = 1:size(i,1)
    %    err(k) = sqrt(min((pp(1,:)-j(k)).^2+(pp(2,:)-i(k)).^2));
    %end;
    %err = mean(err) < E.a/15;
