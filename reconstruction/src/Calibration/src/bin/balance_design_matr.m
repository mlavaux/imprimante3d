 % ----------------------------------------------------------------------------
    function [ X, LEFT, RIGHT ] = balance_design_matr(iter, X , n, p, t_n, t_p) 
 % ----------------------------------------------------------------------------
      %X0=X;
      LEFT  = eye(t_n*n);
      RIGHT = eye(t_p*p);
      for m = 1:iter
          for i = 1:n % rééquilibrage des lignes t_n x t_n
              ii           = (i-1)*t_n+1:(i-1)*t_n+t_n;
              Left(ii,ii) = 1/norm(X(ii,1:end),'fro') * eye(t_n);
          end
          X    = Left*X;
          LEFT = Left*LEFT;
          for j = 1:p % rééquilibrage des colonnes t_p x t_p
    	      jj           = (j-1)*t_p+1:(j-1)*t_p+t_p;
              Right(jj,jj) = 1/norm(X(1:end,jj),'fro') * eye(t_p);
          end
          X     = X*Right;
          RIGHT = RIGHT*Right;
     end
     %ZEO = LEFT*X0*RIGHT-X
     %pause
