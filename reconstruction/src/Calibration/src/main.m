clear;
addpath ./bin

I(:,:,:,1)=imread('../../../media/teabox/IMG_1243.JPG');
I(:,:,:,2)=imread('../../../media/teabox/IMG_1244.JPG');
I(:,:,:,3)=imread('../../../media/teabox/IMG_1245.JPG');
I(:,:,:,4)=imread('../../../media/teabox/IMG_1246.JPG');
%I(:,:,:,5)=imread('../../../media/teabox/IMG_1247.JPG');
%I(:,:,:,1)=imread('/home/pierre/tmp/cubes2_01.JPG');
%I(:,:,:,2)=imread('/home/pierre/tmp/cubes2_02.JPG');
%I(:,:,:,3)=imread('/home/pierre/tmp/cubes2_03.JPG');
%I(:,:,:,3)=imread('/home/pierre/tmp/cubes2_04.JPG');

%for i = 0:18
%  I(:,:,:,i+1) = imread(strcat(strcat('../../../media/tigrou_final/IMG_13', int2str(38 + i)), '.JPG'));
%end

%si 2 plans, num_plan est le nombre de mires dans le premier plan
num_plan = 2;
 
close all;[P,K,M] = calibrage(I, num_plan, 1);
display('Press <enter> to continue...');
pause;

verif_calibrage;

%affichage scène 3D
figure;
hold on
for i = 1:size(I,4)
    R = inv(K)*P(:,1:3,i);
    t = inv(K)*P(:,4,i);
    pos_cam = -R'*t;
    dir = R(3,:)';
    plot3([pos_cam(1) pos_cam(1)+0.2*dir(1)],[pos_cam(2) pos_cam(2)+0.2*dir(2)],[pos_cam(3) pos_cam(3)+0.2*dir(3)],'r');
    dir = R(1,:)';
    plot3([pos_cam(1) pos_cam(1)+0.2*dir(1)],[pos_cam(2) pos_cam(2)+0.2*dir(2)],[pos_cam(3) pos_cam(3)+0.2*dir(3)],'g');
    dir = R(2,:)';
    plot3([pos_cam(1) pos_cam(1)+0.2*dir(1)],[pos_cam(2) pos_cam(2)+0.2*dir(2)],[pos_cam(3) pos_cam(3)+0.2*dir(3)],'b');
end;
axis equal

%affichage de la premiere mire
pp = ellipsepoints([0 0 1 1 0],100);
for j = 1:6
    params_a = ellipse2param( transpose(P(:,[1 2 4],1))*M(:,:,j,1,1)*P(:,[1 2 4],1) );
    plot3(mean(params_a(3:4))*pp(1,:),mean(params_a(3:4))*pp(2,:),zeros(1,size(pp,2)),'b');
end;

%affichage des autres mires
for k = 2:min(size(M,4),num_plan)
    for i = 1:size(I,4)
        params_a = ellipse2param( transpose(P(:,[1 2 4],i))*M(:,:,1,1,i)*P(:,[1 2 4],i) );
        params_b = ellipse2param( transpose(P(:,[1 2 4],i))*M(:,:,1,k,i)*P(:,[1 2 4],i) );
        cc(i,:) = params_b(1:2)-params_a(1:2);
        for j = 1:6
            params_b  = ellipse2param( transpose(P(:,[1 2 4],i))*M(:,:,j,k,i)*P(:,[1 2 4],i) );
            ss(i,j) = mean(params_b(3:4));
        end;
    end;
    for j = 1:6
        plot3(mean(ss(:,j))*pp(1,:)+mean(cc(:,1)),mean(ss(:,j))*pp(2,:)+mean(cc(:,2)),zeros(1,size(pp,2)),'b');
    end;
end;

%affichage des points 3D
plot3(X(1,:),-X(2,:),X(3,:),'r*')
