nb_pts = 6;

%clique 4 points par image pour l appariement
for vue = 1:size(I,4)
    figure(vue);
    %imshow(I(:,:,:,vue));
    %hold on;
    %axis on;
    pause;
    for j = 1:nb_pts
        a(vue,:,j) = ginput(1);
        plot(a(vue,1,j),a(vue,2,j),'rx');
    end;
end;

%reconstruit les points en 3D
for j = 1:nb_pts
    A = [];
    for vue = 1:size(I,4)
        A = [A;P(1,:,vue)-a(vue,1,j)*P(3,:,vue);
            P(2,:,vue)-a(vue,2,j)*P(3,:,vue)];
    end;
    [U,S,V] = svd(A);
    X(:,j) = V(:,end)/V(end,end);
end;

%affiche les points 3D
figure;
plot3(X(1,:),X(2,:),X(3,:),'rx');
axis equal

%reprojette les points reconstruits dans les images et les affiche
for vue = 1:size(I,4)
    for j = 1:nb_pts
        figure(vue);
        temp = P(:,:,vue)*X;
        temp = temp./repmat(temp(3,:),3,1);
        plot(temp(1,:),temp(2,:),'go');
    end;
end;
