# 3D Reconstruction and 3D printing project.

## Structure

### impression
Is a project made in 2012 by V. Duvert, A. Lubineau, C. Naud, J. Packer and F. Ribon
This project describes the process to use the Ultimake 3D printer of the IRIT lab'.
A custom Blender plugins enable to preprocess the mesh before printing it.
Refer to impression/README for further information

# reconstruction
Is a project made in 2013 by V. Angladon, M. Lavaux, F. Leroux, G. Michelin and P. Tysebaert.
This projects contains a complete Matlab pipeline of 3D reconstruction and various processing scripts.
Refer to reconstruction/README for further information.
